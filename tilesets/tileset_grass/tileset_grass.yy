{
    "id": "bff55203-3bfd-4d3d-bf52-e18e2e135fe3",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tileset_grass",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 1,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "c98e7996-cd65-48a6-be06-5a5ea11b29bc",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 2,
    "tileheight": 512,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 512,
    "tilexoff": 0,
    "tileyoff": 0
}