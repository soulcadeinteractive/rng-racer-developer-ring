Converting GML script : ${project_dir}\scripts\udphp_serverSetData\udphp_serverSetData.gml
Converted description : /// @description udphp_serverSetData(n,string)/// @function udphp_serverSetData
/// @param n
/// @param string

Converting GML script : ${project_dir}\scripts\udphp_clientNetworking\udphp_clientNetworking.gml
Converted description : /// @description udphp_clientNetworking(client)/// @function udphp_clientNetworking
/// @param client

Converting GML script : ${project_dir}\scripts\udphp_createClient\udphp_createClient.gml
Converted description : /// @description udphp_createClient(udp_socket,server_ip,buffer,directconnect,directconnect_port)/// @function udphp_createClient
/// @param udp_socket
/// @param server_ip
/// @param buffer
/// @param directconnect
/// @param directconnect_port

Converting GML script : ${project_dir}\scripts\udphp_clientPunch\udphp_clientPunch.gml
Converted description : /// @description udphp_clientPunch(client)/// @function udphp_clientPunch
/// @param client

Converting GML script : ${project_dir}\scripts\udphp_serverPunch\udphp_serverPunch.gml
Converted description : /// @description udphp_serverPunch()/// @function udphp_serverPunch

Converting GML script : ${project_dir}\scripts\udphp_createServer\udphp_createServer.gml
Converted description : /// @description udphp_createServer(udp_server,buffer,player_list,upnp port)/// @function udphp_createServer
/// @param udp_server
/// @param buffer
/// @param player_list
/// @param upnp port

Converting GML script : ${project_dir}\scripts\udphp_serverCommitData\udphp_serverCommitData.gml
Converted description : /// @description udphp_serverCommitData()/// @function udphp_serverCommitData

Converting GML script : ${project_dir}\scripts\udphp_stopServer\udphp_stopServer.gml
Converted description : /// @description udphp_stopServer()/// @function udphp_stopServer

Converting GML script : ${project_dir}\scripts\udphp_clientReadData\udphp_clientReadData.gml
Converted description : /// @description udphp_clientReadData(client)/// @function udphp_clientReadData
/// @param client

Converting GML script : ${project_dir}\scripts\udphp_stopClient\udphp_stopClient.gml
Converted description : /// @description udphp_stopClient(client)/// @function udphp_stopClient
/// @param client

Converting GML script : ${project_dir}\scripts\udphp_playerListPort\udphp_playerListPort.gml
Converted description : /// @description udphp_playerListPort(player)/// @function udphp_playerListPort
/// @param player

Converting GML script : ${project_dir}\scripts\udphp_clientGetServerPort\udphp_clientGetServerPort.gml
Converted description : /// @description udphp_clientGetServerPort(client)/// @function udphp_clientGetServerPort
/// @param client

Converting GML script : ${project_dir}\scripts\udphp_clientIsConnected\udphp_clientIsConnected.gml
Converted description : /// @description udphp_clientIsConnected(client)/// @function udphp_clientIsConnected
/// @param client

Converting GML script : ${project_dir}\scripts\udphp_clientGetServerIP\udphp_clientGetServerIP.gml
Converted description : /// @description udphp_clientGetServerIP(client)/// @function udphp_clientGetServerIP
/// @param client

Converting GML script : ${project_dir}\scripts\udphp_hash\udphp_hash.gml
Converted description : /// @description udphp_hash()/// @function udphp_hash

Converting GML script : ${project_dir}\scripts\udphp_serverNetworking\udphp_serverNetworking.gml
Converted description : /// @description udphp_serverNetworking()/// @function udphp_serverNetworking

Converting GML script : ${project_dir}\scripts\udphp_playerListIP\udphp_playerListIP.gml
Converted description : /// @description udphp_playerListIP(player)/// @function udphp_playerListIP
/// @param player

Converting GML script : ${project_dir}\scripts\udphp_get_count\udphp_get_count.gml
Converted description : /// @description  udphp_get_count();/// @function  udphp_get_count

Converting GML script : ${project_dir}\scripts\udphp_handleerror\udphp_handleerror.gml
Converted description : /// @description udphp_handleerror(type,component,client_id,message)/// @function udphp_handleerror
/// @param type
/// @param component
/// @param client_id
/// @param message

Converting GML script : ${project_dir}\scripts\udphp_punchstate\udphp_punchstate.gml
Converted description : /// @description  udphp_punchstate(client);/// @function  udphp_punchstate
/// @param client

Converting GML script : ${project_dir}\scripts\udphp_string_explode\udphp_string_explode.gml
Converted description : /// @description udphp_sstring_explode(str,sep,index)/// @function udphp_sstring_explode
/// @param str
/// @param sep
/// @param index

Converting GML script : ${project_dir}\scripts\udphp_downloadServerList\udphp_downloadServerList.gml
Converted description : /// @description udphp_downloadServerList([limit],[sortby],[sortby_dir],[filter_data1],[filter_data2],[dfilter_ata3],[filter_data4],[filter_data5],[filter_data6],[dfilter_ata7],[filter_data8])/// @function udphp_downloadServerList
/// @param [limit]
/// @param [sortby]
/// @param [sortby_dir]
/// @param [filter_data1]
/// @param [filter_data2]
/// @param [dfilter_ata3]
/// @param [filter_data4]
/// @param [filter_data5]
/// @param [filter_data6]
/// @param [dfilter_ata7]
/// @param [filter_data8]

Converting GML script : ${project_dir}\scripts\udphp_downloadNetworking\udphp_downloadNetworking.gml
Converted description : /// @description udphp_downloadNetworking()/// @function udphp_downloadNetworking

Converting GML script : ${project_dir}\scripts\udphp_config\udphp_config.gml
Converted description : /// @description udphp_config(master_ip,master_port,reconnect_intv,timeouts,debug,silent,delta time,upnp)/// @function udphp_config
/// @param master_ip
/// @param master_port
/// @param reconnect_intv
/// @param timeouts
/// @param debug
/// @param silent
/// @param delta time
/// @param upnp

Converting GML script : ${project_dir}\scripts\udphp_error_message_handler\udphp_error_message_handler.gml
Converted description : /// @description  udphp_error_message_handler(message);/// @function  udphp_error_message_handler
/// @param message

Converting GML script : ${project_dir}\scripts\htme_error_message_handler\htme_error_message_handler.gml
Converted description : /// @description  htme_error_message_handler(message);/// @function  htme_error_message_handler
/// @param message

Converting GML script : ${project_dir}\scripts\htme_init\htme_init.gml
Converted description : /// @description htme_init([bool re-init])/// @function htme_init
/// @param [bool re-init]

Converting GML script : ${project_dir}\scripts\htme_hash\htme_hash.gml
Converted description : /// @description htme_hash()/// @function htme_hash

Converting GML script : ${project_dir}\scripts\htme_string_explode\htme_string_explode.gml
Converted description : /// @description htme_string_explode(str,sep,index)/// @function htme_string_explode
/// @param str
/// @param sep
/// @param index

Converting GML script : ${project_dir}\scripts\htme_ds_map_find_key\htme_ds_map_find_key.gml
Converted description : /// @description htme_ds_map_find_key(index,value);/// @function htme_ds_map_find_key
/// @param index
/// @param value

Converting GML script : ${project_dir}\scripts\htme_debugger\htme_debugger.gml
Converted description : /// @description htme_debugger(scriptname,level,message)/// @function htme_debugger
/// @param scriptname
/// @param level
/// @param message

Converting GML script : ${project_dir}\scripts\htme_playerMapPort\htme_playerMapPort.gml
Converted description : /// @description htme_playerMapPort(player)/// @function htme_playerMapPort
/// @param player

Converting GML script : ${project_dir}\scripts\htme_get_count\htme_get_count.gml
Converted description : /// @description  htme_get_count();/// @function  htme_get_count

Converting GML script : ${project_dir}\scripts\htme_isLocal\htme_isLocal.gml
Converted description : /// @description htme_isLocal();/// @function htme_isLocal

Converting GML script : ${project_dir}\scripts\htme_playerMapIP\htme_playerMapIP.gml
Converted description : /// @description htme_playerMapIP(player)/// @function htme_playerMapIP
/// @param player

Converting GML script : ${project_dir}\scripts\htme_clientConnectionFailed\htme_clientConnectionFailed.gml
Converted description : /// @description htme_clientConnectionFailed()/// @function htme_clientConnectionFailed

Converting GML script : ${project_dir}\scripts\htme_isStarted\htme_isStarted.gml
Converted description : /// @description htme_htme_isStarted()/// @function htme_htme_isStarted

Converting GML script : ${project_dir}\scripts\htme_clientIsConnected\htme_clientIsConnected.gml
Converted description : /// @description htme_clientIsConnected()/// @function htme_clientIsConnected

Converting GML script : ${project_dir}\scripts\htme_getPlayers\htme_getPlayers.gml
Converted description : /// @description htme_getPlayers()/// @function htme_getPlayers

Converting GML script : ${project_dir}\scripts\htme_isServer\htme_isServer.gml
Converted description : /// @description htme_isSefver()/// @function htme_isSefver

Converting GML script : ${project_dir}\scripts\htme_findPlayerInstance\htme_findPlayerInstance.gml
Converted description : /// @description htme_findPlayerInstance(object,player);/// @function htme_findPlayerInstance
/// @param object
/// @param player

Converting GML script : ${project_dir}\scripts\htme_isLostConnection\htme_isLostConnection.gml
Converted description : /// @description htme_isLostConnection()/// @function htme_isLostConnection

Converting GML script : ${project_dir}\scripts\htme_debugOverlayEnabled\htme_debugOverlayEnabled.gml
Converted description : /// @description htme_debugOverlayEnabled();/// @function htme_debugOverlayEnabled

Converting GML script : ${project_dir}\scripts\htme_getGamename\htme_getGamename.gml
Converted description : /// @description htme_getGamename()/// @function htme_getGamename

Converting GML script : ${project_dir}\scripts\htme_setGamename\htme_setGamename.gml
Converted description : /// @description htme_setGamename(name)/// @function htme_setGamename
/// @param name

Converting GML script : ${project_dir}\scripts\htme_setData\htme_setData.gml
Converted description : /// @description htme_setData(id(2-8),string)/// @function htme_setData
/// @param id(2-8

Converting GML script : ${project_dir}\scripts\htme_commitData\htme_commitData.gml
Converted description : /// @description htme_commitData()/// @function htme_commitData

Converting GML script : ${project_dir}\scripts\htme_disconnectNow\htme_disconnectNow.gml
Converted description : /// @description htme_disconnectNow()/// @function htme_disconnectNow

Converting GML script : ${project_dir}\scripts\htme_getPlayerNumber\htme_getPlayerNumber.gml
Converted description : /// @description htme_getPlayerNumber(playerhash);/// @function htme_getPlayerNumber
/// @param playerhash

Converting GML script : ${project_dir}\scripts\htme_serverDisconnect\htme_serverDisconnect.gml
Converted description : /// @description htme_serverDisconnect(playerhash);/// @function htme_serverDisconnect
/// @param playerhash

Converting GML script : ${project_dir}\scripts\htme_serverShutdown\htme_serverShutdown.gml
Converted description : /// @description htme_serverShutdown()/// @function htme_serverShutdown

Converting GML script : ${project_dir}\scripts\htme_clientDisconnect\htme_clientDisconnect.gml
Converted description : /// @description htme_clientDisconnect();/// @function htme_clientDisconnect

Converting GML script : ${project_dir}\scripts\htme_getDataServer\htme_getDataServer.gml
Converted description : /// @description htme_getDataServer(id(2-8))/// @function htme_getDataServer
/// @param id(2-8

Converting GML script : ${project_dir}\scripts\htme_startLANsearch\htme_startLANsearch.gml
Converted description : /// @description htme_startLANsearch(port,[gamefilter]);/// @function htme_startLANsearch
/// @param port
/// @param [gamefilter]

Converting GML script : ${project_dir}\scripts\htme_stopLANsearch\htme_stopLANsearch.gml
Converted description : /// @description htme_stopLANsearch();/// @function htme_stopLANsearch

Converting GML script : ${project_dir}\scripts\htme_serverEventHandlerDisconnecting\htme_serverEventHandlerDisconnecting.gml
Converted description : /// @description htme_serverEventHandlerDisconnecting(script);/// @function htme_serverEventHandlerDisconnecting
/// @param script

Converting GML script : ${project_dir}\scripts\htme_getLANServers\htme_getLANServers.gml
Converted description : /// @description htme_getLANServers();/// @function htme_getLANServers

Converting GML script : ${project_dir}\scripts\htme_serverEventHandlerConnecting\htme_serverEventHandlerConnecting.gml
Converted description : /// @description htme_serverEventHandlerConnecting(script);/// @function htme_serverEventHandlerConnecting
/// @param script

Converting GML script : ${project_dir}\scripts\htme_globalGet\htme_globalGet.gml
Converted description : /// @description htme_globalGet(name);/// @function htme_globalGet
/// @param name

Converting GML script : ${project_dir}\scripts\htme_globalSet\htme_globalSet.gml
Converted description : /// @description htme_globalSet(name,value,datatype);/// @function htme_globalSet
/// @param name
/// @param value
/// @param datatype

Converting GML script : ${project_dir}\scripts\htme_chatGetSender\htme_chatGetSender.gml
Converted description : /// @description htme_chatGetSender(chat_queue_entry)/// @function htme_chatGetSender
/// @param chat_queue_entry

Converting GML script : ${project_dir}\scripts\htme_globalSetFast\htme_globalSetFast.gml
Converted description : /// @description htme_globalSetFast(name,value,datatype);/// @function htme_globalSetFast
/// @param name
/// @param value
/// @param datatype

Converting GML script : ${project_dir}\scripts\htme_chatGetMessage\htme_chatGetMessage.gml
Converted description : /// @description htme_chatGetMessage(chat_queue_entry)/// @function htme_chatGetMessage
/// @param chat_queue_entry

Converting GML script : ${project_dir}\scripts\htme_clientStart\htme_clientStart.gml
Converted description : /// @description htme_clientStart(server_ip,server_port);/// @function htme_clientStart
/// @param server_ip
/// @param server_port

Converting GML script : ${project_dir}\scripts\htme_serverStart\htme_serverStart.gml
Converted description : /// @description htme_serverStart(port,maxclients)/// @function htme_serverStart
/// @param port
/// @param maxclients

Converting GML script : ${project_dir}\scripts\htme_syncGroupNow\htme_syncGroupNow.gml
Converted description : /// @description htme_syncGroupNow([sync group name], [sync group name 2], [3], ...)/// @function htme_syncGroupNow
/// @param [sync group name]
/// @param  [sync group name 2]
/// @param  [3]
/// @param  ...

Converting GML script : ${project_dir}\scripts\htme_clientCheckConnection\htme_clientCheckConnection.gml
Converted description : /// @description htme_clientCheckConnection();/// @function htme_clientCheckConnection

Converting GML script : ${project_dir}\scripts\htme_clientSyncSingleVarGroup\htme_clientSyncSingleVarGroup.gml
Converted description : /// @description htme_clientSyncSingleVarGroup(group,buffer);/// @function htme_clientSyncSingleVarGroup
/// @param group
/// @param buffer

Converting GML script : ${project_dir}\scripts\htme_clientConnect\htme_clientConnect.gml
Converted description : /// @description htme_clientConnect();/// @function htme_clientConnect

Converting GML script : ${project_dir}\scripts\htme_clientCheckConnectionNetworking\htme_clientCheckConnectionNetworking.gml
Converted description : /// @description htme_clientCheckConnectionNetworking();/// @function htme_clientCheckConnectionNetworking

Converting GML script : ${project_dir}\scripts\htme_clientStop\htme_clientStop.gml
Converted description : /// @description htme_clientStop()/// @function htme_clientStop

Converting GML script : ${project_dir}\scripts\htme_clientConnectNetworking\htme_clientConnectNetworking.gml
Converted description : /// @description htme_clientConnect(server_ip,server_port);/// @function htme_clientConnect
/// @param server_ip
/// @param server_port

Converting GML script : ${project_dir}\scripts\htme_clientBroadcastUnsync\htme_clientBroadcastUnsync.gml
Converted description : /// @description htme_clientBroadcastUnsync(hash)/// @function htme_clientBroadcastUnsync
/// @param hash

Converting GML script : ${project_dir}\scripts\htme_clientNetworking\htme_clientNetworking.gml
Converted description : /// @description htme_clientNetworking();/// @function htme_clientNetworking

Converting GML script : ${project_dir}\scripts\htme_clientRecieveVarGroup\htme_clientRecieveVarGroup.gml
Converted description : /// @description htme_clientRecieveVarGroup(instancehash,playerhash,object_id,instance,tolerance,datatype);/// @function htme_clientRecieveVarGroup
/// @param instancehash
/// @param playerhash
/// @param object_id
/// @param instance
/// @param tolerance
/// @param datatype

Converting GML script : ${project_dir}\scripts\htme_serverBroadcast\htme_serverBroadcast.gml
Converted description : /// @description htme_serverProcessKicks();/// @function htme_serverProcessKicks

Converting GML script : ${project_dir}\scripts\htme_serverSyncSingleVarGroup\htme_serverSyncSingleVarGroup.gml
Converted description : /// @description htme_serverSyncSingleVarGroup(group,buffer);/// @function htme_serverSyncSingleVarGroup
/// @param group
/// @param buffer

Converting GML script : ${project_dir}\scripts\htme_serverCheckConnections\htme_serverCheckConnections.gml
Converted description : /// @description htme_serverCheckConnections();/// @function htme_serverCheckConnections

Converting GML script : ${project_dir}\scripts\htme_serverNetworking\htme_serverNetworking.gml
Converted description : /// @description htme_serverNetworking()/// @function htme_serverNetworking

Converting GML script : ${project_dir}\scripts\htme_serverStop\htme_serverStop.gml
Converted description : /// @description htme_serverStop()/// @function htme_serverStop

Converting GML script : ${project_dir}\scripts\htme_serverKickClient\htme_serverKickClient.gml
Converted description : /// @description htme_serverKickClient(client_ip_port);/// @function htme_serverKickClient
/// @param client_ip_port

Converting GML script : ${project_dir}\scripts\htme_serverRecieveVarGroup\htme_serverRecieveVarGroup.gml
Converted description : /// @description htme_serverRecieveVarGroup(instancehash,playerhash,object_id,inst_stayAlive,instance,tolerance,datatype,groupname,inst_room);/// @function htme_serverRecieveVarGroup
/// @param instancehash
/// @param playerhash
/// @param object_id
/// @param inst_stayAlive
/// @param instance
/// @param tolerance
/// @param datatype
/// @param groupname
/// @param inst_room

Converting GML script : ${project_dir}\scripts\htme_serverCheckConnectionsNetworking\htme_serverCheckConnectionsNetworking.gml
Converted description : /// @description htme_serverCheckConnections();/// @function htme_serverCheckConnections

Converting GML script : ${project_dir}\scripts\htme_serverSendBufferToAllExcept\htme_serverSendBufferToAllExcept.gml
Converted description : /// @description htme_serverSendBufferToAllExcept(buffer,exclude,[checkroom])/// @function htme_serverSendBufferToAllExcept
/// @param buffer
/// @param exclude
/// @param [checkroom]

Converting GML script : ${project_dir}\scripts\htme_serverSendAllInstances\htme_serverSendAllInstances.gml
Converted description : /// @description htme_serverSendAllInstances(player);/// @function htme_serverSendAllInstances
/// @param player

Converting GML script : ${project_dir}\scripts\htme_serverPlayerIsInRoom\htme_serverPlayerIsInRoom.gml
Converted description : /// @description htme_serverPlayerIsInRoom(player,troom);/// @function htme_serverPlayerIsInRoom
/// @param player
/// @param troom

Converting GML script : ${project_dir}\scripts\htme_serverConnectNetworking\htme_serverConnectNetworking.gml
Converted description : /// @description htme_serverConnectNetworking()/// @function htme_serverConnectNetworking

Converting GML script : ${project_dir}\scripts\htme_serverSyncPlayersUDPHP\htme_serverSyncPlayersUDPHP.gml
Converted description : /// @description htme_syncPlayersUDPHP()/// @function htme_syncPlayersUDPHP

Converting GML script : ${project_dir}\scripts\htme_serverBroadcastRoomChange\htme_serverBroadcastRoomChange.gml
Converted description : /// @description htme_serverBroadcastRoomChange(hash)/// @function htme_serverBroadcastRoomChange
/// @param hash

Converting GML script : ${project_dir}\scripts\htme_serverRemoveBackup\htme_serverRemoveBackup.gml
Converted description : /// @description htme_serverRemoveBackup(hash);/// @function htme_serverRemoveBackup
/// @param hash

Converting GML script : ${project_dir}\scripts\htme_serverRecreateInstancesLocal\htme_serverRecreateInstancesLocal.gml
Converted description : /// @description htme_serverRecreateInstancesLocal();/// @function htme_serverRecreateInstancesLocal

Converting GML script : ${project_dir}\scripts\htme_step\htme_step.gml
Converted description : /// @description htme_step()/// @function htme_step

Converting GML script : ${project_dir}\scripts\htme_chatSendServer\htme_chatSendServer.gml
Converted description : /// @description htme_chatSendServer(channel,message,to)/// @function htme_chatSendServer
/// @param channel
/// @param message
/// @param to

Converting GML script : ${project_dir}\scripts\htme_serverBroadcastUnsync\htme_serverBroadcastUnsync.gml
Converted description : /// @description htme_serverBroadcastUnsync(hash,[target])/// @function htme_serverBroadcastUnsync
/// @param hash
/// @param [target]

Converting GML script : ${project_dir}\scripts\htme_syncInstances\htme_syncInstances.gml
Converted description : /// @description htme_syncInstances()/// @function htme_syncInstances

Converting GML script : ${project_dir}\scripts\htme_forceSyncLocalInstances\htme_forceSyncLocalInstances.gml
Converted description : /// @description htme_forceSyncLocalInstances(playerhash);/// @function htme_forceSyncLocalInstances
/// @param playerhash

Converting GML script : ${project_dir}\scripts\htme_serverAskPlayersToResync\htme_serverAskPlayersToResync.gml
Converted description : /// @description htme_serverAskPlayersToResync(room,except);/// @function htme_serverAskPlayersToResync
/// @param room
/// @param except

Converting GML script : ${project_dir}\scripts\htme_syncVar\htme_syncVar.gml
Converted description : /// @description htme_syncVar(buffer,group,datatype,newval,varname,prevSyncMap,[addName]);/// @function htme_syncVar
/// @param buffer
/// @param group
/// @param datatype
/// @param newval
/// @param varname
/// @param prevSyncMap
/// @param [addName]

Converting GML script : ${project_dir}\scripts\htme_networking\htme_networking.gml
Converted description : /// @description htme_networking()/// @function htme_networking

Converting GML script : ${project_dir}\scripts\htme_RecieveVar\htme_RecieveVar.gml
Converted description : /// @description htme_RecieveVar(instvar,newval,tolerance,datatype);/// @function htme_RecieveVar
/// @param instvar
/// @param newval
/// @param tolerance
/// @param datatype

Converting GML script : ${project_dir}\scripts\htme_recieveVarGroup\htme_recieveVarGroup.gml
Converted description : /// @description htme_recieveVarGroup();/// @function htme_recieveVarGroup

Converting GML script : ${project_dir}\scripts\htme_recieveGS\htme_recieveGS.gml
Converted description : /// @description htme_recieveGS();/// @function htme_recieveGS

Converting GML script : ${project_dir}\scripts\htme_networking_searchForBroadcasts\htme_networking_searchForBroadcasts.gml
Converted description : /// @description htme_networking_searchForBroadcasts();/// @function htme_networking_searchForBroadcasts

Converting GML script : ${project_dir}\scripts\htme_syncSingleVarGroup\htme_syncSingleVarGroup.gml
Converted description : /// @description htme_syncSingleVarGroup(group,target)/// @function htme_syncSingleVarGroup
/// @param group
/// @param target

Converting GML script : ${project_dir}\scripts\htme_roomend\htme_roomend.gml
Converted description : /// @description htme_roomend()/// @function htme_roomend

Converting GML script : ${project_dir}\scripts\htme_isStayAlive\htme_isStayAlive.gml
Converted description : /// @description htme_isStayAlive(hash)/// @function htme_isStayAlive
/// @param hash

Converting GML script : ${project_dir}\scripts\htme_roomstart\htme_roomstart.gml
Converted description : /// @description htme_roomstart()/// @function htme_roomstart

Converting GML script : ${project_dir}\scripts\htme_shutdown\htme_shutdown.gml
Converted description : /// @description htme_shutdown();/// @function htme_shutdown

Converting GML script : ${project_dir}\scripts\htme_cleanUpInstance\htme_cleanUpInstance.gml
Converted description : /// @description htme_cleanUpInstance(inst);/// @function htme_cleanUpInstance
/// @param inst

Converting GML script : ${project_dir}\scripts\htme_debugoverlay\htme_debugoverlay.gml
Converted builtin : line 29 : view_xview -> __view_get( e__VW.XView, 0 )
Converted builtin : line 30 : view_xview -> __view_get( e__VW.XView, 0 )
Converted builtin : line 31 : view_yview -> __view_get( e__VW.YView, 0 )
Converted builtin : line 32 : view_yview -> __view_get( e__VW.YView, 0 )
Converted builtin : line 30 : view_wview -> __view_get( e__VW.WView, 0 )
Converted builtin : line 32 : view_hview -> __view_get( e__VW.HView, 0 )
Converted description : /// @description htme_debugoverlay();/// @function htme_debugoverlay

Converting GML script : ${project_dir}\scripts\htme_sendGS\htme_sendGS.gml
Converted description : /// @description htme_sendGS(target,variable,[exclude]);/// @function htme_sendGS
/// @param target
/// @param variable
/// @param [exclude]

Converting GML script : ${project_dir}\scripts\htme_do_createMicro\htme_do_createMicro.gml
Converted description : /// @description htme_debugoverlay_createMicro(player_hash,object_id,instanceid,instancehash,isVisible,isExisting);/// @function htme_debugoverlay_createMicro
/// @param player_hash
/// @param object_id
/// @param instanceid
/// @param instancehash
/// @param isVisible
/// @param isExisting

Converting GML script : ${project_dir}\scripts\htme_doMain\htme_doMain.gml
Converted description : /// @description htme_debugoverlayStateMain();/// @function htme_debugoverlayStateMain

Converting GML script : ${project_dir}\scripts\htme_dotbd\htme_dotbd.gml
Converted description : /// @description htme_dotbd();/// @function htme_dotbd

Converting GML script : ${project_dir}\scripts\htme_doOff\htme_doOff.gml
Converted description : /// @description htme_debugoverlayStateMain();/// @function htme_debugoverlayStateMain

Converting GML script : ${project_dir}\scripts\htme_doDrawInstanceTable\htme_doDrawInstanceTable.gml
Converted description : /// @description htme_debugoverlayDrawInstanceTable(title,type,filter,drawInRoom);/// @function htme_debugoverlayDrawInstanceTable
/// @param title
/// @param type
/// @param filter
/// @param drawInRoom

Converting GML script : ${project_dir}\scripts\htme_doInstVisible\htme_doInstVisible.gml
Converted description : /// @description htme_debugoverlayStateMain();/// @function htme_debugoverlayStateMain

Converting GML script : ${project_dir}\scripts\htme_doInstAll\htme_doInstAll.gml
Converted description : /// @description htme_debugoverlayStateInstAll();/// @function htme_debugoverlayStateInstAll

Converting GML script : ${project_dir}\scripts\htme_sendGSFast\htme_sendGSFast.gml
Converted description : /// @description htme_sendGSFast(target,variable,[exclude]);/// @function htme_sendGSFast
/// @param target
/// @param variable
/// @param [exclude]

Converting GML script : ${project_dir}\scripts\htme_doInstCached\htme_doInstCached.gml
Converted description : /// @description htme_debugoverlayStateInstInvisible();/// @function htme_debugoverlayStateInstInvisible

Converting GML script : ${project_dir}\scripts\htme_doGlobalSync\htme_doGlobalSync.gml
Converted description : /// @description htme_doGlobalSync();/// @function htme_doGlobalSync

Converting GML script : ${project_dir}\scripts\htme_doInstInvisible\htme_doInstInvisible.gml
Converted description : /// @description htme_debugoverlayStateInstInvisible();/// @function htme_debugoverlayStateInstInvisible

Converting GML script : ${project_dir}\scripts\htme_doSignedPackets\htme_doSignedPackets.gml
Converted description : /// @description htme_doGlobalSync(show_inbox);/// @function htme_doGlobalSync
/// @param show_inbox

Converting GML script : ${project_dir}\scripts\htme_doChat\htme_doChat.gml
Converted description : /// @description htme_doChat();/// @function htme_doChat

Converting GML script : ${project_dir}\scripts\htme_doMapsAndLists\htme_doMapsAndLists.gml
Converted description : /// @description htme_doGlobalSync();/// @function htme_doGlobalSync

Converting GML script : ${project_dir}\scripts\htme_doPlayers\htme_doPlayers.gml
Converted description : /// @description htme_debugoverlayStateInstInvisible();/// @function htme_debugoverlayStateInstInvisible

Converting GML script : ${project_dir}\scripts\htme_defaultEventHandler\htme_defaultEventHandler.gml
Converted description : /// @description htme_defaultEventHandler(argument0);/// @function htme_defaultEventHandler
/// @param argument0

Converting GML script : ${project_dir}\scripts\htme_clean_connect_leftovers\htme_clean_connect_leftovers.gml
Converted description : /// @description  htme_clean_connect_leftovers();/// @function  htme_clean_connect_leftovers

Converting GML script : ${project_dir}\scripts\htme_chatSend\htme_chatSend.gml
Converted description : /// @description htme_chatSend(channel,message,[to])/// @function htme_chatSend
/// @param channel
/// @param message
/// @param [to]

Converting GML script : ${project_dir}\scripts\htme_sendNewSignedPacket\htme_sendNewSignedPacket.gml
Converted description : /// @description htme_sendNewSignedPacket(buffer,target,[exclude],[room]);/// @function htme_sendNewSignedPacket
/// @param buffer
/// @param target
/// @param [exclude]
/// @param [room]

Converting GML script : ${project_dir}\scripts\htme_clean_mp_sync\htme_clean_mp_sync.gml
Converted description : /// @description  htme_clean_mp_sync();/// @function  htme_clean_mp_sync

Converting GML script : ${project_dir}\scripts\htme_sendSingleSignedPacket\htme_sendSingleSignedPacket.gml
Converted description : /// @description htme_sendSingleSignedPacket(target,buffer,n);/// @function htme_sendSingleSignedPacket
/// @param target
/// @param buffer
/// @param n

Converting GML script : ${project_dir}\scripts\htme_recieveSignedPackets\htme_recieveSignedPackets.gml
Converted description : /// @description htme_recieveSignedPackets();/// @function htme_recieveSignedPackets

Converting GML script : ${project_dir}\scripts\htme_clean_signed_packets\htme_clean_signed_packets.gml
Converted description : /// @description  htme_clean_signed_packets(client_ip_port);/// @function  htme_clean_signed_packets
/// @param client_ip_port

Converting GML script : ${project_dir}\scripts\htme_initSignedPacket\htme_initSignedPacket.gml
Converted description : /// @description htme_initSignedPacket(buffer,target);/// @function htme_initSignedPacket
/// @param buffer
/// @param target

Converting GML script : ${project_dir}\scripts\mp_sync\mp_sync.gml
Converted description : /// @description mp_sync()/// @function mp_sync

Converting GML script : ${project_dir}\scripts\mp_add\mp_add.gml
Converted description : /// @description mp_add(groupname,variables,datatype,interval);/// @function mp_add
/// @param groupname
/// @param variables
/// @param datatype
/// @param interval

Converting GML script : ${project_dir}\scripts\mp_addBuiltinPhysics\mp_addBuiltinPhysics.gml
Converted description : /// @description mp_addBuiltinPhysics(groupname,interval);/// @function mp_addBuiltinPhysics
/// @param groupname
/// @param interval

Converting GML script : ${project_dir}\scripts\mp_addBuiltinBasic\mp_addBuiltinBasic.gml
Converted description : /// @description mp_addBuiltinBasic(groupname,interval);/// @function mp_addBuiltinBasic
/// @param groupname
/// @param interval

Converting GML script : ${project_dir}\scripts\mp_setType\mp_setType.gml
Converted description : /// @description mp_setType(group,type)/// @function mp_setType
/// @param group
/// @param type

Converting GML script : ${project_dir}\scripts\mp_unsync\mp_unsync.gml
Converted description : /// @description mp_unsync()/// @function mp_unsync

Converting GML script : ${project_dir}\scripts\mp_addPosition\mp_addPosition.gml
Converted description : /// @description mp_addPosition(groupname,interval);/// @function mp_addPosition
/// @param groupname
/// @param interval

Converting GML script : ${project_dir}\scripts\htme_serverProcessKicks\htme_serverProcessKicks.gml
Converted description : /// @description htme_serverProcessKicks();/// @function htme_serverProcessKicks

Converting GML script : ${project_dir}\scripts\mp_map_syncOut\mp_map_syncOut.gml
Converted description : /// @description mp_map_syncOut(varName, variable)/// @function mp_map_syncOut
/// @param varName
/// @param  variable

Converting GML script : ${project_dir}\scripts\mp_tolerance\mp_tolerance.gml
Converted description : /// @description mp_tolerance(groupname,tolerance)/// @function mp_tolerance
/// @param groupname
/// @param tolerance

Converting GML script : ${project_dir}\scripts\mp_syncAsChatHandler\mp_syncAsChatHandler.gml
Converted description : /// @description mp_syncAsChatHandler(channel)/// @function mp_syncAsChatHandler
/// @param channel

Converting GML script : ${project_dir}\scripts\mp_map_syncIn\mp_map_syncIn.gml
Converted description : /// @description mp_map_syncIn(varName, variable)/// @function mp_map_syncIn
/// @param varName
/// @param  variable

Converting GML script : ${project_dir}\scripts\mp_chatGetQueue\mp_chatGetQueue.gml
Converted description : /// @description mp_chatGetQueue()/// @function mp_chatGetQueue

Converting GML script : ${project_dir}\scripts\mp_stayAlive\mp_stayAlive.gml
Converted description : /// @description mp_stayAlive();/// @function mp_stayAlive

Converting GML script : ${project_dir}\scripts\mp_chatSend\mp_chatSend.gml
Converted description : /// @description mp_chatSend(message,[to])/// @function mp_chatSend
/// @param message
/// @param [to]

Converting GML script : ${project_dir}\objects\obj_punch_client\Create_0.gml
Converted description : /// @description README

Converting GML script : ${project_dir}\objects\obj_htme\Create_0.gml
Converted description : /// @description htme_init(); - Start engine/// @function htme_init

Converting GML script : ${project_dir}\objects\obj_htme\Destroy_0.gml
Converted description : /// @description  Clean

Converting GML script : ${project_dir}\objects\obj_htme\Step_0.gml
Converted description : /// @description htme_step(); - Pass step event to the engine/// @function htme_step

Converting GML script : ${project_dir}\objects\obj_htme\Other_68.gml
Converted description : /// @description htme_networking(); - Pass networking event to the engine/// @function htme_networking

Converting GML script : ${project_dir}\objects\obj_htme\Other_5.gml
Converted description : /// @description htme_roomend(); - Clear up instances/// @function htme_roomend

Converting GML script : ${project_dir}\objects\obj_htme\Other_4.gml
Converted description : /// @description  Re-init if disconnected in last room

Converting GML script : ${project_dir}\objects\obj_htme\Other_3.gml
Converted description : /// @description  Clean

Converting GML script : ${project_dir}\objects\obj_htme\Draw_0.gml
Converted description : /// @description htme_debugoverlay(); - Draw debug overlay/// @function htme_debugoverlay

Converting GML script : ${project_dir}\objects\obj_htme\KeyPress_27.gml
Converted description : /// @description  Shutdown client or server

Converting GML script : ${project_dir}\scripts\htme_serverEventPlayerConnected\htme_serverEventPlayerConnected.gml
Converted description : /// @description htme_serverEventPlayerConnected(ip,port);/// @function htme_serverEventPlayerConnected
/// @param ip
/// @param port

updated compatibility script __global_object_depths.gml