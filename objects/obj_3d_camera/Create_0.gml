/// @description Insert description here
//Initialize 3D
render_3d_init();
var object_layer = layer_get_id("Cars");
var road_layer = layer_get_id("Tiles_Road");
var grass_layer = layer_get_id("Tile_Layer_Grass");
var particle_layer = layer_get_id("Particle_Layer");


//Setup Camera Variables
camera_variables_init();


//Initialize Camera
camera_init(0,true,60,view_get_wport(0)/view_get_hport(0),32,32000,camera_update_0);

layer_script_begin(object_layer, layer_set_objects);
layer_script_begin(particle_layer, layer_set_particle_layer);
//layer_script_end(particle_layer, layer_set_particle_layer);
layer_script_begin(road_layer, layer_set_roads);
layer_script_begin(grass_layer, layer_set_grass);

//Set camera's sense of up
camera_xup[0] = 0;
camera_yup[0] = 0;
camera_zup[0] = 1;

//Starting Camera Setup
camera_z_distance[0] = 1280;
camera_x_distance[0] = 1;
camera_y_distance[0] = 1;
camera_angle[0] = 0;
camera_x[0] = 0
camera_y[0] = 0
camera_z[0] = 0;

