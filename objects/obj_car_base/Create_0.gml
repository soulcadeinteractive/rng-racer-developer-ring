my_target = -1;
angle_dif = 0;
car_speed = 5120000;
//intitialize Particle System
part = part_system_create();
part_system_automatic_draw(part,false);
part_system_position(part,0,0);

//Create Emmiter
em = part_emitter_create(part);

//Tire Particle
tire_part = part_type_create();
part_type_sprite(tire_part,spr_tire_tracks,false,false,false);
part_type_life(tire_part,room_speed * 4,room_speed * 4);
part_type_alpha2(tire_part,0.7,0);

boost = 1000;

//image_xscale = 0.2369598;
//image_yscale = 0.2369598;