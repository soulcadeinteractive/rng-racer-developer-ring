//Particle System
if (phy_speed > 1)
   {
   part_emitter_region(part,em,x-1,x+1,y-1,y+1,ps_shape_rectangle,ps_distr_gaussian);
   inertia_dir = point_direction(x,y,x+phy_speed_x,y+phy_speed_y);
   forward_dir = point_direction(x,y,x + lengthdir_x(256,-phy_rotation - 90),y + lengthdir_y(256,-phy_rotation - 90));
   
   angle_dif = abs(angle_difference(self.forward_dir,self.inertia_dir));
	var car_front_x = phy_position_x + lengthdir_x(128,-phy_rotation - 90);
	var car_front_y = phy_position_y + lengthdir_y(128,-phy_rotation - 90);
	var car_back_x = phy_position_x + lengthdir_x(-128,-phy_rotation - 90);
	var car_back_y = phy_position_y + lengthdir_y(-128,-phy_rotation - 90);
	
	var lay_id = layer_get_id("Tiles_Road");
	var map_id = layer_tilemap_get_id(lay_id);
	var car_front_wheels_road_contact = tilemap_get_at_pixel(map_id, car_front_x, car_front_y);
	var car_rear_wheels_road_contact = tilemap_get_at_pixel(map_id, car_back_x, car_back_y);
	
   if (angle_dif > 20)
   && car_front_wheels_road_contact != 0
   && car_rear_wheels_road_contact != 0
      {
      part_type_orientation(tire_part,-phy_rotation,-phy_rotation,0,0,0);
      part_emitter_burst(part,em,tire_part,20)
	  boost += (angle_dif/90)
      }
   }

if my_target != -1
   {
   t_dir = point_direction(x,y,my_target.x,my_target.y)
   t_xd = lengthdir_x(car_speed,t_dir)
   t_yd = lengthdir_y(car_speed,t_dir)
   
   physics_apply_force(x,y,t_xd,t_yd)
   phy_rotation = -t_dir - 90
   }

