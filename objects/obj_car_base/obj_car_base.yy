{
    "id": "79640a70-0e3c-4f51-905c-40e3024a13c5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_car_base",
    "eventList": [
        {
            "id": "b08aa0af-ad12-4c8d-97d8-b7284c9773a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "79640a70-0e3c-4f51-905c-40e3024a13c5"
        },
        {
            "id": "ffe8a547-663e-4082-9aee-2d7a069895a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "79640a70-0e3c-4f51-905c-40e3024a13c5"
        },
        {
            "id": "085d770f-a747-42be-b561-901dc3d516d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "79640a70-0e3c-4f51-905c-40e3024a13c5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "79640a70-0e3c-4f51-905c-40e3024a13c5"
        },
        {
            "id": "e2e4a5de-3205-4d1b-b3dc-5009e93a42e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "79640a70-0e3c-4f51-905c-40e3024a13c5"
        },
        {
            "id": "5d244694-4e11-48e0-96a0-b571e746d334",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "41728e89-67cb-47bc-a1b4-5ca98ed06147",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "79640a70-0e3c-4f51-905c-40e3024a13c5"
        },
        {
            "id": "7956ac2b-c83b-4895-b00a-135fd4197a0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 8,
            "m_owner": "79640a70-0e3c-4f51-905c-40e3024a13c5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "1c9814ac-c10d-45bc-a820-06da38493a21",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 123.929794,
            "y": 0.333374023
        },
        {
            "id": "25e3b050-63e0-4f12-bdea-c92552213dab",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 157.9298,
            "y": 30.333374
        },
        {
            "id": "3c98e168-f716-473a-aaab-3771afedce7a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 158.9298,
            "y": 263.9596
        },
        {
            "id": "3d08b511-a207-496f-967b-ebe3f9b68608",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 139.9298,
            "y": 317.9596
        },
        {
            "id": "22844dc1-761f-4bbf-8eaa-793fe36cce10",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 28.9297943,
            "y": 320.9596
        },
        {
            "id": "947ebb28-f14d-4374-b547-e8ac4baa929d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 10.9297943,
            "y": 268.9596
        },
        {
            "id": "f14649fa-5122-4e8b-801f-ca73eeea3db2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 11.1940689,
            "y": 30.660183
        },
        {
            "id": "88f09e96-c37c-4b0d-b9e0-6d59d4ba7b94",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 39.19407,
            "y": -0.5398159
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "9a77cb1a-6b78-4e9a-b544-cc8bc26a7a1b",
    "visible": true
}