{
    "id": "aaf2f767-c333-4f29-bc5d-00ddc326d8a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_track_generation",
    "eventList": [
        {
            "id": "fe3d7676-4409-458a-bcc6-7d594c239626",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aaf2f767-c333-4f29-bc5d-00ddc326d8a5"
        },
        {
            "id": "a56b9b11-15a7-45e3-9820-f4e1c5fef63e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "aaf2f767-c333-4f29-bc5d-00ddc326d8a5"
        },
        {
            "id": "05d78eb2-9458-4655-bf2e-5683739855ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "aaf2f767-c333-4f29-bc5d-00ddc326d8a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}