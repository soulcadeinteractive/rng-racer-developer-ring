/// @description Insert description here
// You can write your code in this editor

debug_track = true;
col=c_red; //grid color
dcol=c_red; //your default color
gw=1024; //the width of each cell in the grid
gh=1024; //the height of each cell in the grid
w=6; //the width of the grid lines
yy=0; //the starting y
xx=0; //the starting x

bcol=c_blue; //grid color
bdcol=c_blue; //your default color
bgw=3072; //the width of each cell in the grid
bgh=3072; //the height of each cell in the grid
bw=12; //the width of the grid lines
byy=0; //the starting y
bxx=0; //the starting x

//Lay down grass
var tileset = layer_tilemap_get_id("Tile_Layer_Grass")
var room_cell_width = 20;
var room_cell_height = 20;
var margin = 0.5;
for (var xxx = -(room_cell_width * margin); xxx < (room_cell_width + (room_cell_width * margin)); xxx += 1)
	{
	for (var yyy = -(room_cell_height * margin); yyy < (room_cell_height + (room_cell_height * margin)); yyy += 1)
		{
		tilemap_set_at_pixel(tileset,1,xxx * 512,yyy * 512);
		}
	}
scr_init_track_generation(room_width/3,room_height/3);
