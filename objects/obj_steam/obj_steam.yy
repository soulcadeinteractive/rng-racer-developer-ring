{
    "id": "be1cd00c-fd18-4d39-aede-2f6e40ba6d80",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_steam",
    "eventList": [
        {
            "id": "b4e667e3-29ca-4b4f-a4dc-d623013b5b52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "be1cd00c-fd18-4d39-aede-2f6e40ba6d80"
        },
        {
            "id": "08caedd2-ebce-4cc2-acf3-fb6b9d10ce78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "be1cd00c-fd18-4d39-aede-2f6e40ba6d80"
        },
        {
            "id": "9b10972d-233c-4618-9ffa-081855dde6c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "be1cd00c-fd18-4d39-aede-2f6e40ba6d80"
        },
        {
            "id": "36bd465a-48e7-463c-9e5c-9967ec6b0e53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 69,
            "eventtype": 7,
            "m_owner": "be1cd00c-fd18-4d39-aede-2f6e40ba6d80"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}