/*****************************
* If a command has been submitted...
*****************************/
if console_submit(my_console) {

    /*****************************
    * Here we can put all our commands.
    *****************************/
    if console_cmd(my_console,"reset position")
       {
       with(obj_multiplayer_player)
            {                  
                 {
                 x = xstart;
                 y = ystart;
                 }
       
			}
    /*****************************
    * If user enters into the console:
    * show_message "message"
    *****************************/
    if console_cmd(my_console,"show_message") {
    
        /*****************************
        * Get the message value to display
        *****************************/
        msg = console_value(my_console,1);
        
        /*****************************
        * Show message
        *****************************/
        show_message(msg);
    
    }

}

/* */
/*  */
}