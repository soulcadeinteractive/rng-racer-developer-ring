{
    "id": "68affd37-ff1d-42f8-862a-6065ca831b65",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_console",
    "eventList": [
        {
            "id": "e8059a76-edd2-4216-b1cc-f2f955c17d4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "68affd37-ff1d-42f8-862a-6065ca831b65"
        },
        {
            "id": "6b5072f5-8351-454a-9c6f-d27d4ef95b53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "68affd37-ff1d-42f8-862a-6065ca831b65"
        },
        {
            "id": "3b4fa972-4119-4f3f-948f-b3b8c49334f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "68affd37-ff1d-42f8-862a-6065ca831b65"
        },
        {
            "id": "53ae5739-b056-475b-8d1a-50658d5eae50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "68affd37-ff1d-42f8-862a-6065ca831b65"
        },
        {
            "id": "e2acb76a-e78c-4ccf-8a38-af7917aead1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 18,
            "eventtype": 9,
            "m_owner": "68affd37-ff1d-42f8-862a-6065ca831b65"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}