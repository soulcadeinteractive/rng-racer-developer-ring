/// @description Insert description here
// You can write your code in this editor


var xx = view_xport[player_index];
var yy = view_yport[player_index];
var width = view_wport[player_index]
var height = view_hport[player_index]

if gamepad_is_connected(player_index) == true
{
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_font(fnt_pixel);
draw_set_color(c_black);
draw_text(xx + 64,yy + 64,string(player_index + 1));
draw_set_color(image_blend);
draw_text(xx + 68,yy + 68,string(player_index + 1));

if gamepad_button_check(player_index,gp_select)
{
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_font(fnt_tall_dark_handsome)
draw_text(64 + xx,64 + yy,"Horizontal Speed: " + string(phy_speed_x));
draw_text(64 + xx,128 + yy,"Vertical Speed: " + string(phy_speed_y));
draw_text(64 + xx,192 + yy,"Speed: " + string(phy_speed));
draw_text(64 + xx,256 + yy,"Boost: " + string(boost));
draw_text(64 + xx,320 + yy,"Screen Shake: " + string(shake));
}
}
if gamepad_is_connected(player_index) == false
	{
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	draw_set_color(c_black);
	draw_set_alpha(0.8);
	draw_rectangle(xx,yy,xx + width,yy + height,false);
	draw_set_color(c_white);
	draw_set_alpha(1);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	draw_text(xx + (width/2),yy + (height/2), "Player " + string(player_index + 1) + "#" + "  Please plug in your controller")
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	}