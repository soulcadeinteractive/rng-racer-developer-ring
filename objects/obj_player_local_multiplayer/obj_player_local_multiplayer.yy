{
    "id": "20282fc7-a81f-41d7-8003-16d8bd19f6f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_local_multiplayer",
    "eventList": [
        {
            "id": "0457b45c-6ca9-4e6b-bb38-63a4450dc9cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "20282fc7-a81f-41d7-8003-16d8bd19f6f1"
        },
        {
            "id": "995053f1-901b-45ac-b974-fecd5d853bcf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "20282fc7-a81f-41d7-8003-16d8bd19f6f1"
        },
        {
            "id": "b759db0d-d096-46a5-8af1-4f2f49f5268e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "20282fc7-a81f-41d7-8003-16d8bd19f6f1"
        },
        {
            "id": "f1c2ba47-c5cc-4c84-aefc-163e3396aa36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "20282fc7-a81f-41d7-8003-16d8bd19f6f1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "20282fc7-a81f-41d7-8003-16d8bd19f6f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "79640a70-0e3c-4f51-905c-40e3024a13c5",
    "persistent": false,
    "physicsAngularDamping": 36,
    "physicsDensity": 1,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.4,
    "physicsObject": true,
    "physicsRestitution": 0.4,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "c1c52f82-5f83-4bce-b9e6-5693490d8ac3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 630.154053,
            "y": 117.545441
        },
        {
            "id": "dc11d074-16d6-4d1f-bcf3-2c7e9cfaafee",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 627.5679,
            "y": 1050.9248
        },
        {
            "id": "ae13c62c-b3c9-4b30-9661-206f3443248b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 539.188538,
            "y": 1310.9248
        },
        {
            "id": "c826c769-9303-4e97-9164-0961f71fcbc7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 131.395447,
            "y": 1310.23511
        },
        {
            "id": "c6c9bd2a-d392-4dbd-bed4-89b7703bead1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32.53337,
            "y": 1036.442
        },
        {
            "id": "bcc37a2d-b3e3-429b-a3e6-219a311fd66d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 60.32647,
            "y": 96.71785
        },
        {
            "id": "0d260cd5-148a-4eb3-91da-27a092b7854d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 90.5678558,
            "y": 30.47647
        },
        {
            "id": "c0730ec4-fff5-4aa1-82e4-e34f20c154d4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 591.119568,
            "y": 31.2695732
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "389d2332-bb78-422a-8c00-94240f297bd7",
    "visible": true
}