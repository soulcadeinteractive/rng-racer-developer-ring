{
    "id": "c245fa16-6cb1-401c-8187-a62e793a15d1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_multiplayer_player",
    "eventList": [
        {
            "id": "ca75762a-b541-46ce-bda2-91749311a30e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c245fa16-6cb1-401c-8187-a62e793a15d1"
        },
        {
            "id": "1146d76d-1f4f-41bb-8038-5b71aadac5a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c245fa16-6cb1-401c-8187-a62e793a15d1"
        },
        {
            "id": "66b79d97-4072-44bf-ba19-7172465c70d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c245fa16-6cb1-401c-8187-a62e793a15d1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "79640a70-0e3c-4f51-905c-40e3024a13c5",
    "persistent": false,
    "physicsAngularDamping": 36,
    "physicsDensity": 1,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.6,
    "physicsObject": true,
    "physicsRestitution": 0.4,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "c9f132d8-e3c3-4686-820e-409114c0f5a5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 136.801682,
            "y": 5.084717
        },
        {
            "id": "17350196-0598-4057-9b8b-6a803fda5160",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 157.4627,
            "y": 32.1355667
        },
        {
            "id": "f13987f9-0f98-42cd-9556-9094e7db4ab0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 158.310165,
            "y": 254.1695
        },
        {
            "id": "04ff8833-57b9-4ce4-800f-5242effe4dbd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 133.733871,
            "y": 322.813568
        },
        {
            "id": "74fa1d9c-cf2f-46c7-909c-647eb5b45744",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 27.8016663,
            "y": 318.5763
        },
        {
            "id": "14779e4f-c8fd-45ae-965d-75dc84bbd910",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 7.46269226,
            "y": 258.4068
        },
        {
            "id": "6d3ded12-075d-42dd-a32d-1aefddad5a24",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 14.2423553,
            "y": 29.1864624
        },
        {
            "id": "fd7e1a5b-4bda-4893-903d-ae43b964ac75",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 30.6999817,
            "y": 4.525442
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "9a77cb1a-6b78-4e9a-b544-cc8bc26a7a1b",
    "visible": true
}