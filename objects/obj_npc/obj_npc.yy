{
    "id": "828330ef-223f-414c-afd3-5e00cdfbe79d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_npc",
    "eventList": [
        {
            "id": "4757d084-4dd4-47c8-9416-249c07956c5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "828330ef-223f-414c-afd3-5e00cdfbe79d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "79640a70-0e3c-4f51-905c-40e3024a13c5",
    "persistent": false,
    "physicsAngularDamping": 36,
    "physicsDensity": 0.7,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.8,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "88116419-cd73-4790-8fbf-b389614e4214",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 502.008423,
            "y": -3.49998474
        },
        {
            "id": "a73ae1a7-bb49-4ca9-a554-faa5d45e8563",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 642.0084,
            "y": 81.50003
        },
        {
            "id": "f3316648-db17-40ae-b95a-39c8e9940a54",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 627.0084,
            "y": 1096.50012
        },
        {
            "id": "820da60f-6396-4e7b-a61e-529be66d09a6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 542.0084,
            "y": 1296.50012
        },
        {
            "id": "9936f7f3-dfc3-492a-b595-f23a3af6d1c8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 112.008362,
            "y": 1296.50012
        },
        {
            "id": "bbf1427c-f5e2-435f-8860-7b83a337abad",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 37.00836,
            "y": 1096.50012
        },
        {
            "id": "1b626a4c-465d-4fbc-bc59-6bc93ef6aaec",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 52.00836,
            "y": 91.50003
        },
        {
            "id": "401363df-c9c9-4aeb-a119-103d638e735f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 192.008362,
            "y": -8.499985
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "389d2332-bb78-422a-8c00-94240f297bd7",
    "visible": true
}