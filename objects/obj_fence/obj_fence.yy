{
    "id": "41728e89-67cb-47bc-a1b4-5ca98ed06147",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fence",
    "eventList": [
        {
            "id": "de9e6cb4-48d2-4ee7-a329-eca857ad31cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "41728e89-67cb-47bc-a1b4-5ca98ed06147"
        },
        {
            "id": "cfebb35d-97c7-4017-bcb6-135f118cdbee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "41728e89-67cb-47bc-a1b4-5ca98ed06147"
        },
        {
            "id": "ab1a02bf-5da4-44df-ab47-fa0f898f0269",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "79640a70-0e3c-4f51-905c-40e3024a13c5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "41728e89-67cb-47bc-a1b4-5ca98ed06147"
        },
        {
            "id": "e3e1e91a-aff5-4c0d-822d-f5fb68828131",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "41728e89-67cb-47bc-a1b4-5ca98ed06147",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "41728e89-67cb-47bc-a1b4-5ca98ed06147"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 50,
    "physicsDensity": 50,
    "physicsFriction": 1,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 50,
    "physicsObject": true,
    "physicsRestitution": 1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "aeffe253-8744-4adf-a5d7-87f22616543b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "9644bb51-5299-494c-8a5d-f525c3cdf058",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 0
        },
        {
            "id": "39c7e1e6-eb8b-454f-a02a-e56b8ca1a3d7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 512
        },
        {
            "id": "a2e91218-f7c8-4145-8e90-33e55774f311",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 512
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "51bb04d7-0437-4da0-924e-3a4e3d65e2bf",
    "visible": true
}