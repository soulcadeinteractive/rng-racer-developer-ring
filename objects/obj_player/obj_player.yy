{
    "id": "aad799b6-c9e4-4a88-ab6b-014d54ba38ff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "6b0b929f-bea7-41b2-af48-d4eaab268c0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "aad799b6-c9e4-4a88-ab6b-014d54ba38ff"
        },
        {
            "id": "2e1250dc-814c-4565-aede-be28132eeb80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aad799b6-c9e4-4a88-ab6b-014d54ba38ff"
        },
        {
            "id": "82cf98f9-b308-47e6-91fe-296c17024863",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "aad799b6-c9e4-4a88-ab6b-014d54ba38ff"
        },
        {
            "id": "d4cfa75d-d3e6-46fe-b9dd-fbcb7de6c4bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "aad799b6-c9e4-4a88-ab6b-014d54ba38ff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "79640a70-0e3c-4f51-905c-40e3024a13c5",
    "persistent": false,
    "physicsAngularDamping": 36,
    "physicsDensity": 1,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.6,
    "physicsObject": true,
    "physicsRestitution": 0.4,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "4cbb9e39-b3ff-4c9c-9a03-1736af198cf8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 125.726624,
            "y": 1.53104877
        },
        {
            "id": "9ce4f392-71b7-4bee-ae19-afda8fc0954c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 157.051941,
            "y": 31.65153
        },
        {
            "id": "6f6a2aa8-241c-4c3f-9c0c-34488c775136",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 157.051941,
            "y": 253.338272
        },
        {
            "id": "6ec18b6e-a63f-4feb-9bf9-ea61390117d0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 134.16037,
            "y": 320.808167
        },
        {
            "id": "4c20b9b5-8041-45bc-9bae-64b0fe8d474e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 31.7507172,
            "y": 323.2178
        },
        {
            "id": "937c8d83-f56a-453f-9d50-4041d71331b9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 7.65432739,
            "y": 255.74791
        },
        {
            "id": "47839cfa-83f3-45c5-a997-d4b8fe88e6b6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 10.0639648,
            "y": 29.2418919
        },
        {
            "id": "a8825bfc-77a4-4e57-a6ce-604485468c02",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 36.5699921,
            "y": 0.3262291
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "9a77cb1a-6b78-4e9a-b544-cc8bc26a7a1b",
    "visible": true
}