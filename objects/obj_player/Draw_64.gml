/// @description Insert description here
// You can write your code in this editor

var xadjust = 0;
draw_set_color(c_white);
draw_text(64 + xadjust,64,"X Position: " + string(phy_position_x));
draw_text(64 + xadjust,128,"Y Position: " + string(phy_position_y));
draw_text(64 + xadjust,192,"Speed: " + string(phy_speed));
draw_text(64 + xadjust,256,"Boost: " + string(boost));
draw_text(64 + xadjust,256+64,"Screen Shake: " + string(camera_shake[0]));
draw_text(64 + xadjust,256+128,"Camera Z: " + string(camera_z_distance[0]));
