/// @description Insert description here
// You can write your code in this editor
forward_x = 0;
forward_y = 0;
globalvar debug;
debug = false;
shake = 0;
throttle = 0;
boost = 1000;

max_speed = 128000 * 2;
gear_count = 2;
gear = 1;
physics_world_update_speed(360);
physics_world_update_iterations(30);
event_inherited();

obj_3d_camera.camera_follow = id