{
    "id": "2fda5e31-523d-49b8-a17c-028d3952e990",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_npc_multiplayer",
    "eventList": [
        {
            "id": "e96444a4-98b8-4044-9b46-38a03fa978fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2fda5e31-523d-49b8-a17c-028d3952e990"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "79640a70-0e3c-4f51-905c-40e3024a13c5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.7,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "4341c3a7-f9b8-4afa-b2c7-33b355183d72",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 8
        },
        {
            "id": "dc4fc6d7-aee7-4af9-a26c-cea4872683b8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 80,
            "y": 8
        },
        {
            "id": "502ec4a1-0714-4922-9fb8-8d789522cc9a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 80,
            "y": 156
        },
        {
            "id": "3e3daff9-6121-4583-a2d3-c3ed7d225555",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 156
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "389d2332-bb78-422a-8c00-94240f297bd7",
    "visible": true
}