/// @Syncing track generation
// You can write your code in this editor
col=c_red; //grid color
dcol=c_red; //your default color
gw=1024; //the width of each cell in the grid
gh=1024; //the height of each cell in the grid
w=3; //the width of the grid lines
yy=0; //the starting y
xx=0; //the starting x

bcol=c_blue; //grid color
bdcol=c_blue; //your default color
bgw=3072; //the width of each cell in the grid
bgh=3072; //the height of each cell in the grid
bw=6; //the width of the grid lines
byy=0; //the starting y
bxx=0; //the starting x



/*enum point_of_contact
	{
	center,left,right,
	}
	
enum points_of_contact
	{
	center_to_center,
    center_to_left,
    center_to_right,
    left_to_center,
    left_to_right,
    left_to_left,
    right_to_center,
    right_to_right,
    right_to_left,
	}
*/
sd = 3072
var track_shape = 2;

switch(track_shape)	
	{
	case 1:
	var a1,a2,b1,b2,c1,c2,d1,d2,e1,e2,f1,f2;
	a1 = choose(point_of_contact.left,point_of_contact.left,point_of_contact.center,point_of_contact.right,point_of_contact.right);
	a2 = choose(point_of_contact.left,point_of_contact.left,point_of_contact.center,point_of_contact.right,point_of_contact.right);
	b2 = choose(point_of_contact.left,point_of_contact.left,point_of_contact.center,point_of_contact.right,point_of_contact.right);
	b1 = a2
	c2 = b1
	c1 = choose(point_of_contact.left,point_of_contact.left,point_of_contact.center,point_of_contact.right,point_of_contact.right);;
	d2 = c2
	d1 = choose(point_of_contact.left,point_of_contact.left,point_of_contact.center,point_of_contact.right,point_of_contact.right);
	e2 = choose(point_of_contact.left,point_of_contact.left,point_of_contact.center,point_of_contact.right,point_of_contact.right);
	e1 = d1
	f2 = a1;
	f1 = e1;
	
	scr_generate_upward_right_section(0,0,a1,a2);
	scr_generate_upward_left_section(sd,0,b1,b2);
	scr_generate_vertical_section(sd,sd,c1,c2);
	scr_generate_downward_left_section(sd,sd*2,d1,d2);
	scr_generate_downward_right_section(0,sd*2,e1,e2);
	scr_generate_vertical_section(0,sd,f1,f2);
	
	break;
	
	case 2:
	var a1,a2,b1,b2,c1,c2,d1,d2;
	a1 = choose(point_of_contact.left,point_of_contact.center,point_of_contact.right);
	a2 = choose(point_of_contact.left,point_of_contact.center,point_of_contact.right);
	b1 = a2;
	b2 = choose(point_of_contact.left,point_of_contact.center,point_of_contact.right);
	c1 = b2;
	c2 = choose(point_of_contact.left,point_of_contact.center,point_of_contact.right);
	d1 = c2;
	d2 = a1

	
	scr_generate_upward_right_section(0,0,a1,a2);
	scr_generate_upward_left_section(sd,0,b2,a2);
	scr_generate_downward_left_section(sd,sd,b2,c2);
	scr_generate_downward_right_section(0,sd,a1,c2);
	
	break;

	}