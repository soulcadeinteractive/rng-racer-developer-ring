{
    "id": "29ea22f9-e8e9-423c-ae0f-e0190d3cf29a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_multiplayer_track_generation",
    "eventList": [
        {
            "id": "4e6896f4-bbb1-4cd6-b944-e118a22ee6e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "29ea22f9-e8e9-423c-ae0f-e0190d3cf29a"
        },
        {
            "id": "074942a5-4f47-4615-987c-b5822519b704",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "29ea22f9-e8e9-423c-ae0f-e0190d3cf29a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}