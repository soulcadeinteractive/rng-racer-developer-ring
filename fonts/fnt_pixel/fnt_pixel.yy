{
    "id": "073903ea-bfca-4617-b5b4-bc9fa91afd46",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_pixel",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Pixellari",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "fee45a30-f654-4b5f-8bda-e9f47e372e50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 52,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 360,
                "y": 56
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "712c749b-d804-42d6-830d-5ec7dff47e2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 36,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 446,
                "y": 164
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6f530563-53b9-4bff-85c0-50ef2e73fffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 429,
                "y": 164
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d4fdedf7-834e-4974-aa23-4ae59cfa3c06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 36,
                "offset": 0,
                "shift": 27,
                "w": 24,
                "x": 262,
                "y": 56
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "197b52e8-a121-42aa-b391-9acdc79748af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 39,
                "offset": 6,
                "shift": 27,
                "w": 18,
                "x": 140,
                "y": 110
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0b02081b-245e-4c0f-9ff8-ecacb37c0510",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 36,
                "offset": 0,
                "shift": 42,
                "w": 39,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1856c155-de7a-4203-901e-911e9bb3532b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 36,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 236,
                "y": 56
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b2117ee1-c60a-42ca-8111-06af49b24313",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 497,
                "y": 164
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6b77e052-1328-48f0-82ec-a94b21374fb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 39,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 341,
                "y": 164
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a9a3b5bc-da3a-4a50-a006-e2b852178e77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 39,
                "offset": 6,
                "shift": 18,
                "w": 9,
                "x": 374,
                "y": 164
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "7954fd61-0816-4e25-a27a-a793cb63e98e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 385,
                "y": 164
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "18301d6f-63e7-4f5a-8c44-69b665cdaa97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 30,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 236,
                "y": 164
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a386f81c-a8de-496f-bd88-ea2c69b5b9fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 42,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 421,
                "y": 164
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4f21f9c7-eeda-44a9-99de-e8f01e3aedc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 24,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 321,
                "y": 164
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9736265f-6844-4943-a635-6a73a1947d32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 36,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 470,
                "y": 164
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f077eb30-edcc-4f03-92f5-0f797ab59103",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 122,
                "y": 164
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f81f1dfc-6298-480b-bb17-8477182b5f96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 36,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 446,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "57980ea4-b127-49a6-acce-4102bd018fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 36,
                "offset": 6,
                "shift": 27,
                "w": 18,
                "x": 400,
                "y": 110
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "cce26d69-d189-4e7b-926d-75896a431e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 380,
                "y": 110
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "5aff5579-e20f-4b58-bd52-c28855be8b8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 420,
                "y": 110
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7a924999-2e4c-4977-8f31-6de1977cfdae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 36,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 400,
                "y": 56
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "4b522d75-ca6f-4e58-b569-cdf4e0028fb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 440,
                "y": 110
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0f5abd84-3e59-48d8-890a-1b326e60661a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 460,
                "y": 110
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "aa9cf06e-4d19-4eb3-a5f5-6416867f7fbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 36,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 48,
                "y": 110
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f9c106bf-3709-40e7-84c6-5d6dabf973bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 22,
                "y": 164
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "58235dd6-b99d-4c76-8e06-d0188242ecdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 82,
                "y": 164
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a634bae3-66db-4344-b564-18d2938b1ac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 36,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 454,
                "y": 164
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a7090fae-fbff-4a64-8766-eebff3bfda46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 42,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 413,
                "y": 164
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a0c59fda-a35a-40cf-ba69-9276bc74447f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 0,
                "shift": 33,
                "w": 30,
                "x": 299,
                "y": 2
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "615f5e5a-09a0-42b3-8f69-359125bb0ea3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 30,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 216,
                "y": 164
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d9d5551c-82e9-4776-a8be-225d2e256a1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 0,
                "shift": 33,
                "w": 30,
                "x": 267,
                "y": 2
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7f2dcee3-5ac1-40e1-bd4f-7a0cf03b9d57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 160,
                "y": 110
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "7845ad1a-5060-4c9b-961d-78219883bca7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 45,
                "offset": 3,
                "shift": 42,
                "w": 36,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "614db0ad-b5ca-4cc9-b3d4-afa7f931c6ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 36,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 54,
                "y": 56
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "bace1e77-e013-4e44-b28c-22ad75cd3da4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 36,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1d7ae108-2366-49a9-82b4-26d0097e172b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 36,
                "offset": 6,
                "shift": 30,
                "w": 21,
                "x": 94,
                "y": 110
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a1df69e5-4a3a-4f88-baf6-b886e6eec33a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 36,
                "offset": 6,
                "shift": 33,
                "w": 24,
                "x": 458,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "dbe15885-71f6-47df-85f6-09e7df221f50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 36,
                "offset": 6,
                "shift": 27,
                "w": 18,
                "x": 62,
                "y": 164
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "054463ff-000f-42bf-b893-26f23bec9c43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 36,
                "offset": 6,
                "shift": 27,
                "w": 18,
                "x": 42,
                "y": 164
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9f382693-2b8f-4d17-8514-9d8cfa43ec0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 36,
                "offset": 6,
                "shift": 30,
                "w": 21,
                "x": 25,
                "y": 110
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "de988871-f7aa-4f88-9d79-0716db406ed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 36,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 484,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8f1d369c-5395-47f8-b342-c4406d89480e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 36,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 293,
                "y": 164
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "7d5e32bb-b403-490b-80e7-73ea93699225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 21,
                "x": 469,
                "y": 56
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6e84c8e7-585c-463b-8d65-00cd7b42fa9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 36,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 28,
                "y": 56
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "252cf61c-3aa4-4c6f-924e-217e08d6a28b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 480,
                "y": 110
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "339f8269-fb52-4a5a-87aa-bb5c3e4e6de0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 36,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "596a08fa-1656-4452-98b9-96ad09b5ec43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 36,
                "offset": 3,
                "shift": 33,
                "w": 27,
                "x": 360,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b415b9f8-7da1-4a61-b38b-4538b26170f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 36,
                "offset": 6,
                "shift": 33,
                "w": 24,
                "x": 314,
                "y": 56
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a569e8a2-ba0c-4647-a1e8-a40693b89892",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 36,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 423,
                "y": 56
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "1d332b8c-f290-4e72-bb1c-cfb980899676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 36,
                "offset": 6,
                "shift": 36,
                "w": 27,
                "x": 331,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "964d8797-bf5b-4e17-bea6-624bce9bceff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 36,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 288,
                "y": 56
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e55be489-2d6e-4f00-a860-be247e452c21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 36,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 106,
                "y": 56
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9105009c-ede9-4bb1-9909-73153e81848e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 36,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 132,
                "y": 56
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6aa803d6-ebd5-41bd-8d69-2750a13cc7aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 36,
                "offset": 6,
                "shift": 33,
                "w": 24,
                "x": 158,
                "y": 56
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "77bd782e-c068-4f94-b854-92d4afbef9ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 36,
                "offset": 6,
                "shift": 33,
                "w": 24,
                "x": 80,
                "y": 56
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "8a200cdb-deae-4562-ad5b-27364affcfc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 36,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0a804152-b90a-4966-853e-7f56466eab99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 36,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 184,
                "y": 56
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "fbcba5f8-8aff-452c-bf63-e8256b4236b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 36,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 210,
                "y": 56
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0298f942-05fc-4eaf-bd66-270aa712be2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 36,
                "offset": 0,
                "shift": 33,
                "w": 30,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6a8f6dbe-1fae-4fed-895f-82541fe38d89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 39,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 363,
                "y": 164
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e16a5d54-286e-4000-abe0-d70a6d5fe31f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 240,
                "y": 110
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8ebc7a8d-48dd-4e3e-886c-c430b9073acf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 39,
                "offset": 6,
                "shift": 18,
                "w": 9,
                "x": 352,
                "y": 164
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "597c28e0-a4c9-4dc4-a789-b89b0f566fbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 0,
                "shift": 24,
                "w": 21,
                "x": 256,
                "y": 164
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "788e633d-65b0-4e3e-a43e-57b06075a308",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 45,
                "offset": 0,
                "shift": 27,
                "w": 24,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "01ca6564-c4cc-4ca5-bff9-839765ba5a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 12,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 486,
                "y": 164
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "704c0945-dcb5-433f-8675-ea88a094a38f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 340,
                "y": 110
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "baae15ea-87ce-41e8-8a4b-d20354c14a55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 36,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 71,
                "y": 110
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "63fc9a71-87ce-4bf2-bbf3-75a46511f5ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 180,
                "y": 110
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "640c5679-1d0d-478e-bf3f-044ff10abf4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 36,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 117,
                "y": 110
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "538cfbf2-bb45-4e3e-a02b-d8245ce96f37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 260,
                "y": 110
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "963018cb-fb32-4116-85f4-0e4667a48d55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 36,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 279,
                "y": 164
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "042ef9a9-629b-411a-a06f-cab9f86002c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 435,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3498dc40-dbe5-4fe3-8715-3c1ff187eb18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 36,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 377,
                "y": 56
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3426144e-dd8b-4b30-953f-140ff6c862c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 36,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 478,
                "y": 164
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "aa49d100-7c1a-4abe-90e1-398c31913133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 45,
                "offset": -3,
                "shift": 12,
                "w": 12,
                "x": 202,
                "y": 164
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "cd7f1969-41db-4641-b013-16ca5fcc9d1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 21,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d2135279-695f-41d0-8b68-75d329e62103",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 36,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 462,
                "y": 164
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ba25da33-0493-460e-81c2-359b3cc43618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 36,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0e15837e-10f5-41f4-9fdd-d0edcc04e364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 200,
                "y": 110
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "8a9eb14c-1599-4865-974c-9c6771597733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 220,
                "y": 110
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2bdf2896-7bf2-44b1-9e97-a75535a722bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 389,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e4e39b46-6517-4ac1-ba0b-8939b2e8a78a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 412,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0a5d005c-2a20-4a10-83f0-0fc2144297b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 280,
                "y": 110
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "648bd845-8516-4a29-99fc-e9829d7fe997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 300,
                "y": 110
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8643ae48-8d47-428b-88b7-ff0a7250d953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 307,
                "y": 164
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "722d6d86-2017-4fec-8aee-173af82a1d9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 320,
                "y": 110
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ff548284-298a-4660-a9ab-629c2d05775b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 360,
                "y": 110
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "90ff6770-ea47-449f-a88c-16b8b4ac2c36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 36,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7b6966a3-1e16-462f-be83-95b30bd95b97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 2,
                "y": 164
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f0033c4c-514f-4455-bf0f-ac7f6adc8d10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 45,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 340,
                "y": 56
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "59c30700-d885-4c7c-a08f-e7146330390b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 36,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 102,
                "y": 164
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7a17465f-725c-4d4f-926b-b5e163928067",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 39,
                "offset": 0,
                "shift": 18,
                "w": 15,
                "x": 159,
                "y": 164
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7912860a-284a-4447-ad5c-c7c080dbd30c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 42,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 405,
                "y": 164
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "8261382c-9698-444c-b1a8-71d48e32edc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 39,
                "offset": 0,
                "shift": 18,
                "w": 15,
                "x": 142,
                "y": 164
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a3e83770-e167-487d-a490-6b96e8e84c45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 24,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 176,
                "y": 164
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 36,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}