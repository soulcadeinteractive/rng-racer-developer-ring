{
    "id": "60becef5-18b8-4e6d-9a92-f17a1f1c3808",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_lemon_milk",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Lemon\/Milk",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "124d2209-8556-4d3e-9fe1-23d84cbc7ce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 32,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 54,
                "y": 152
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e961a952-75cd-4527-9b73-e2837b903cfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 196,
                "y": 152
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "91877016-61cb-4108-99a3-09856c211f71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 210,
                "y": 152
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3488a09a-3b35-42ea-973a-cc5f2159abc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 27,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 90,
                "y": 63
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "13b960a0-2e04-4b8b-91c9-743ab4337727",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 78,
                "y": 121
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "30125f43-ae63-4796-adaa-8c089da9cfdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 27,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 141,
                "y": 33
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a8bcd3ce-1642-44d8-9481-9f7edd89ded5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 27,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 160,
                "y": 33
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "80c71281-b6a7-49a2-8210-1fe7517930ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 245,
                "y": 152
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2524ffc9-8513-48a6-a158-14b09b747ea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 132,
                "y": 152
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4f2f8136-3c9e-4ae0-ae44-dc75e0f0279e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 123,
                "y": 152
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "038e07b3-2cc8-48d8-8fcf-6fab1a29a38c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 152
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "64322ee8-10eb-4640-ba54-218392f204b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 29,
                "y": 152
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "317fd633-bb6a-4b81-a1da-3f6974af561c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 29,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 162,
                "y": 152
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4f3e6fd7-85a4-4761-bef9-3005b9b13580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 99,
                "y": 152
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5a1631c3-46d7-4cd9-b797-20533d6d95fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 27,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 203,
                "y": 152
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "fdcb0897-09b2-4621-9c3f-f632ddca8dff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 92,
                "y": 121
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3a538d53-11db-431c-a3ed-8d586890a3a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 27,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 107,
                "y": 63
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "9141d93b-353c-4e85-b647-296b6fad3a80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 27,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 170,
                "y": 152
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d7f185f9-8b22-4fcb-b39f-56bfffa024f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 146,
                "y": 92
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "dbf4f924-8038-4ed1-b92b-0b69b81742ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 63,
                "y": 121
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "83ce95f6-a557-414f-a05d-c5b4ad41d2af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 27,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 20,
                "y": 63
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "904e4f47-c6b0-45d1-8fc3-b6156cbbe707",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 27,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 66,
                "y": 92
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6421db02-a0b1-40a1-865c-b9e8eaa81281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 192,
                "y": 63
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "21a2ef2d-22e4-4399-9d7e-429bd55537d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 50,
                "y": 92
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "40417022-f269-4668-bbb3-6321fd343ef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 27,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 33,
                "y": 121
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ecd9d391-9536-4179-906d-f1cfcf19568f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 210,
                "y": 92
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b728d90f-4d54-4794-9cf1-dfea7dc3f31f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 189,
                "y": 152
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "88d39c8f-4cd5-4bb2-863d-8f55f86455d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 154,
                "y": 152
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a4758c1c-40db-4cee-9484-0c790d686ea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 124,
                "y": 63
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "efbd6ac6-be05-40be-a96e-6fbceba57a7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 42,
                "y": 152
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8b2986e6-0342-4c8d-862b-0afbce1c88c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 73,
                "y": 63
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "279d8a00-ac5e-496f-be91-7087dde34080",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 106,
                "y": 121
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8c7205d2-d501-4d27-ae62-25834b821458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 29,
                "offset": 1,
                "shift": 25,
                "w": 24,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "4c2e5c0c-91c8-4de2-8eb6-c4c91acc5a2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 27,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 44,
                "y": 33
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "948251f8-d443-4e13-a9c9-9b4fc1b8c7ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 18,
                "y": 121
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3ba35841-0182-46d4-80ee-9019a5d6a689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 27,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 198,
                "y": 33
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "5f859430-a0fd-4711-a879-7663963c3bf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 27,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 92
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8ceca8ec-ec71-4465-b9f1-8e2ba2af3bbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 27,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 190,
                "y": 121
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "51acb080-da70-4e82-825f-70e97391e653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 27,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 134,
                "y": 121
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f5c7a197-cf20-4f80-b514-e8faf64f9ed4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 27,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8637988d-456f-447b-9801-ef4944dbc0a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 130,
                "y": 92
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f9c84251-b734-4611-b44d-16d9e21c9592",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 27,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 220,
                "y": 152
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1b039814-c2c9-4703-bd69-f2fa45ef1ed9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 218,
                "y": 121
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "7a04b41d-5462-45dd-9e0d-f7ddb8a19b50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 34,
                "y": 92
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a480e0c8-ed09-482f-81bf-441ba3b64985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 27,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 148,
                "y": 121
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a45718a9-89f2-4b64-910f-08bbe1f59b69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 27,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "51d156aa-6445-4407-8e59-ec69edf635e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 27,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 175,
                "y": 63
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b489d20b-9c3c-48b1-9eb1-2cbf187da5dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 27,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2738a763-fdb0-47b4-bbfb-e161fcd523c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 92
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3d1f2072-d786-4a4c-b15f-766fc2a55498",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 27,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d7b16f45-1445-4167-9e90-7c30aab3bbab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "08dfc47a-7115-406d-a5d6-96187af4d8f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 27,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 120,
                "y": 121
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "28425764-6702-447c-844d-20335797767d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 208,
                "y": 63
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1dc98baf-ecfe-4ecf-819a-fd4e26f7fe89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 27,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 141,
                "y": 63
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "1f819a14-5da1-4079-a3f7-832b22008de0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 28,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 84,
                "y": 33
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "01bb9003-0b85-4470-aeb3-75d44e1ac78a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 28,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fa7650f1-ace2-4189-a27c-4bf2ce797c08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 27,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 63
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "4dec355d-c4c2-4d08-8baa-34fdc7968663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 27,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 235,
                "y": 33
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "13640ee2-cff6-4519-98e5-bc7c28b69da7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 226,
                "y": 92
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "58304841-fc6b-4071-8b5e-b7800f9792bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 75,
                "y": 152
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "98e51d91-1856-49f5-8bab-c95055fd684d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 16,
                "y": 152
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7fa54c09-768c-4bcb-a767-e8220710c51f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 87,
                "y": 152
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "cb2c21b0-da02-4d56-ad96-ad0f20e06ef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 141,
                "y": 152
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a1c42279-270d-42f7-b68c-affd82ba2021",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 28,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 122,
                "y": 33
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "fb58c787-94ad-429f-8154-bd66705a1522",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 232,
                "y": 152
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2f213a01-8ec5-4eb4-890c-02500d1ef1a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 27,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 64,
                "y": 33
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6e46d49b-11dd-46dd-a7b3-00d8dd158b24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 48,
                "y": 121
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4307f305-ebb6-4295-bf59-90d85086d0f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 27,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 179,
                "y": 33
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "47957121-dcfb-4404-8dce-0e8fa6bc9c54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 27,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 92
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9c4596f1-68fb-425e-929e-6833c9122c41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 27,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 232,
                "y": 121
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "11e349c6-4a8d-4700-a661-93592e48edad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 27,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 204,
                "y": 121
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "17d5b83c-a2de-45d0-9a7d-a111a7fb5a2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 27,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a4d8be43-b2ff-4a57-a992-2d341a96e700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 194,
                "y": 92
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "2499620b-6034-4cd5-b7aa-7e9df3446b87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 27,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 226,
                "y": 152
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "016be3ba-0b7b-46a3-8a8f-e49f4d5c4864",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 152
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "71d044ba-19c3-4854-9e51-874793a4c223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 82,
                "y": 92
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "fc68c22e-d658-42c2-b7d2-c309ffc85b12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 27,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 162,
                "y": 121
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6b7edf77-cb9b-4400-9afb-1888cff8e994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 27,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "89d496d4-60bc-4d22-b0c2-22098fe29122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 27,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 56,
                "y": 63
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "4c19a4a8-399a-436d-a88b-d4eec436aa24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 27,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 23,
                "y": 33
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "45b1e0ac-e229-4d6e-8eab-036a206b85af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 121
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "901cf81a-fd73-470e-b8c5-fa0ece4ddd5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 27,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 2,
                "y": 33
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b6583acb-cc14-48e1-b150-c3ce6a1bd574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 224,
                "y": 63
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "dc42fd71-f1f4-40a0-b9d5-03b846f3799f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 27,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 176,
                "y": 121
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a0eab8b3-cb24-4a2d-8c55-b0aee67ef664",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 114,
                "y": 92
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "94f2fa99-203b-41b8-9c5f-287021a5679f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 27,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 158,
                "y": 63
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "6abc88aa-a209-4dfc-998f-135d18aed854",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 28,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 103,
                "y": 33
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f4b702f8-63ff-43e1-97e0-1a266ef05ddc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 28,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "fc2a0879-824c-441d-ba02-1ef6e14639d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 27,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 217,
                "y": 33
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d0661a30-3069-4350-b0c6-ba26b9960f29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 27,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 38,
                "y": 63
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "40ad9387-f473-450c-96b7-f73e71bad311",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 98,
                "y": 92
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "65c5273e-0ff2-42da-8a0f-9a523dc0442a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 111,
                "y": 152
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "8a174c18-be71-4339-9939-e1d1de69ac47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 29,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 240,
                "y": 152
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "390937a1-0962-4dd8-950e-e3307d1be5af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 63,
                "y": 152
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "fb5c57c9-a0c3-42f6-8b68-4b2a6425a3e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 26,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "fcd37c85-68f1-4b31-bf00-5c959e44dd7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 25,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 100,
                "y": 2
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 18,
    "styleName": "",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}