{
    "id": "4af73bed-0750-4686-9a76-1ca453152480",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_bebas",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 1,
    "first": 0,
    "fontName": "Bebas Neue",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d5696f6a-72e0-4297-898f-284926aa1125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 32,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 143,
                "y": 138
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2aac1041-ed5b-4fcd-b504-0bf29f9fdbee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 24,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 204,
                "y": 138
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "55c91a54-8df9-41bd-a392-38193142e708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 9,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 222,
                "y": 138
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "9498e224-8771-4659-97ff-7af3a1b9eb74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 62,
                "y": 36
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "34861299-7762-4f55-bcf7-0ba73fc66ed2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "319277ff-6557-4849-8de5-5fa1fe87c0da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "35b223e5-fd45-4e6f-9bae-aeb4f704a537",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 107,
                "y": 36
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "991c5591-1cb5-4787-a45c-9164179ef364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 8,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 233,
                "y": 138
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "9eeba66a-e12a-4511-8f16-7660ead4e434",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 116,
                "y": 138
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f2742a14-fb75-423a-b036-e7f021312512",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 125,
                "y": 138
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8b149e26-7fc5-45b1-8bfc-1f46aa146ca5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 83,
                "y": 138
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0f247617-299e-4f33-b790-bec1a115e9c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 217,
                "y": 104
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "19b1161d-ac55-443a-a32a-b840d4b2f62b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 27,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 171,
                "y": 138
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a22ce959-77f5-4074-90c1-5227ce832881",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 177,
                "y": 138
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6dd8eda3-d1d2-4a42-8ad3-ad511d9be984",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 24,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 216,
                "y": 138
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a0300368-1a6c-49de-8f58-cd0369f93e11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 47,
                "y": 36
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0f6ec46d-a4b1-4762-8d1c-800e5c7e517b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 184,
                "y": 70
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "61c7bdf8-bfb3-4f55-a5e0-58c361c92d20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 24,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 134,
                "y": 138
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "dcccbf3f-7e78-4ca6-8fc3-127d215894c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 93,
                "y": 104
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9d735879-7fd3-453d-a79c-597846c65b52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 80,
                "y": 104
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0a8b6640-981a-4a58-9d1f-620036637402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e42df8c1-2265-4bf2-80dc-2d7ee393def7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 15,
                "y": 104
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5578ce59-5fca-4162-b5a6-6d97a20e9e51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 28,
                "y": 104
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "3bb93141-67d6-4f3c-8333-a11587e80438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 119,
                "y": 104
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "e72a57b5-0cda-4665-a2c8-0b22c2176eb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 70
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "bcd453c3-cd0e-4952-88dc-f1d0476cd654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 226,
                "y": 70
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "adab6fbf-d92b-4900-b7f6-ce4e82a539a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 24,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 192,
                "y": 138
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d35e3d7d-8968-401f-ad13-2be0c101a3e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 28,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 165,
                "y": 138
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "03265305-1cad-4159-950c-69996478ceac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 231,
                "y": 104
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "dd7f2e11-d82e-4509-8373-d67a40c49841",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 70,
                "y": 138
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3bec5d34-f1c5-40a9-91ce-35f1b4402677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 138
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "90e21807-a615-46d6-bcfb-7bf42c0033b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 106,
                "y": 104
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "aded7ae9-0d1e-4d24-b9cf-d2a7fc2a880c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "de7db4a5-c5f4-4f58-91e5-10e0d0f8032b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "66024a9e-46d7-4d2f-8ee9-4d5b13f3f741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 70
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "61645d15-e2cf-48f2-af7f-ec3be8befeb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 212,
                "y": 70
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "52f6e5fd-f991-4421-8a7d-32c729deee8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 70
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "23c1e1ec-c484-472d-b64c-956ad3b5d7d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 169,
                "y": 104
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4b08fa49-c9c9-427d-8566-62627a7e0bcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 145,
                "y": 104
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "bb7f8b0c-0b47-42f8-9656-094af54bd927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 219,
                "y": 36
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "02378274-ac4a-49e7-97c7-bf0a509b2d14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 121,
                "y": 36
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4b8d36e9-2866-4eab-86e0-cec8047aec43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 24,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 198,
                "y": 138
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c4f31bba-4a2e-436e-a0cb-f2e3cc667272",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 60,
                "y": 138
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ac6a2a2d-7ca9-4913-9a9f-3903e82cd934",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 163,
                "y": 36
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1e9d13b6-7b4f-4426-b963-4e72ad050c1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 193,
                "y": 104
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5994ce3e-dd96-4322-97b1-096b67d2cdd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ea7bafea-378a-4299-9365-c4015c1bb428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 149,
                "y": 36
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "81568c68-80e0-4e61-83d4-92eab8b3898b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 177,
                "y": 36
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "9a295a8a-9586-4364-9879-026aab54da94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 41,
                "y": 104
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b56ae1d5-1f0a-49bf-831b-3c8ecf63c232",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cb30a142-590a-4d86-a890-8179c3faf01e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 70
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f23bc399-5e44-48fd-a8b6-f627cc64a1c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 191,
                "y": 36
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "180389f9-51e3-4c4d-bf21-1d0b31e7cdc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 205,
                "y": 36
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c5e07502-fc2c-41a4-ae16-d10227bbdeb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 54,
                "y": 104
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "319a63ca-dc63-4cb3-a644-9ea34925b899",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "77454d7d-f9c3-4892-9406-0dd4b64a7d67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 24,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f4ed95c3-a768-442b-bcf1-aa6b90ef6f35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "87dc6a96-9e87-4b78-9d37-327ab0eff9e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 92,
                "y": 36
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "e18e9c76-1bad-4ca1-8309-3d786b527096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 233,
                "y": 36
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "44c400d4-8e1f-4078-8827-24fa09dc6660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 107,
                "y": 138
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d16671ba-be1d-4761-8761-421a70f81273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 32,
                "y": 36
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6b0c4424-59f6-43e0-b526-3ecda7e9bf0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 98,
                "y": 138
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "53b91644-693b-4d7f-ae33-fca62bb8f3eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 11,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 150,
                "y": 138
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "d3348959-7f39-48c4-838c-17f0037f9d6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 27,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "443d3f41-036b-43bf-9259-e9bccb92786a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 1,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 239,
                "y": 138
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a5dd2fb1-6fd6-4abc-8758-35d7f73883b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 77,
                "y": 36
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f3370e2b-8f7a-4a45-a8e3-bc8ccdc9ba79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 70
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0a6f2595-db08-49b3-92c1-4a77af223a1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 114,
                "y": 70
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "af76a3ce-d0b4-4f9e-aa12-cf9ca5e4263b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 70
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "894ba205-6551-4189-892f-5cce791f3f91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 157,
                "y": 104
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7c17f100-27b9-4aa6-86b0-d538379adcd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 205,
                "y": 104
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "22a2a405-d8af-4b7c-884a-13cd005a9b09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 16,
                "y": 70
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "fc022c86-a958-4b2c-a44f-6381db2f08e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "5a5718e7-6a3e-4d1d-9f59-9bcd9f90a845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 24,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 210,
                "y": 138
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f1adc026-4619-4875-b7bd-98943994c2a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 40,
                "y": 138
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5db276ea-5340-4dd4-9fa1-0dd4ba08950b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 135,
                "y": 36
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "147af39e-2874-4c7c-9418-8b4ef44b3aa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 181,
                "y": 104
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d2518ca3-183c-47f2-9b14-c2d286420098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 24,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "226d980f-8b48-4371-a768-ed11ed7c81b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 156,
                "y": 70
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "0a4dfdb2-fbe4-4481-b085-8d7c8fa8f088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 198,
                "y": 70
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "de0b4762-400c-44fe-a238-7e6304bf890a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 67,
                "y": 104
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "041436c8-708a-4538-9720-b09984c7cb0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2047b2c3-34cc-4d4b-9de6-f3959d217288",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 170,
                "y": 70
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "de5aed53-af4b-41a6-bf7b-389e9603fb44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 142,
                "y": 70
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0166cf89-a3e9-42dd-b91d-3b325526b46d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 128,
                "y": 70
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8a8fe9f4-a8e5-40a9-abc7-7755eeef84dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 132,
                "y": 104
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "cb629ebd-3d7a-4471-9eb3-dbf9c272d264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "82e8d3c7-e722-401d-a6da-327b3aaa464a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 24,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "502ea570-f274-4839-b6c9-ec4b930fa5eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "90f9cc49-9ca3-4fa6-b477-98fef25134d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 17,
                "y": 36
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2918a250-38f9-4286-a259-897c8185bf7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 100,
                "y": 70
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "af1ec092-7b2c-4d95-9d4c-263e330a8eb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 30,
                "y": 138
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a252fd2e-8069-4a71-9557-d248e6cdab62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 26,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 186,
                "y": 138
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bebecc96-5541-481d-8dbb-b8a8a1710780",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 50,
                "y": 138
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "545b7f68-d06c-48d3-8195-866d50638490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 15,
                "y": 138
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "2aaf3fc7-c708-4d68-ab4e-443611df5928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 84
        },
        {
            "id": "a9ef4c96-8672-45b9-937b-72cb00a2e2bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 86
        },
        {
            "id": "9e510602-08f5-4636-a5ef-e7ada65aaa87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 87
        },
        {
            "id": "947e1939-ffd2-42d5-8e97-1549dccc584c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 89
        },
        {
            "id": "cd2ea7ca-a85f-4e0b-932a-c11ecd4233a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 116
        },
        {
            "id": "f90784e4-91db-4275-a4b9-782b422c7cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 118
        },
        {
            "id": "8fc4201a-4d21-446d-910a-0cc57d7b9fc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 119
        },
        {
            "id": "55e2d5be-15b0-4b8d-b619-cce6457ec886",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 121
        },
        {
            "id": "39cd3460-de27-4747-b1ab-cb9c2b539f49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 221
        },
        {
            "id": "0e9efd06-1c4a-4870-86c3-f7b06ddfbc29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 253
        },
        {
            "id": "c6f9588d-1352-4aef-b109-60ef8957fc73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 255
        },
        {
            "id": "b85a7537-88f4-4492-8c0f-472955f8cfca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 354
        },
        {
            "id": "9d4b9318-89ed-4a8b-b2f5-66892ae1fafd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 355
        },
        {
            "id": "815135aa-e8f5-4972-a98b-3229216fba46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 356
        },
        {
            "id": "aaa33523-e215-4bd5-a376-e379e21cb109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 357
        },
        {
            "id": "64bf237f-0206-4d28-b0b0-f7f9bc66d212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 358
        },
        {
            "id": "687884e2-ade0-4f04-ae37-ab1fe3353047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 359
        },
        {
            "id": "497899cc-3b15-48cf-a664-ccd1e4c1ab2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 372
        },
        {
            "id": "cc64ab73-aac1-4f82-a6b4-69b99760d7b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 373
        },
        {
            "id": "62429114-8fa3-44b6-bede-c2de42e393dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 374
        },
        {
            "id": "c4098dcc-9a1f-40ce-bc24-7bbff47bee34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 375
        },
        {
            "id": "81da37e4-d0f3-470c-8936-6b09e4f79cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 376
        },
        {
            "id": "538d9a89-b493-4a5f-a7a6-bcc9c7477be4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 538
        },
        {
            "id": "a447a39f-2f22-4b6a-8e55-c5ac2bd9b687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 539
        },
        {
            "id": "1289c094-1131-4897-b2ce-33c94a7e19dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7786
        },
        {
            "id": "df8094d7-165d-47c3-a708-6d28c6d44c50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7787
        },
        {
            "id": "f4a470a2-4c7c-47b4-a1c1-8f41ef1f671c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7808
        },
        {
            "id": "8a588ce9-ff09-4a36-bdfd-13e8a8bb0e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7809
        },
        {
            "id": "7323b3da-995b-44f1-a657-5ffa629064aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7810
        },
        {
            "id": "32cc08aa-f7b7-45de-a5ea-09f47e4b426f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7811
        },
        {
            "id": "0dfa8a5f-3ea2-472b-9730-7ab972120611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7812
        },
        {
            "id": "c4389677-ac02-4c64-8de1-6b8a6bdb8152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7813
        },
        {
            "id": "cb90e91b-5766-49d8-b40a-b26ca07d1932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7922
        },
        {
            "id": "37ea21f7-bcd4-4005-8035-991d970f2389",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7923
        },
        {
            "id": "db8ecd7a-efa3-4c5b-961f-b668e5649264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 84
        },
        {
            "id": "dffd42d7-5805-4320-b26e-9e618714d683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "90445067-15a4-46c1-857c-c56c955fbddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 116
        },
        {
            "id": "686e1bf8-cf46-4eab-ab4c-fe4c78af1589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 121
        },
        {
            "id": "ce24653f-7e5d-4a3c-b048-b71419a4dfcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 221
        },
        {
            "id": "a6bed4e1-ae5d-4401-a7ab-78cac286a803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 253
        },
        {
            "id": "20832c7b-1e8a-4091-8d2e-3b7f1bc03c30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 255
        },
        {
            "id": "473a0a32-ec96-4475-823c-a283f50a6ff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 354
        },
        {
            "id": "260caac4-5fb7-4344-b262-9d87193fe0e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 355
        },
        {
            "id": "163f9c0c-123a-430b-94b4-3bd5167fef7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 356
        },
        {
            "id": "4a41e206-6275-410a-9d2e-c69e53459c32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 357
        },
        {
            "id": "c74bf64b-6378-4b05-8a15-715ef8dcc78e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 358
        },
        {
            "id": "cc28a536-578c-45e7-94b5-887756352c9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 359
        },
        {
            "id": "7f6b7f3c-a78e-4e33-85eb-3869a9cb048e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 374
        },
        {
            "id": "1b330248-feed-44f5-ac34-eda9c0cccbce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 375
        },
        {
            "id": "503c7c31-2674-469d-8437-537a93eb9e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 376
        },
        {
            "id": "ba058e3f-c627-4992-be62-2b22297289d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 538
        },
        {
            "id": "d6e924d8-acea-4fc5-a910-5b5378af8ab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 539
        },
        {
            "id": "a182b87e-2e4f-44f6-8776-4cf73159c635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 7786
        },
        {
            "id": "784cd201-44c4-4e3d-961f-d0bdbbed297e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 7787
        },
        {
            "id": "1d0f50e7-3669-4768-a49a-eb22fbc4bfc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 7922
        },
        {
            "id": "dd580d31-3892-4c8d-a44d-712d546fdbdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 7923
        },
        {
            "id": "b1c679a2-be06-4ee1-adb0-d680bf3b8922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 84
        },
        {
            "id": "bc896d00-bdea-42d1-b0ca-41475d537af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 86
        },
        {
            "id": "31ebe9d9-8691-40c4-8389-be6b93067637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 87
        },
        {
            "id": "1acc58e3-ebaa-4750-81be-ac67f89889eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 89
        },
        {
            "id": "3b9fbb45-0aad-42a4-9c08-a743e29fe757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 116
        },
        {
            "id": "f72fc3e8-8dcf-479f-8ee6-a0ed8704c967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 118
        },
        {
            "id": "1853b94a-b651-4664-8cc8-76ffaeb17505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 119
        },
        {
            "id": "5ce3b5eb-ced6-4a94-9125-ee36e08ad09d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 121
        },
        {
            "id": "9b66cad7-68f3-446d-bcc8-44754fe3b15f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 221
        },
        {
            "id": "5c30ddd7-62d5-46cc-91e7-8af5ba373ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 253
        },
        {
            "id": "5acbf4f8-02bf-4299-b626-abcaff3f022a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 255
        },
        {
            "id": "b9f3558f-960c-49d9-a826-25c39a36a269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 354
        },
        {
            "id": "43d806a5-7e74-4464-8312-fb090fc77bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 355
        },
        {
            "id": "2f51cc97-5daf-447a-8506-cf4d8b2873e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 356
        },
        {
            "id": "7b9f0443-9a25-4520-a96d-7c87f1203901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 357
        },
        {
            "id": "b54969d9-261d-4272-bfbc-aef8e939bbfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 358
        },
        {
            "id": "c1dbf6e3-76b8-4d2a-9611-ba02994dffe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 359
        },
        {
            "id": "4a1b67ab-c272-433d-874d-f0262967cc7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 372
        },
        {
            "id": "b8dcf428-1444-4853-9277-b4ccc5380303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 373
        },
        {
            "id": "883c39a1-8b9d-4fa7-8013-38b2d32b80fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 374
        },
        {
            "id": "bbdbc966-0290-49f7-8c1e-891bcb4178b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 375
        },
        {
            "id": "7218af2b-cd74-433a-b7f3-21a588994404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 376
        },
        {
            "id": "a2dedef7-13d2-41a9-9fa7-cd29a9310c05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 538
        },
        {
            "id": "75e7a941-56c2-479a-8421-5a8eb22cb55b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 539
        },
        {
            "id": "f599be13-4327-4a67-a482-543b81c817c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7786
        },
        {
            "id": "177fb5c8-204e-4bf2-9cd3-81f95e69959f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7787
        },
        {
            "id": "c3771f8c-fc9e-426c-9da5-9c9cba7d6ab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7808
        },
        {
            "id": "f7b18378-0409-4016-8f75-76e23091436d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7809
        },
        {
            "id": "190bf545-dcce-4dfd-8918-376e3d8cb231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7810
        },
        {
            "id": "26459b6b-fac2-48d7-b195-6f345d5458ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7811
        },
        {
            "id": "b53805ab-ef26-44eb-a855-eadef040f63d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7812
        },
        {
            "id": "42fded20-cc0b-4d51-a93a-81beccea31db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7813
        },
        {
            "id": "4564114e-fc0c-4509-a8c2-91f6f1e800b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7922
        },
        {
            "id": "cfb5fe7d-99f3-419a-906e-f4af2d2d3403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7923
        },
        {
            "id": "60159398-a465-4443-b477-5d4e38d6b9b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 47,
            "second": 47
        },
        {
            "id": "f0264ba6-61d0-4734-b1e9-d506e7b36d8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 84
        },
        {
            "id": "7d268e83-c4c4-4f62-8c6d-4d5dae1d9e62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 89
        },
        {
            "id": "da22fc78-b348-4aeb-a961-8af0becf0297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 116
        },
        {
            "id": "b9eb691c-274c-4800-a718-47f04284e6ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 121
        },
        {
            "id": "78781517-792b-4cbf-aecb-42cdbc8c9beb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 221
        },
        {
            "id": "ab859fca-6cf4-4530-9830-3278885d01c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 253
        },
        {
            "id": "c0faef68-00f7-48ee-b9c4-539fa192b01e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 255
        },
        {
            "id": "5c631cda-9b88-4a42-a8ac-c4392f7b6145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 354
        },
        {
            "id": "de5531c3-972e-4798-ad30-8ebb1e180a4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 355
        },
        {
            "id": "ba706541-c95d-4748-9d85-ec9fed07b714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 356
        },
        {
            "id": "ed36546d-919d-463b-859c-d88bf392de5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 357
        },
        {
            "id": "1cbc0427-f1f4-4fb1-9e1b-b875ef747e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 358
        },
        {
            "id": "a6c1cdca-e718-4e58-bef6-3749af580835",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 359
        },
        {
            "id": "652826dc-3cb1-4ec7-81cd-69cb809d8b47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 374
        },
        {
            "id": "6ee0ee5b-ba5e-44ea-8a7e-4e12a4f3f41f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 375
        },
        {
            "id": "93a2040d-6907-45ef-a2e8-c68681715b17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 376
        },
        {
            "id": "8d1e636b-c061-43bc-8137-c06d1d52dc13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 538
        },
        {
            "id": "7474d791-d816-471a-961b-590fbac02b17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 539
        },
        {
            "id": "78136f75-0786-4ca3-a1fe-38230de2a2b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 7786
        },
        {
            "id": "daa5403d-58e2-4a9d-b4dd-b424452042d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 7787
        },
        {
            "id": "b1d02b57-dd2f-47b8-ab00-16156b95c3a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 7922
        },
        {
            "id": "c4714039-85f8-4be4-8a4d-f46f0818aab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 7923
        },
        {
            "id": "a96f9316-4580-49b8-8e7b-0315315980cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 84
        },
        {
            "id": "2ad7fadc-4240-4ea8-9d09-b343269703b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 89
        },
        {
            "id": "bdcdc22e-0c43-4019-8571-282754dd6e7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 116
        },
        {
            "id": "b63f87c4-9531-411b-b65d-1c1217bc042e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 121
        },
        {
            "id": "24f630e5-7db2-4c23-9a20-181df4039985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 221
        },
        {
            "id": "cc5828a4-b1a0-4806-b166-c6afed7c379e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 253
        },
        {
            "id": "21943245-d01f-4895-abbe-0c0fa72b3e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 255
        },
        {
            "id": "feaf0e7d-eaf0-4ce9-9d88-e24f77e61e18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 354
        },
        {
            "id": "96870a79-7c6c-4a44-85d8-073beced023b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 355
        },
        {
            "id": "f5b2d969-9fab-4d4a-a53f-9c24e235286b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 356
        },
        {
            "id": "65a37c54-5f94-4124-94c0-2ecd51f32844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 357
        },
        {
            "id": "2423d839-c08b-4510-8d04-226af78d004c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 358
        },
        {
            "id": "178c7f7f-c2f0-4507-a911-5038dda6b6d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 359
        },
        {
            "id": "cf0b585b-d297-4916-ac36-95fdb015cde0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 374
        },
        {
            "id": "14016888-6fd8-4eea-b4d5-514383a3a877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 375
        },
        {
            "id": "06c17ec9-fa62-4cdd-9d8d-a3eaa5261869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 376
        },
        {
            "id": "4846da20-a3e1-4fbe-a902-917d21a384da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 538
        },
        {
            "id": "e942b40c-743f-4a4f-a0b3-7c5ad0508327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 539
        },
        {
            "id": "9d5f3811-aa35-4cf7-9578-af3f732a4aa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 7786
        },
        {
            "id": "a7fc715f-c7c2-4ce2-bc33-c1957c12119c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 7787
        },
        {
            "id": "c9c561cf-891e-4fed-a706-dfc40fc8083a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 7922
        },
        {
            "id": "4382b34f-5bc0-4b2d-8ee1-d67beb1b947c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 7923
        },
        {
            "id": "823d339a-0821-4907-901c-feb1190e1594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 63
        },
        {
            "id": "e3845c90-66c9-48bd-9f4b-c0d4a2ce09b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "589f84e4-9d53-4b19-b4d2-b753020fecb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "4910caaf-dc46-4535-b29d-43fa69348f8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8216
        },
        {
            "id": "4c79674b-76d7-4bac-a7b1-60d297995894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8220
        },
        {
            "id": "6772a4fa-3533-4f05-82f1-074473b84512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8221
        },
        {
            "id": "03fbecf6-2d17-434f-af63-1d7bd9129106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "3a04147e-2ae9-4981-8e04-b1b055f8014d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "b4952920-d13f-401d-a7f6-36b1c83c1634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "19911a9b-b06b-4295-9e5d-b275ab8aadd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 106
        },
        {
            "id": "ed6f86cb-3a6c-4a24-8f3f-c022abe0334c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "1fb84b10-1fc2-4256-a90d-446184910e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 230
        },
        {
            "id": "fc6dde3c-84ff-4369-8e6a-87de84feec24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 306
        },
        {
            "id": "5d8a61f1-808d-4608-8b03-0f165f2178fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 307
        },
        {
            "id": "eb51249f-804c-49e4-8d58-6ed76c6b9e2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 308
        },
        {
            "id": "383446de-3ed2-4bec-bb75-c4dd8cb7f5dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 309
        },
        {
            "id": "3bed5c9b-5c9b-4638-a37d-65f17ea5acea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 508
        },
        {
            "id": "bfb92a7a-91e1-40a7-ae8e-f4527602cab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 509
        },
        {
            "id": "4c976d5c-66de-4423-b109-4e6898a7aa04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 567
        },
        {
            "id": "6cd5e28d-6ac9-4483-bf2a-6211e0308de1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8218
        },
        {
            "id": "fe00d63a-adb5-49cb-bf93-5f32787adf47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8222
        },
        {
            "id": "0f7e246f-e324-4050-a2e6-89e4998ee5c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8230
        },
        {
            "id": "82bbf007-6c7c-4b9a-b905-8244cdd13532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 45
        },
        {
            "id": "888f6a2e-33de-4b02-8b68-d7653e545148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "1a9f8423-07b6-4bc2-bf61-8188f13dd660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "7df21581-0cd1-48d1-81d3-26ff7a994b66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "03b126c2-6dca-448b-8e69-f2947ea779f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "86898060-ff5f-4b52-b75d-70faabee644c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "e338eb48-5005-43b7-b2f7-de4ad83c2ac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 116
        },
        {
            "id": "9a6fd71d-7467-4951-a42b-9bcbc6564add",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "a96927b5-1588-4e12-882e-6ea0120feea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "548bbb0a-08ba-489d-beb3-a17b91a5073b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "b97f7e11-fc29-485a-88a5-95e89f857b40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 173
        },
        {
            "id": "ba3b7019-a8a2-47fe-9338-59ef946504ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 221
        },
        {
            "id": "fa8a80ab-ea8a-4405-8037-7fdf18f4b047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 253
        },
        {
            "id": "0727439b-8666-46e2-a1db-d11bb907d8b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 255
        },
        {
            "id": "542ddf0e-0858-44b8-948b-86fb5a75b979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 354
        },
        {
            "id": "b9f8e600-bd16-420f-a96c-f3826f979e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 355
        },
        {
            "id": "2004974b-4060-4b0e-b5b3-f057c1ef2cd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 356
        },
        {
            "id": "c1678666-19ff-4fcd-8417-82b44a5c0fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 357
        },
        {
            "id": "e0615420-59bb-43e5-99f5-d0016edbbb37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 358
        },
        {
            "id": "c4181ebe-b25b-4174-ad0e-95a3af29fff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 359
        },
        {
            "id": "cdff3121-fd60-478b-85ac-d97f0d29428e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "fe564b5a-4ca0-4a16-a185-ac00eb368edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 373
        },
        {
            "id": "091b0805-726b-4b8b-a633-19664b92f7ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 374
        },
        {
            "id": "abdd806b-2ed8-4ab5-902b-436edc8f79b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 375
        },
        {
            "id": "8e748633-36b1-44d3-9d56-b68f167fbda0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 376
        },
        {
            "id": "172a5d3e-0066-4d76-8fde-c57e186bf94b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 538
        },
        {
            "id": "49187895-0b0d-4b10-b538-2d19dcc3e2c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 539
        },
        {
            "id": "4660270e-b722-44a5-a5a4-b9e298959174",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 700
        },
        {
            "id": "7bb3edb0-6ee8-43b1-be45-107ef335e3a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7786
        },
        {
            "id": "c31913bb-ffd4-4ff7-8772-260cab913368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7787
        },
        {
            "id": "ec188925-e5fd-49e2-86a2-92173ede320d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "9eea5097-cb3a-4f7c-85f4-4d8fb3644af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7809
        },
        {
            "id": "62740056-613c-4147-a5cb-4777952452a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "03ed44c3-a82f-459f-b5e0-6122617a5c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7811
        },
        {
            "id": "c93e931a-793c-48ba-9c7e-75b35b354c72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "a3f95010-7e96-40d6-8852-78d0356f6b6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7813
        },
        {
            "id": "f18fab26-ee99-4f43-ab58-11467e93fb91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7922
        },
        {
            "id": "b144b9ac-663e-4c23-aabb-6fd6676bd3fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7923
        },
        {
            "id": "cf3a58d9-3693-48ee-a9a0-e79b5c1fdbba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8216
        },
        {
            "id": "1177eecd-0198-40f3-afea-23d06affdfb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "af1ca306-9e9a-4263-8285-b2e093827032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8220
        },
        {
            "id": "60048f61-187d-42b5-ae3b-ea63d0fb959c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8221
        },
        {
            "id": "8a6c7c0d-7ed9-4015-8ee1-e872cd03f1de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "dc3ec3a3-c357-48f2-98bd-c913780d3eb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "3231f359-ba66-48f6-9e52-03acd36cd540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "a6c7c915-8af3-456b-884d-d4a831fbf1a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 106
        },
        {
            "id": "2890f4f2-9270-4269-8f00-4d25d92a8e97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 198
        },
        {
            "id": "5347510a-19c0-4a36-b485-2eaf4688bddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 230
        },
        {
            "id": "7e35565f-63d4-451c-a1fc-79917f97c93a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 306
        },
        {
            "id": "a4857b28-881a-4cb2-b2ad-cd872c364ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 307
        },
        {
            "id": "fabaf861-eeef-40ab-8488-22b6e627cd4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 308
        },
        {
            "id": "07297963-9e14-4a3c-9fa1-761c957ea991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 309
        },
        {
            "id": "fbd1404c-517d-4023-b73b-0e351005cd8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 508
        },
        {
            "id": "136aab7b-97b4-45da-be42-4fc8d24d3129",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 509
        },
        {
            "id": "b38df807-b179-4f06-98fc-55bcfc5d8b74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 567
        },
        {
            "id": "ca491cc5-1a0e-4e0c-b65d-aaf936d76dad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 8218
        },
        {
            "id": "7874fb54-20ef-44b0-a515-78fd6b96904d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 8222
        },
        {
            "id": "f4669459-57eb-44ee-b3c5-07587e6ffc27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 8230
        },
        {
            "id": "432254d8-2a7e-4204-859b-4b551a5c138b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "003fbc99-715d-4c0c-a2aa-2f11246cd0ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "d61c4a62-cc4a-460f-ab4a-14bec069ae9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "af733ad8-6ebb-4188-a3eb-a226d1c980f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "2605be1b-2da6-4b7b-9206-fa68189dca73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "30b6354b-3498-47d0-a708-786374d36d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "6fd202a9-db67-4c25-b70c-8aa7060513c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "a1bf4f10-c422-4122-8a8a-b0674f2f080a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "42e5ea33-3af9-4b49-a9c7-b3951b2fd31c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 106
        },
        {
            "id": "4ae9c519-7a51-47b2-996d-51d46cb9e1a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "10b21890-5f4d-4943-9d06-60e626d2f200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 192
        },
        {
            "id": "bfba0650-655a-457f-8811-484588a9bcb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 193
        },
        {
            "id": "4f827de3-7a32-4bae-8ab5-45f520056a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 194
        },
        {
            "id": "254541eb-02c0-448d-87ee-8348b3e2d862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 195
        },
        {
            "id": "c532a1e6-b6ca-430a-ab51-ca3e1641d824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "196c04fa-abbf-48a8-bd73-68c90652660f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 197
        },
        {
            "id": "3274a54d-a339-45cd-befe-f9079617d4bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 198
        },
        {
            "id": "18b844b4-f3ed-497d-9d04-2e11c4b71fe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 224
        },
        {
            "id": "5bae62b7-a89e-4a45-b25a-625e12233f89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 225
        },
        {
            "id": "2db0fd32-1724-4963-affe-c789e0412273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 226
        },
        {
            "id": "bd511b1d-f283-425c-9a7f-fc45164c13d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 227
        },
        {
            "id": "ef16aba1-4761-403f-a3c7-ae0bf330190b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 228
        },
        {
            "id": "4ae36a93-0fde-405c-8680-e5e1abf1a81c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 229
        },
        {
            "id": "14668cb3-83d4-4bff-874e-3f985bad0145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 230
        },
        {
            "id": "05b2ceaa-4653-426c-9cdb-c888aed971ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 256
        },
        {
            "id": "4c9b28aa-aac6-4907-8e87-7b47b2a54f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 257
        },
        {
            "id": "31cf8189-bf7d-4391-951f-b665d1c12096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 258
        },
        {
            "id": "b84d5150-3647-406f-9a7a-628c45b531f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 259
        },
        {
            "id": "9def1d4c-491a-41a4-b0b6-ffb0f178fa30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 260
        },
        {
            "id": "c9e3eb7a-f062-4eba-8b66-fda7aaf7ff15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 261
        },
        {
            "id": "acf275ad-88f1-4a17-9a62-1074f2c012db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 306
        },
        {
            "id": "13241717-3e07-409c-b438-383302f48bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 307
        },
        {
            "id": "6deda967-7f35-4998-95f9-e61bc4b01293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 308
        },
        {
            "id": "8720676c-4d9f-45dc-9835-b6655a3aaf35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 309
        },
        {
            "id": "10c308d8-81d4-49c9-9496-7578e2c43416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 508
        },
        {
            "id": "52548a85-a590-432a-ab7a-9df3dbdd5186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 509
        },
        {
            "id": "60bfe85a-d4e8-4074-be6b-fba59119d1e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 567
        },
        {
            "id": "66c245e7-4f1e-497b-ba6a-aff9189c9db5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8218
        },
        {
            "id": "2b7ad33f-ef4c-40c9-bc97-e4d1bc891eea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8222
        },
        {
            "id": "a2407385-c04b-4110-8c7c-9e1c9676822f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8230
        },
        {
            "id": "474095f1-c11e-4525-a5d8-9e77cfca28cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "f7270c9b-a4c5-42cd-9c6c-9f118ce4955e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "bd9ef9da-f3c3-4c9b-9b3c-ce31618644ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "099c8eb2-313d-406b-8918-440b5205018c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "e5993cc1-f244-4dbd-bf3e-9e63afc8a5d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "1245cf9b-5002-4949-89e7-74b410f6d484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 106
        },
        {
            "id": "9c0b67c5-75d1-44f9-97fb-3e1b42c93f3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 192
        },
        {
            "id": "c4d4076e-095f-415f-8705-1a5fcaef36ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 193
        },
        {
            "id": "83384c66-d13f-4b36-aa1f-126de79e65d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 194
        },
        {
            "id": "3f553ee9-c31d-4401-b423-c11cf7e198ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 195
        },
        {
            "id": "81cdcfbc-b9c3-4341-81a7-bce7f43caa52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 196
        },
        {
            "id": "bc4bceea-f3d2-448a-9572-d0740bb3c419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 197
        },
        {
            "id": "f44ae50c-f51c-402d-abe9-9e7697b97588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 198
        },
        {
            "id": "b2fc7773-6b6b-4725-8b2b-e1b70e017937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 224
        },
        {
            "id": "89055f54-8c3b-4c9b-94e0-189434fca3c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 225
        },
        {
            "id": "17ab1899-3654-4b2d-b471-cd2ca4ff31f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 226
        },
        {
            "id": "8aaefa7e-bace-4552-bde8-b5bb37e6cc46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 227
        },
        {
            "id": "45e1f7a8-0c55-46a7-9d52-6fc0414aa153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 228
        },
        {
            "id": "2106bcc8-1da2-494c-8efb-26ce9b7d90ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 229
        },
        {
            "id": "dd660522-7503-4aa7-b7b7-adf00d5670cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 230
        },
        {
            "id": "4e994748-4b73-4b2b-b4a9-083e54c0c2dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 256
        },
        {
            "id": "0c4a21f2-ea7b-4919-aa0d-f4b19fdfde8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 257
        },
        {
            "id": "c79fb495-fdde-4046-85f6-27b9e6c568c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 258
        },
        {
            "id": "4ac2d30a-4a86-4a89-8ee4-cb94c07cfa40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 259
        },
        {
            "id": "fd2eb221-b3fc-451a-8556-48e97da015d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 260
        },
        {
            "id": "d424b890-bb32-4f99-a020-0c4cfac72376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 261
        },
        {
            "id": "51d91eb7-c23b-40f5-9e9d-5c595b8ab31b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 306
        },
        {
            "id": "156e2a27-bcf3-418a-a96e-e1256421fb0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 307
        },
        {
            "id": "ad928fd1-5c21-40d5-a575-7db61247fd09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 308
        },
        {
            "id": "a0c4f9cc-5fe2-4e3a-9d7a-050c08f5df53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 309
        },
        {
            "id": "64604232-d573-40a3-9a14-3c4214728761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 508
        },
        {
            "id": "52c157f6-effb-49c6-9ae6-918789699555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 509
        },
        {
            "id": "47fedcba-157f-4890-9c40-76e6590479c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 567
        },
        {
            "id": "2b512f3d-631b-4bca-bdda-88899a97c091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8218
        },
        {
            "id": "4ef1b2cd-7a96-4ec1-a168-48970c18d7e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8222
        },
        {
            "id": "5b654850-65c1-4629-b461-06c08200e0df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8230
        },
        {
            "id": "c361f798-9ac1-404f-ab34-9b96bd99ca07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "792df5de-82a1-4aa4-955b-41eec26b4a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "e2efe269-b64f-45a6-8182-36aa08e7e8a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 198
        },
        {
            "id": "296c2c6e-748a-4119-beb7-8ac26108d9f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "327a0373-9a9d-406b-981f-c977d2461933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 508
        },
        {
            "id": "6c21158a-bd54-4c41-bd43-12f712cff90a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 509
        },
        {
            "id": "cd255d27-4044-4fac-9257-371d8538457a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8218
        },
        {
            "id": "44c3500b-ba6e-47a3-81dd-03387727dbb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8222
        },
        {
            "id": "844ccc02-e210-4034-bd81-396652205c97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8230
        },
        {
            "id": "8443723e-1f68-49ba-9145-a7368b521dca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "9f134afd-6dfa-448e-97c6-348ba8d08a39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "e7ec170f-5df4-450e-aafa-921d900ae12b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "555d63d4-6eb1-4570-992d-e35538875ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "19738c57-ea1a-4aba-b324-bc11add5b7dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "0a797538-ea32-451e-b3d6-37b85ef72023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "5028447f-e88f-45e3-8162-1fd2ceb94642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "b99b702f-1903-4bc2-9e85-4fbbc41c08e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "b32e8d16-9518-49aa-993f-919756bf8bad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 106
        },
        {
            "id": "f3ca41c6-9213-412b-9733-6eecc1d01f27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "749c3d3d-e5f8-46b7-88c0-2571243d0bd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 192
        },
        {
            "id": "99319814-57a6-45ab-a10c-80ca7843925a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 193
        },
        {
            "id": "f2bc1273-fd41-4635-b64f-d3295642bd4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 194
        },
        {
            "id": "1cf1ee7c-36f6-4859-b365-3a4832adf6f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 195
        },
        {
            "id": "2c116f40-f5c9-4001-b4d5-ac0083460b42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 196
        },
        {
            "id": "661fdc20-fe95-4989-a0cb-819d0f07ba04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 197
        },
        {
            "id": "18512d72-7dc8-46eb-9ba1-813e93688b1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 198
        },
        {
            "id": "3fd6bee2-325c-44b7-bbf8-b533aef8b167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 224
        },
        {
            "id": "de41960b-9c37-4b9d-b580-a37ab2a1f901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 225
        },
        {
            "id": "e7574b68-4b41-4f12-a966-cbd5be495797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 226
        },
        {
            "id": "8b849865-fa0d-4660-8be2-f6f8a2757209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 227
        },
        {
            "id": "e10ea01c-f058-41d1-be12-6293b100388a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 228
        },
        {
            "id": "861dce1f-3398-4f29-a709-569c9c46614a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 229
        },
        {
            "id": "12b38352-0de5-4cb6-a82c-cc4ad1b79b7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 230
        },
        {
            "id": "7154024e-34f2-4363-bf85-05eb340baaf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 256
        },
        {
            "id": "017dfb65-89b8-4198-9112-374290938525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 257
        },
        {
            "id": "5be29348-4eae-4a35-94dd-f0c7749166a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 258
        },
        {
            "id": "7094e634-bba0-4c9c-a6c2-4539eb2fd52b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 259
        },
        {
            "id": "f8210ef5-047f-4214-bacf-8c3b3bf715ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 260
        },
        {
            "id": "ac18923c-131d-490d-ac0a-904f1a7f8078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 261
        },
        {
            "id": "745d8ec7-dba7-4237-a048-1a566a8015bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 306
        },
        {
            "id": "4e55577a-d3a2-459e-8fe8-1c116f4cf1ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 307
        },
        {
            "id": "85e93078-508a-4729-93f6-ff7efa3aa918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 308
        },
        {
            "id": "e8b592e0-514d-470c-96e1-f13ca05d70cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 309
        },
        {
            "id": "71821916-294c-443e-9a16-51ab11639112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 508
        },
        {
            "id": "ecd12046-2380-41e6-9cb8-f198f32c12e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 509
        },
        {
            "id": "c040d010-1fac-4553-873d-0421236329ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 567
        },
        {
            "id": "098f4833-9fdf-4705-b968-89257f72857c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8218
        },
        {
            "id": "ac9e2ff2-718f-463a-bfb7-4734e45afc7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8222
        },
        {
            "id": "0486a9f2-7646-4585-ba80-8306b5d3841e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8230
        },
        {
            "id": "72354888-475f-4bf5-a97a-1939f867ec6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 63
        },
        {
            "id": "a5d8c94c-d85f-4384-b3be-99c3f6fc12f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 84
        },
        {
            "id": "34ad951c-c32e-405f-9f73-d12b967f36db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 86
        },
        {
            "id": "bb823723-ebda-4bed-b8c1-7b492707bc55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 89
        },
        {
            "id": "61fc37ec-fc96-48cb-be88-e36d6eedb046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 116
        },
        {
            "id": "a03422fd-97e4-42b0-92f0-e09b2fc86bcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 118
        },
        {
            "id": "618cb192-c45c-414b-9ffc-329bc4317798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 121
        },
        {
            "id": "04305dee-596f-4bdc-acaf-41b49f1546ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 221
        },
        {
            "id": "e569cfd0-9d35-44bf-a68c-b199b8470b54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 253
        },
        {
            "id": "7d3f0dfa-49e2-4d94-ba07-6c3fa8a539ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 255
        },
        {
            "id": "d0eab70c-5fd2-4847-b4ad-d1783e46e95c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 354
        },
        {
            "id": "95d9bf7d-0cb3-4eeb-9ef0-88cea2093ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 355
        },
        {
            "id": "4428ef54-1282-48a9-a74f-e0c9f92b36cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 356
        },
        {
            "id": "c3c9b58a-97e2-43e5-b7ee-64ef25b42ebd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 357
        },
        {
            "id": "4e1ef249-c9b7-44b0-8130-1cebcdc9c950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 358
        },
        {
            "id": "3d16f395-5b90-46f8-8196-ca18a84f60ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 359
        },
        {
            "id": "eee4e647-7940-41d5-8892-474ec585fb36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 374
        },
        {
            "id": "9acf35c2-dd32-422e-9c34-5bc2466f41c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 375
        },
        {
            "id": "a55ee633-f620-42f4-854c-f9d37da41599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 376
        },
        {
            "id": "5db50881-7cf2-426a-ab1c-841f06ec3f7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 538
        },
        {
            "id": "4e7a1754-9ace-43c4-bb3d-8515de616ca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 539
        },
        {
            "id": "9c3f13ca-94dc-4298-87e6-2cf91d232c9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 700
        },
        {
            "id": "c5feba7f-195c-4e2d-9de0-2ce7ccdda6dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 7786
        },
        {
            "id": "a988d179-82db-4887-9545-b5a23203d8cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 7787
        },
        {
            "id": "6266feec-2a43-4da0-9771-c584e060063c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 7922
        },
        {
            "id": "53115e7b-06e1-41db-8df5-4841c248bd62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 7923
        },
        {
            "id": "2e51b26c-937b-426b-aa50-d8452cd4f62b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 8216
        },
        {
            "id": "6aa5a216-c91b-43e8-ba06-87a0a2af519a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 8217
        },
        {
            "id": "47fedcdf-4c94-4d6e-9769-154701c2134b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 8220
        },
        {
            "id": "42aa4400-a43f-44c9-b31d-fdf3b97adfe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 8221
        },
        {
            "id": "f4dd952e-237a-4ed4-9863-a97f151f356e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 44
        },
        {
            "id": "f389135e-5e55-4457-9a95-b74d0a3079da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 46
        },
        {
            "id": "006d85c8-c950-4c5e-89d6-18c9fcd32ac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 74
        },
        {
            "id": "41d28cb5-5b93-4a92-af07-47aebc7ff2f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 106
        },
        {
            "id": "238dbb3d-d96a-4041-af24-8458370c2c4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 198
        },
        {
            "id": "84fb5a86-09ae-4332-bee3-68b99bdeed8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 230
        },
        {
            "id": "292ba247-4d2e-4815-8c7a-c5d651404044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 306
        },
        {
            "id": "efcdc28c-e4fd-4da9-862d-73b4dada4e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 307
        },
        {
            "id": "9eb4074c-d46c-4823-80f8-648556b463ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 308
        },
        {
            "id": "9c5ecbe5-b5d9-4112-8b0e-a389ffe83ffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 309
        },
        {
            "id": "326bce43-b321-46be-ba7a-70d7fc10410e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 508
        },
        {
            "id": "ce50f3b9-291b-4497-95ae-9d46d2bf055d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 509
        },
        {
            "id": "4e297a0a-a9bc-4678-aab6-e2ea3261147a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 567
        },
        {
            "id": "33a57b01-5e5c-42ec-a7be-35f466aaa453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8218
        },
        {
            "id": "009eee8e-f9c7-4619-a5ac-cd4d8bf840c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8222
        },
        {
            "id": "e6dc827c-4cd2-44c4-b5b2-90f93e2c4301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8230
        },
        {
            "id": "5538cf0e-fcd5-42f6-9c4e-661d154a6287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 45
        },
        {
            "id": "480ca453-1328-4b84-843b-3a040aeba355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 63
        },
        {
            "id": "1d82cc52-18d1-4dac-b119-f00436010f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 84
        },
        {
            "id": "910ea359-e88f-4b42-abf5-ea607021740d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 86
        },
        {
            "id": "d56b41ff-713f-4fc3-a021-cfe6f196a388",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 87
        },
        {
            "id": "f2ade633-ebdb-4a3f-8fd6-fb838c46d71d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 89
        },
        {
            "id": "419a87fe-52c2-4157-a367-c1f1ef0aebe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 116
        },
        {
            "id": "b5c2104b-3e97-467a-a481-1d9184afc8cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 118
        },
        {
            "id": "e32cac47-6ee6-4f03-ab8b-adf44af9d50f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 119
        },
        {
            "id": "fb8d6169-58ce-437b-a036-921e214d3315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 121
        },
        {
            "id": "a572892e-5f6a-4f58-a671-feaa4af5991b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 173
        },
        {
            "id": "1b87eb13-fc22-4f5f-945b-7dd1354dc0a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 221
        },
        {
            "id": "edab3cbd-ac8e-462d-b359-cccde54e84be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 253
        },
        {
            "id": "43430020-66b8-47f9-b47a-5c75f9c6e18e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 255
        },
        {
            "id": "c0612196-9d40-4a40-a03f-5b99916d0fc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 354
        },
        {
            "id": "5c646320-33e8-4f61-aa98-178457739762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 355
        },
        {
            "id": "5e48f198-e5f6-4a53-98a9-283afd64ca39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 356
        },
        {
            "id": "e054cfe9-6d11-43c8-8084-b44a99d290be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 357
        },
        {
            "id": "c5e36454-5ab7-4623-86ec-ab4489ef541e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 358
        },
        {
            "id": "3c886670-97a6-417d-bd71-e60a87e94dc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 359
        },
        {
            "id": "5e95b9d1-87a0-4bbd-84a5-c19dad159af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 372
        },
        {
            "id": "5bd34191-1c30-4c18-9402-065b26cb463e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 373
        },
        {
            "id": "59008795-a77f-4b45-bb76-1c5a81fb72f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 374
        },
        {
            "id": "8f973af8-f61e-48a9-b158-57306b794c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 375
        },
        {
            "id": "eed196ca-97d1-4e3b-bfbe-f6647e5c22b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 376
        },
        {
            "id": "ac20396d-1cb3-4662-bda4-497f672e396b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 538
        },
        {
            "id": "651f1ae4-e155-4e2e-b9c2-ee21055e4cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 539
        },
        {
            "id": "a514afe5-3955-41b9-b2b0-5ef71a215d5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 700
        },
        {
            "id": "37b3e50e-1b0d-4f2b-b823-321078b3c081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 7786
        },
        {
            "id": "f625ef71-c95a-488b-b97b-3aedec53571e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 7787
        },
        {
            "id": "27da8903-f417-4f66-962a-051a28ba10ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 7808
        },
        {
            "id": "eabb7b56-a7a3-405e-b98e-f80a84b27c1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 7809
        },
        {
            "id": "62bb9e39-cba0-41da-9870-a8283ddbe19b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 7810
        },
        {
            "id": "dfb47afb-0519-44f3-b814-228bec9c2ed1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 7811
        },
        {
            "id": "4aa419c6-a2d0-4f5b-9ccf-54331ccb7977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 7812
        },
        {
            "id": "78813010-fa9b-4aea-b98f-b52879ff64e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 7813
        },
        {
            "id": "126c5e16-ba43-472c-8ade-352d189f874c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 7922
        },
        {
            "id": "eafa0d3c-f3bc-4962-99ea-03f4a92954ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 7923
        },
        {
            "id": "cd9bb71f-20b8-4556-819d-6a0456b409c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 8216
        },
        {
            "id": "1b631f71-c494-4641-aa49-f3355f95ccad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 8217
        },
        {
            "id": "0a84a45c-c3cf-41b9-a901-4398c391b17e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 8220
        },
        {
            "id": "a3226af9-f658-49c0-862d-3e745b0a93e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 8221
        },
        {
            "id": "beca2c9b-b42a-4982-b5dc-35b44320d9a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 44
        },
        {
            "id": "597ebee4-3e31-42de-b154-24adbfc756b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 46
        },
        {
            "id": "f59bdf2a-93fe-4169-945c-bc144dfde9c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 74
        },
        {
            "id": "e770ecdf-a738-42fc-bf65-35f13a120f2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 106
        },
        {
            "id": "ed02f52f-d64f-4904-a4ff-e8d864e08bd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 198
        },
        {
            "id": "3b5542e0-30b9-4b27-877f-c7ced229f788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 230
        },
        {
            "id": "3c6b2eaa-ee29-42d7-b84b-4e8701fbff4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 306
        },
        {
            "id": "3bd36591-21b9-439c-8801-8e5648f6a278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 307
        },
        {
            "id": "9f31a158-4ec1-4666-af20-22c7c0308aac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 308
        },
        {
            "id": "35df32f1-cf9e-4510-887c-d8f4d9d8be2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 309
        },
        {
            "id": "a379eaf8-3476-4738-9160-eafb8994f573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 508
        },
        {
            "id": "247f233f-bc4d-4b8f-930e-d320f0d23987",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 509
        },
        {
            "id": "b473b8ef-16aa-43ef-bd43-fd3622f49632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 567
        },
        {
            "id": "7dd2744e-84e5-45f0-9669-00ecff4a4c74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 8218
        },
        {
            "id": "1bfeda4d-97b5-407c-83ed-85d631f6fbd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 8222
        },
        {
            "id": "635274ed-3049-4718-81d1-ab90797b645d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 8230
        },
        {
            "id": "45aaaea5-41ac-4e9d-93e9-479846db03d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 44
        },
        {
            "id": "90ef5289-b8c5-4848-9611-d908d973267c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 45
        },
        {
            "id": "417c3b05-7288-497e-bc22-72108c82246b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 46
        },
        {
            "id": "84a199f9-7989-44b8-9989-f5373687ef02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 58
        },
        {
            "id": "40340329-535a-4ded-a500-456719bc8dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 59
        },
        {
            "id": "76eed5de-6b75-4225-ac0a-d035fc389498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 65
        },
        {
            "id": "08d0ae65-22f6-40b6-8f96-0b34469e47f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 74
        },
        {
            "id": "b915ddf4-ab47-497d-baeb-327917b603f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 97
        },
        {
            "id": "a1a3cf7d-a217-4b41-a78f-0c6808d8c03f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 106
        },
        {
            "id": "7849fcbc-f07f-4692-b7f4-abb9c994ac63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 173
        },
        {
            "id": "03ea0143-cd5c-4087-9217-e2b8a78d47f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 192
        },
        {
            "id": "56e6e393-2858-4ca8-83e5-a7bfc2141da6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 193
        },
        {
            "id": "7f63ec2e-c463-4900-8ff9-659ca7f1af31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 194
        },
        {
            "id": "e35feb55-11af-474d-8b14-524a4e8449f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 195
        },
        {
            "id": "65fe5f22-5499-4e0a-aa70-dc09b9d8200d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 196
        },
        {
            "id": "627fb9b4-8ec5-4783-822d-ea92187d200a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 197
        },
        {
            "id": "1f50a677-9415-44b3-a881-62e54a190650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 198
        },
        {
            "id": "34fb9814-6b71-4c4e-9d96-4e25b8a162db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 224
        },
        {
            "id": "0ca59cc1-9b3a-4afb-9203-3b3edfff1f5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 225
        },
        {
            "id": "470e647e-6e5e-42e9-9813-b7e3d17dcb76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 226
        },
        {
            "id": "19f9e611-ba60-4abe-8279-a5ddf4c1107c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 227
        },
        {
            "id": "bff23a88-31a5-474d-8874-431c4fa1990b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 228
        },
        {
            "id": "65423352-069c-4528-bbf7-5016bea61459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 229
        },
        {
            "id": "04c61557-c066-4f48-9f39-89f5f6c35aa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 230
        },
        {
            "id": "699dafcd-6607-4da5-b8ff-94e30219dd20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 256
        },
        {
            "id": "b63ac01c-da0c-4bde-ad00-77917b90cf6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 257
        },
        {
            "id": "086cc683-9775-4dda-ab32-b6940c0099c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 258
        },
        {
            "id": "4fe6cd26-c944-4545-9d68-77f3832b2cb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 259
        },
        {
            "id": "69de979e-8128-4bad-a48e-e033be3897e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 260
        },
        {
            "id": "ac086e3b-d2da-4d9f-8224-ed9a16a3b15d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 261
        },
        {
            "id": "4357608d-9366-4863-ac16-b126fe90b92b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 306
        },
        {
            "id": "0751a0ef-56d1-42e9-91a6-1e6aa6b51118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 307
        },
        {
            "id": "e6f7f263-a2fd-468f-8918-e0dce9b0b314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 308
        },
        {
            "id": "91ee6291-c27f-4d52-bedf-60792254366e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 309
        },
        {
            "id": "0f9ba7ec-dc34-461d-bf51-a8ba39a83fde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 508
        },
        {
            "id": "1745bacf-6bbe-4c14-aa34-d3b6ee616f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 509
        },
        {
            "id": "9c4418cb-893d-4e68-b36b-9fff206beb60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 567
        },
        {
            "id": "cf04df19-c85a-402c-b571-9ffd3d42fb4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 8218
        },
        {
            "id": "07896523-0b36-437f-8045-9007a5bba3c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 8222
        },
        {
            "id": "95f70caa-bdfd-4921-95ea-0038f8a8922d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 8230
        },
        {
            "id": "f588ee3e-a024-447d-acb5-8e81cff7c180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "2e9b98fe-7867-472d-a1b2-fce6b2d8ea75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "8093ca29-abeb-4f8a-8689-53f1c09f0302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 65
        },
        {
            "id": "1994a6ff-87e1-4852-a040-610e444b98b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 74
        },
        {
            "id": "7238bbf2-2c37-4486-a2de-099139fbfc14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 97
        },
        {
            "id": "6b55ac2c-5199-4992-9db9-cad2b88baa30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 106
        },
        {
            "id": "9836bb8f-dd28-4b1f-ac51-f1ff1efc6962",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 192
        },
        {
            "id": "a05c1f21-bf84-4565-abde-a800899316bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 193
        },
        {
            "id": "f039efc2-4229-4288-be6b-ac5dc17ebfb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 194
        },
        {
            "id": "e8f0a369-d8e9-47a6-a758-0835fc4c75a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 195
        },
        {
            "id": "8b84c31c-602d-4d19-b7e4-a082a7a98ff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 196
        },
        {
            "id": "275e8a2c-27b2-4650-9fb4-b446c8bef51a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 197
        },
        {
            "id": "1c401619-2b19-4ff5-8b15-2e3acc514543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 198
        },
        {
            "id": "aa5d92b3-21f4-4e1e-86f5-69c4397882de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 224
        },
        {
            "id": "a0db7d30-f245-4f43-b4cf-63807b102cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 225
        },
        {
            "id": "fc3e5f26-34bb-4c60-92c5-1622a65ab650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 226
        },
        {
            "id": "caa228ab-6f02-4f05-abf7-9f6e6f09a4c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 227
        },
        {
            "id": "9f2ba6da-6b8b-4b90-9afc-c84ef94c3a25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 228
        },
        {
            "id": "0a8d4c15-7dcb-44dd-9e2d-ef1c39ae3d5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 229
        },
        {
            "id": "a33cbc4a-89b0-4ef5-8dea-190b7fd9d302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 230
        },
        {
            "id": "63e5a21c-5667-464a-9605-a145191233f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 256
        },
        {
            "id": "af8fd809-9449-445f-8624-e2ad3b0ac87e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 257
        },
        {
            "id": "ee717c2e-1371-4967-a67f-0e3a1ac21a96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 258
        },
        {
            "id": "8b589fac-562c-4e88-bc1d-4d7f633e3770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 259
        },
        {
            "id": "5e23b74f-5671-43be-a5d5-f8128a7e8569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 260
        },
        {
            "id": "8ae61296-b35a-479b-b2be-4e40fed1e2db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 261
        },
        {
            "id": "2f01b5c5-a1a4-44e9-8c2d-67aa428540ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 306
        },
        {
            "id": "499abbd6-eca6-4c33-991e-e70497a058b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 307
        },
        {
            "id": "893dd2c6-207d-4e16-9987-6e78582743df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 308
        },
        {
            "id": "d5166407-4229-43c6-b685-d344d977d3aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 309
        },
        {
            "id": "571044dd-a401-4fe4-9c59-6e72538ac868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 508
        },
        {
            "id": "afb0a684-7c20-49c4-b269-48fd85aa4c9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 509
        },
        {
            "id": "c27b3357-8530-443e-b7c7-03fc7eceb6ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 567
        },
        {
            "id": "30ba02f4-14f2-4121-9e0f-b31330193c5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8218
        },
        {
            "id": "b9db9cc2-ff00-4170-b880-9532cd6fe01f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8222
        },
        {
            "id": "2a51e300-9018-4ae3-a8d5-48df3502db7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8230
        },
        {
            "id": "a9469c4b-0b2a-455c-af17-8741a3f1821d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "be362f66-c4dd-4e6d-b0e1-7761fa7c2d11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "ba91d4f7-ea79-4603-bc95-29d1fc80c320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 198
        },
        {
            "id": "bafb50f3-db67-46df-8911-76efbc1e769b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 230
        },
        {
            "id": "1d2a97b0-dc6e-411b-8782-dc2992ce296c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 508
        },
        {
            "id": "12190c90-ee56-4920-9d8b-0b391f3f5230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 509
        },
        {
            "id": "5cfcf4f0-5738-4e63-817d-6f88f190dc25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8218
        },
        {
            "id": "57d8a15f-b176-4154-94fe-2889f92b08f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8222
        },
        {
            "id": "285fb74b-aa49-4a55-95ca-3522d3aa9de1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8230
        },
        {
            "id": "bfcb08aa-9839-448b-bfb8-d7f5be6aef7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 44
        },
        {
            "id": "b8e993e9-a79e-44f3-8ec8-9a0a9d0b855f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 45
        },
        {
            "id": "807ed43d-f97a-4072-a549-837478a25992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 46
        },
        {
            "id": "6e1d197b-d6de-4ad0-ab7d-e4da7b53a845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 58
        },
        {
            "id": "92c9dc19-eb2e-466a-ad98-c31d4d46bf60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 59
        },
        {
            "id": "03fab2aa-0669-4560-9a2f-ce396b756708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 65
        },
        {
            "id": "46f43b31-77c1-4183-ad5d-c07f7d231a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 74
        },
        {
            "id": "74b2ae90-d5ba-400e-9e2c-5f5969180c76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 97
        },
        {
            "id": "5959ecc9-024b-4304-a188-a961007a0820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 106
        },
        {
            "id": "f449df2e-281e-48c9-9c51-074ee21ed40a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 173
        },
        {
            "id": "85461c27-4bf4-4169-8652-c6b438573113",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 192
        },
        {
            "id": "70ecfb5e-c923-4aa5-8f38-f33a33eea231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 193
        },
        {
            "id": "43c13510-1e73-459c-8215-556ce20f6182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 194
        },
        {
            "id": "8e42521e-ca49-49e3-a878-c912df5babfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 195
        },
        {
            "id": "6a6ce3cd-073e-4198-adbc-10bc90b1a37f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 196
        },
        {
            "id": "f803e0ab-db3b-47fd-a72f-d949b87f88da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 197
        },
        {
            "id": "0ec6f324-7b2e-4835-9f55-75f0961414b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 198
        },
        {
            "id": "0ad28cb9-4921-4f52-9d1a-55eae9d031ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 224
        },
        {
            "id": "7565da52-1f61-48ab-a282-0f4f9a881947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 225
        },
        {
            "id": "eac9c6a6-2f9f-47ff-961a-e274159a28ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 226
        },
        {
            "id": "7ab73d01-6b97-413d-a2d3-f623dd0a34a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 227
        },
        {
            "id": "787c527a-0232-4ab9-ad82-7e885a3983ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 228
        },
        {
            "id": "039d3dcb-5c1e-4aa6-9145-51c81f79cc95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 229
        },
        {
            "id": "ced9ace9-fb4e-4140-a4f9-2c559c796f5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 230
        },
        {
            "id": "e68c6681-bfb1-449a-91e5-b5044a050503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 256
        },
        {
            "id": "36d62a7c-4cc2-4560-99b7-b04a0e07b95e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 257
        },
        {
            "id": "dc07e376-dff8-4e88-896b-bb4c574e229f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 258
        },
        {
            "id": "ef697fd2-70e1-494e-aecd-ca7692af3b1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 259
        },
        {
            "id": "0ec0443a-b204-410a-855b-72302569a846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 260
        },
        {
            "id": "383e6b82-ed4d-4f99-a0ab-9124f8d0a64f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 261
        },
        {
            "id": "d7c6d021-8743-4f56-bc18-9ab076b5c31c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 306
        },
        {
            "id": "d6f2a8f0-cb41-4501-a716-1446b9d9a3a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 307
        },
        {
            "id": "d232e66f-64bb-44a6-9839-86fce8746012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 308
        },
        {
            "id": "22b5ad0b-12dd-4c71-ac96-1ec1c1054039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 309
        },
        {
            "id": "b79edfe6-f5f2-415b-91c6-af00a3eb1ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 508
        },
        {
            "id": "403ca658-a773-4e45-a3fa-97252634b41e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 509
        },
        {
            "id": "09cd79f1-e833-486f-b085-18dcdab2c2cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 567
        },
        {
            "id": "47f09be9-33ed-4e50-8055-18a03cb93687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 8218
        },
        {
            "id": "1f24ffab-5dfe-44ee-8b7e-9ae5fa167e00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 8222
        },
        {
            "id": "e1c07f43-6990-408b-9542-722d89e24577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 8230
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}