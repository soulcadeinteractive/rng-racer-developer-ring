// Initialise the global array that allows the lookup of the depth of a given object
// GM2.0 does not have a depth on objects so on import from 1.x a global array is created
// NOTE: MacroExpansion is used to insert the array initialisation at import time
gml_pragma( "global", "__global_object_depths()");

// insert the generated arrays here
global.__objectDepths[0] = 0; // obj_npcglobal.__objectDepths[1] = 0; // obj_car_baseglobal.__objectDepths[2] = 0; // obj_playerglobal.__objectDepths[3] = 0; // obj_multiplayer_playerglobal.__objectDepths[4] = -1; // obj_npc_targetglobal.__objectDepths[5] = 0; // obj_npc_multiplayerglobal.__objectDepths[6] = -9999; // obj_htmeglobal.__objectDepths[7] = 0; // htme_obj_netControlglobal.__objectDepths[8] = 0; // htme_obj_waitForClientglobal.__objectDepths[9] = 0; // obj_consoleglobal.__objectDepths[10] = 0; // obj_menu_uiglobal.__objectDepths[11] = 0; // htme_obj_menu

global.__objectNames[0] = "obj_npc";global.__objectNames[1] = "obj_car_base";global.__objectNames[2] = "obj_player";global.__objectNames[3] = "obj_multiplayer_player";global.__objectNames[4] = "obj_npc_target";global.__objectNames[5] = "obj_npc_multiplayer";global.__objectNames[6] = "obj_htme";global.__objectNames[7] = "htme_obj_netControl";global.__objectNames[8] = "htme_obj_waitForClient";global.__objectNames[9] = "obj_console";global.__objectNames[10] = "obj_menu_ui";global.__objectNames[11] = "htme_obj_menu";global.__objectDepths[12] = -9999; // obj_htmeglobal.__objectNames[12] = "obj_htme";global.__objectDepths[2] = 0; // obj_punch_clientglobal.__objectDepths[3] = -9999; // obj_htmeglobal.__objectNames[2] = "obj_punch_client";global.__objectNames[3] = "obj_htme";

global.__objectDepths[3] = -9999; // obj_htme


global.__objectNames[3] = "obj_htme";


// create another array that has the correct entries
var len = array_length_1d(global.__objectDepths);
global.__objectID2Depth = [];
for( var i=0; i<len; ++i ) {
	var objID = asset_get_index( global.__objectNames[i] );
	if (objID >= 0) {
		global.__objectID2Depth[ objID ] = global.__objectDepths[i];
	} // end if
} // end for