/// @description Initializes car
/// @param car_type The car model that the stats are being set for

var car_type = argument0;

switch(car_type)
	{
	case car_model.type_1:
		//Assign Car Physical Properties
		sprite_index = spr_car_1;
		image_blend = 
		//Assign Car Stats
		init_car_stats(car_type);
		
	break;
	
	case car_model.type_2:
		//Assign Car Physical Properties
		sprite_index = spr_car_2;
		//Assign Car Stats
		init_car_stats(car_type);
	break;
	
	case car_model.type_3:
		//Assign Car Physical Properties
		sprite_index = spr_car_3;
		//Assign Car Stats
		init_car_stats(car_type);
	break;
	
	case car_model.type_4:
		//Assign Car Physical Properties
		sprite_index = spr_car_4;
		//Assign Car Stats
		init_car_stats(car_type);
	break;
	
	case car_model.type_5:
		//Assign Car Physical Properties
		sprite_index = spr_car_5;
		//Assign Car Stats
		init_car_stats(car_type);
	break;
	}