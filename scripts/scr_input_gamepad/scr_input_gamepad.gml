/// @description All Car Controls for Gamepad
/// @param Controlller Slot

//Bind function to slot
	var slot = argument0;

//Checks if car is on road
	var car_front_x = phy_position_x + lengthdir_x(128,-phy_rotation - 90);
	var car_front_y = phy_position_y + lengthdir_y(128,-phy_rotation - 90);
	var car_back_x = phy_position_x + lengthdir_x(-128,-phy_rotation - 90);
	var car_back_y = phy_position_y + lengthdir_y(-128,-phy_rotation - 90);
	
	var lay_id = layer_get_id("Tiles_Road");
	var map_id = layer_tilemap_get_id(lay_id);
	var car_front_wheels_road_contact = tilemap_get_at_pixel(map_id, car_front_x, car_front_y);
	var car_rear_wheels_road_contact = tilemap_get_at_pixel(map_id, car_back_x, car_back_y);
	var traction

	if car_front_wheels_road_contact == 0
	&& car_rear_wheels_road_contact == 0
		{
		traction = 0.6;
		}
	
	if car_front_wheels_road_contact != 0
	or car_rear_wheels_road_contact != 0
		{
		traction = 1;
		}
	
//Set Max Speed of Car
var _speed = ((max_speed/gear_count) * gear) * traction;

///Player Input
if gamepad_is_connected(slot)
   {
   
//Restart Game when Start is Pressed
	if gamepad_button_check_pressed(slot,gp_start)
		{
		room_restart();
		}
		
//Steering
   if phy_speed != 0
      {
	  var turn_angle = 3.5
	  var rotate = (turn_angle * gamepad_axis_value(slot,gp_axislh)) + 1;
	  
      if gamepad_axis_value(slot,gp_axislh) < -0.5
         {
         phy_rotation += rotate
         }
   
   if gamepad_axis_value(slot,gp_axislh) > 0.5
       {
       phy_rotation +=  rotate
	   
       }
       }
	   

//Gear Shift
	   
	   //Shift Up
	   if gamepad_button_check_pressed(slot,gp_shoulderr)
			{
			gear += 1
			}

	   //Shift Down
	   if gamepad_button_check_pressed(slot,gp_shoulderl)
			{
			gear -= 1
			}

		//Clamp Gears
		gear = clamp(gear,1,gear_count);
		
		
//Acceleration and Boost
       throttle = (gamepad_button_value(slot,gp_shoulderrb) + gamepad_button_value(slot,gp_face3));
	   if throttle > 0.1
          {

		  
		  if boost > 0
			{
			if gamepad_button_check(slot,gp_face3)
				{
				boost -= 1
				}
			}

		if boost <= 0
			{
			throttle = clamp(throttle,0,1);
			}
          forward_x = lengthdir_x(_speed,-phy_rotation - 90) * throttle;
          forward_y = lengthdir_y(_speed,-phy_rotation - 90) * throttle;
   
          physics_apply_force(phy_position_x,phy_position_y,forward_x,forward_y)
          if gamepad_button_check(slot,gp_face3) && boost > 0
			{
			camera_shake[0] = (phy_speed/512);
			}
		  }
		//Clamping Boost
		boost = clamp(boost,0,1000);
    
//Braking
    brake = gamepad_button_value(slot,gp_shoulderlb)
    if brake > 0.1
    && !gamepad_button_check(slot,gp_face2)
       {
       phy_speed_x = lerp(phy_speed_x,0,0.03 * (brake));
       phy_speed_y = lerp(phy_speed_y,0,0.03 * (brake));
       } 

//Reverse
    if brake > 0.1
    && gamepad_button_check(slot,gp_face2)
   {
   forward_x = lengthdir_x(-max_speed * 0.3,-phy_rotation - 90) * brake;
   forward_y = lengthdir_y(-max_speed * 0.3,-phy_rotation - 90) * brake;
   
   physics_apply_force(x,y,forward_x,forward_y)
   } 
}


//Setting Camera 
// Update Camera Position
camera_x[0] = x;
camera_y[0] = y;
var camera_theta = -phy_rotation + 90; 
camera_angle[0] = lerp(camera_angle[0],camera_theta,0.0035 + (0.0005 * (phy_speed/200)) + (0.04 * gamepad_button_value(slot,gp_shoulderlb)))
camera_z_distance[0] = lerp(camera_z_distance[0],1280 + (1280 * (phy_speed/100)),0.5);
camera_y_distance[0] = lerp(camera_y_distance[0],1 + (25 * (phy_speed/100)),0.5);
shake = shake * 0.95;

if gamepad_button_check_pressed(slot,gp_select)
	{
	menu_active = !menu_active;
	}