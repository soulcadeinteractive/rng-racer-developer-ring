/// @description Keyboard Control for RNG Racer

//Checks if car is on road
	var car_front_x = phy_position_x + lengthdir_x(128,-phy_rotation - 90);
	var car_front_y = phy_position_y + lengthdir_y(128,-phy_rotation - 90);
	var car_back_x = phy_position_x + lengthdir_x(-128,-phy_rotation - 90);
	var car_back_y = phy_position_y + lengthdir_y(-128,-phy_rotation - 90);
	
	var lay_id = layer_get_id("Tiles_Road");
	var map_id = layer_tilemap_get_id(lay_id);
	var car_front_wheels_road_contact = tilemap_get_at_pixel(map_id, car_front_x, car_front_y);
	var car_rear_wheels_road_contact = tilemap_get_at_pixel(map_id, car_back_x, car_back_y);
	var traction

	if car_front_wheels_road_contact == 0
	&& car_rear_wheels_road_contact == 0
		{
		traction = 0.6;
		}
	
	if car_front_wheels_road_contact != 0
	or car_rear_wheels_road_contact != 0
		{
		traction = 1;
		}
	
//Set Max Speed of Car
var _speed = ((max_speed/gear_count) * gear) * traction;


//Steering
   if phy_speed != 0
      {
	  var turn_angle = 3.5
	  var rotate = (turn_angle * ((-keyboard_check(vk_left) + (keyboard_check(vk_right)))))
	  
      phy_rotation += rotate
      
       }
	   

//Gear Shift
	   
	   //Shift Up
	   if keyboard_check_pressed(ord("X"))
			{
			gear += 1
			}

	   //Shift Down
	   if keyboard_check_pressed(ord("Z"))
			{
			gear -= 1
			}

		//Clamp Gears
		gear = clamp(gear,1,gear_count);
		
		
//Acceleration and Boost
       throttle = (keyboard_check(vk_space) + keyboard_check(vk_lshift));
	   if throttle > 0.1
          {

		  
		  if boost > 0
			{
			if keyboard_check(vk_lshift)
				{
				boost -= 1
				}
			}

		if boost <= 0
			{
			throttle = clamp(throttle,0,1);
			}
          forward_x = lengthdir_x(_speed,-phy_rotation - 90) * throttle;
          forward_y = lengthdir_y(_speed,-phy_rotation - 90) * throttle;
   
          physics_apply_force(phy_position_x,phy_position_y,forward_x,forward_y)
          if keyboard_check(vk_lshift) && boost > 0
			{
			camera_shake[0] = (phy_speed/512);
			}
		  }
		//Clamping Boost
		boost = clamp(boost,0,1000);
    
//Braking
    brake = keyboard_check(vk_lcontrol)
    if brake > 0.1
    && !keyboard_check(vk_down)
       {
       phy_speed_x = lerp(phy_speed_x,0,0.03 * (brake));
       phy_speed_y = lerp(phy_speed_y,0,0.03 * (brake));
       } 

//Reverse
    if brake > 0.1
    && keyboard_check(vk_down)
   {
   forward_x = lengthdir_x(-max_speed * 0.3,-phy_rotation - 90) * brake;
   forward_y = lengthdir_y(-max_speed * 0.3,-phy_rotation - 90) * brake;
   
   physics_apply_force(x,y,forward_x,forward_y)
   } 



//Setting Camera
// Update Camera Position
camera_x[0] = x;
camera_y[0] = y;
var camera_theta = -phy_rotation + 90; 
camera_angle[0] = lerp(camera_angle[0],camera_theta,0.0035 + (0.0005 * (phy_speed/200)) + (0.04 * brake))
camera_z_distance[0] = lerp(camera_z_distance[0],1280 + (1280 * (phy_speed/100)),0.5);
camera_y_distance[0] = lerp(camera_y_distance[0],1 + (25 * (phy_speed/100)),0.5);
shake = shake * 0.95;

if keyboard_check_pressed(vk_escape)
	{
	menu_active = !menu_active;
	}