//Establish textures
globalvar texture_brick,texture_fence,texture_concrete,texture_metal_wall;

texture_brick = sprite_get_texture(spr_texture_brick,0);
texture_fence = sprite_get_texture(spr_texture_fence,0);
texture_concrete = sprite_get_texture(spr_texture_concrete,0);
texture_metal_wall = sprite_get_texture(spr_texture_metal_wall,0);

//Establish Vertex Buffers
globalvar vert_wall_512x,vert_wall_1024x;
vert_wall_512x = vertex_buffer_create_quadrilateral_fixed(0,0,0,0,512,-512);
vert_wall_1024x = vertex_buffer_create_quadrilateral_fixed(0,0,0,0,1024,-1024);