///

//This code generates the horizontal section

//Initialize some needed variables to start off
var cell_size = 256;
var xx = argument0;
var yy = argument1;

var enter_contact_type = argument2;
var exit_contact_type = argument3;
var poc;

switch (enter_contact_type)
	{
	//Entering Left
	case point_of_contact.left:
		//Exiting Left
		if exit_contact_type == point_of_contact.left
			{
			poc = points_of_contact.left_to_left;
			scr_gen_horizontal_left_to_left(xx,yy,cell_size);
			}
		//Exiting Center
		if exit_contact_type == point_of_contact.center
			{
			poc = points_of_contact.left_to_center;
			scr_gen_horizontal_left_to_center(xx,yy,cell_size);
			}
		//Exiting Right
		if exit_contact_type == point_of_contact.right
			{
			poc = points_of_contact.left_to_right;
			scr_gen_horizontal_left_to_right(xx,yy,cell_size);
			}
	break;
	//Entering Centered
	case point_of_contact.center:
		//Exiting Left
		if exit_contact_type == point_of_contact.left
			{
			poc = points_of_contact.center_to_left;
			scr_gen_horizontal_center_to_left(xx,yy,cell_size);
			}
		//Exiting Center
		if exit_contact_type == point_of_contact.center
			{
			poc = points_of_contact.center_to_center;
			scr_gen_horizontal_center_to_center(xx,yy,cell_size);
			}
		//Exiting Right
		if exit_contact_type == point_of_contact.right
			{
			poc = points_of_contact.center_to_right;
			scr_gen_horizontal_center_to_right(xx,yy,cell_size);
			}
	break;
	//Entering Right
	case point_of_contact.right:
		//Exiting Left
		if exit_contact_type == point_of_contact.left
			{
			poc = points_of_contact.right_to_left;
			scr_gen_horizontal_right_to_left(xx,yy,cell_size);
			}
		//Exiting Center
		if exit_contact_type == point_of_contact.center
			{
			poc = points_of_contact.right_to_center;
			scr_gen_horizontal_right_to_center(xx,yy,cell_size);
			}
		//Exiting Right
		if exit_contact_type == point_of_contact.right
			{
			poc = points_of_contact.right_to_right;
			scr_gen_horizontal_right_to_right(xx,yy,cell_size);
			}
	break;
	}

