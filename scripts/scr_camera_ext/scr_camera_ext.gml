//view control
var camera_speed = argument0;
var camera_index = argument1;
__view_set( e__VW.XView, camera_index, __view_get( e__VW.XView, camera_index) + (((x-(__view_get( e__VW.WView, camera_index)/2)-__view_get( e__VW.XView, camera_index))) * camera_speed) + random_range(-shake,shake) );
__view_set( e__VW.YView, camera_index, __view_get( e__VW.YView, camera_index) + (((y-(__view_get( e__VW.HView, camera_index)/2)-__view_get( e__VW.YView, camera_index))) * camera_speed) + random_range(-shake,shake)  );

shake = shake * 0.95