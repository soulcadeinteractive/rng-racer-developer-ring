/// @description Player Input
/// @param Controller

var slot = argument0

switch(gamepad_is_connected(slot))
     {
     case true:
     scr_input_gamepad(slot);
     break;
     
     case false:
     scr_input_keyboard();
     break;
     }

