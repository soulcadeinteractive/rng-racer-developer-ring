/// @description Sets the stats for the car depending on the model
/// @param car_type The car model that the stats are being set for

var car_type = argument0;

switch(car_type)
	{
	case car_model.type_1:
	forward_x = 0;
	forward_y = 0;
	shake = 0;
	throttle = 0;
	boost = 2000;
	max_speed = 128000 * 2;
	gear_count = 2;
	gear = 1;
	break;
	
	case car_model.type_2:
	forward_x = 0;
	forward_y = 0;
	shake = 0;
	throttle = 0;
	boost = 1000;
	max_speed = 128000 * 2;
	gear_count = 2;
	gear = 1;
	break;
	
	case car_model.type_3:
	forward_x = 0;
	forward_y = 0;
	shake = 0;
	throttle = 0;
	boost = 1000;
	max_speed = 128000 * 2;
	gear_count = 2;
	gear = 1;
	break;
	
	case car_model.type_4:
	forward_x = 0;
	forward_y = 0;
	shake = 0;
	throttle = 0;
	boost = 1000;
	max_speed = 128000 * 2;
	gear_count = 2;
	gear = 1;
	break;
	
	case car_model.type_5:
	forward_x = 0;
	forward_y = 0;
	shake = 0;
	throttle = 0;
	boost = 1000;
	max_speed = 128000 * 2;
	gear_count = 2;
	gear = 1;
	break;
	}