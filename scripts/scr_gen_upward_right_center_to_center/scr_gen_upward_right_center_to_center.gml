var xx = argument0;
var yy = argument1;
var cell_size = argument2;
var select_section_varient = irandom_range(1,5);
var tileset = layer_tilemap_get_id("Tiles_Road")
switch (select_section_varient)
{	
	//***************************************************************************//
	//Just a straight up, uniform, curve from point to point.
	//***************************************************************************//
	case 1:
	tilemap_set_at_pixel(tileset,41,xx + (cell_size * 4),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 5),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 6),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 7),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 8),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 9),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 10),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 11),yy + (cell_size * 4));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 7),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 8),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 10),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 11),yy + (cell_size * 5));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 7),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 8),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 10),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 11),yy + (cell_size * 6));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,4,xx + (cell_size * 7),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 8),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 9),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 10),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 11),yy + (cell_size * 7));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 8));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 9));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 10));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 11));
	break;
	//***************************************************************************//
	//Enters, jerks left, and then goes right
	//***************************************************************************//
	case 2:
	tilemap_set_at_pixel(tileset,41,xx + (cell_size * 0),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 1),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 2),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 3),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 4),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 5),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 6),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 7),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 8),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 9),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 10),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 11),yy + (cell_size * 4));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 0),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 1),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 3),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 4),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 7),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 8),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 10),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 11),yy + (cell_size * 5));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 0),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 1),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 3),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 4),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 7),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 8),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 10),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 11),yy + (cell_size * 6));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 0),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 1),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,4,xx + (cell_size * 3),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 4),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 5),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 6),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 7),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 8),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 9),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 10),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 11),yy + (cell_size * 7));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 0),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 1),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 3),yy + (cell_size * 8));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 0),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 1),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,74,xx + (cell_size * 3),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 4),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 5),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 6),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,35,xx + (cell_size * 7),yy + (cell_size * 9));
	
	tilemap_set_at_pixel(tileset,22,xx + (cell_size * 0),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 1),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 2),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 3),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,87,xx + (cell_size * 4),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 10));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 11));
	break;
	//***************************************************************************//
	//Angled Straightshot
	//***************************************************************************//
	case 3:
	tilemap_set_at_pixel(tileset,41,xx + (cell_size * 7),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 8),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 9),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 10),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 11),yy + (cell_size * 4));

	tilemap_set_at_pixel(tileset,5,xx + (cell_size * 6),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,88,xx + (cell_size * 7),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 8),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 10),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 11),yy + (cell_size * 5));
	
	tilemap_set_at_pixel(tileset,75,xx + (cell_size * 6),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 7),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 8),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 10),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 11),yy + (cell_size * 6));
	
	tilemap_set_at_pixel(tileset,5,xx + (cell_size * 5),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,88,xx + (cell_size * 6),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 7),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 8),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,51,xx + (cell_size * 10),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 11),yy + (cell_size * 7));
	
	tilemap_set_at_pixel(tileset,75,xx + (cell_size * 5),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 7),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,4,xx + (cell_size * 8),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,38,xx + (cell_size * 9),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,32,xx + (cell_size * 10),yy + (cell_size * 8));
	
	tilemap_set_at_pixel(tileset,5,xx + (cell_size * 4),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,88,xx + (cell_size * 5),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 7),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,82,xx + (cell_size * 8),yy + (cell_size * 9));

	tilemap_set_at_pixel(tileset,75,xx + (cell_size * 4),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,69,xx + (cell_size * 7),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,63,xx + (cell_size * 8),yy + (cell_size * 10));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 11));
	break;
	//***************************************************************************//
	//Slanted "M" shape
	//***************************************************************************//
	case 4:
	tilemap_set_at_pixel(tileset,41,xx + (cell_size * 8),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 9),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 10),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 11),yy + (cell_size * 4));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 8),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 10),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 11),yy + (cell_size * 5));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 8),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 10),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 11),yy + (cell_size * 6));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 8),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,4,xx + (cell_size * 10),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 11),yy + (cell_size * 7));

	tilemap_set_at_pixel(tileset,41,xx + (cell_size * 4),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 5),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 6),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 7),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,68,xx + (cell_size * 8),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 10),yy + (cell_size * 8));

	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 7),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 8),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 10),yy + (cell_size * 9));

	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,4,xx + (cell_size * 7),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 8),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 9),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,16,xx + (cell_size * 10),yy + (cell_size * 10));

	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 11));
	break;
	
	case 5:
	//***************************************************************************//
	//Hard Angled Turn
	//***************************************************************************//
	tilemap_set_at_pixel(tileset,29,xx + (cell_size * 1),yy + (cell_size * 1));
	tilemap_set_at_pixel(tileset,23,xx + (cell_size * 2),yy + (cell_size * 1));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 3),yy + (cell_size * 1));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 4),yy + (cell_size * 1));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 5),yy + (cell_size * 1));
	tilemap_set_at_pixel(tileset,64,xx + (cell_size * 6),yy + (cell_size * 1));
	tilemap_set_at_pixel(tileset,58,xx + (cell_size * 7),yy + (cell_size * 1));

	tilemap_set_at_pixel(tileset,10,xx + (cell_size * 1),yy + (cell_size * 2));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 2));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 3),yy + (cell_size * 2));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 4),yy + (cell_size * 2));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 2));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 2));
	tilemap_set_at_pixel(tileset,39,xx + (cell_size * 7),yy + (cell_size * 2));
	tilemap_set_at_pixel(tileset,64,xx + (cell_size * 8),yy + (cell_size * 2));
	tilemap_set_at_pixel(tileset,58,xx + (cell_size * 9),yy + (cell_size * 2));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 1),yy + (cell_size * 3));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 3));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 3),yy + (cell_size * 3));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 4),yy + (cell_size * 3));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 3));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 3));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 7),yy + (cell_size * 3));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 8),yy + (cell_size * 3));
	tilemap_set_at_pixel(tileset,39,xx + (cell_size * 9),yy + (cell_size * 3));
	tilemap_set_at_pixel(tileset,64,xx + (cell_size * 10),yy + (cell_size * 3));
	tilemap_set_at_pixel(tileset,58,xx + (cell_size * 11),yy + (cell_size * 3));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 1),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 3),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,4,xx + (cell_size * 4),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 5),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,45,xx + (cell_size * 6),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 7),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 8),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 10),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,39,xx + (cell_size * 11),yy + (cell_size * 4));
	
	tilemap_set_at_pixel(tileset,56,xx + (cell_size * 1),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 3),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,50,xx + (cell_size * 4),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,44,xx + (cell_size * 5),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,26,xx + (cell_size * 6),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,20,xx + (cell_size * 7),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,45,xx + (cell_size * 8),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 10),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 11),yy + (cell_size * 5));
	
	tilemap_set_at_pixel(tileset,37,xx + (cell_size * 1),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,31,xx + (cell_size * 2),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 3),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 4),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,25,xx + (cell_size * 5),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,26,xx + (cell_size * 8),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,20,xx + (cell_size * 9),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,45,xx + (cell_size * 10),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 11),yy + (cell_size * 6));
	
	tilemap_set_at_pixel(tileset,56,xx + (cell_size * 2),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 3),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 4),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,50,xx + (cell_size * 5),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,44,xx + (cell_size * 6),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,26,xx + (cell_size * 10),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,20,xx + (cell_size * 11),yy + (cell_size * 7));
	
	tilemap_set_at_pixel(tileset,37,xx + (cell_size * 2),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,31,xx + (cell_size * 3),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 4),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,25,xx + (cell_size * 6),yy + (cell_size * 8));
	
	tilemap_set_at_pixel(tileset,56,xx + (cell_size * 3),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 4),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,50,xx + (cell_size * 6),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,44,xx + (cell_size * 7),yy + (cell_size * 9));
	
	tilemap_set_at_pixel(tileset,37,xx + (cell_size * 3),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,31,xx + (cell_size * 4),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,25,xx + (cell_size * 7),yy + (cell_size * 10));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 11));

	break;
	//***************************************************************************//
	//Default just incase something happens
	//***************************************************************************//
	default:
	tilemap_set_at_pixel(tileset,41,xx + (cell_size * 4),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 5),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 6),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 7),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 8),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 9),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 10),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 11),yy + (cell_size * 4));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 7),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 8),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 10),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 11),yy + (cell_size * 5));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 7),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 8),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 9),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 10),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 11),yy + (cell_size * 6));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,4,xx + (cell_size * 7),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 8),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 9),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 10),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 11),yy + (cell_size * 7));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 8));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 9));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 10));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 11));
	break;
}