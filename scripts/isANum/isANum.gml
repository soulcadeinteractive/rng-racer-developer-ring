///isANum(char)

// checks a character to see if it is a number

var char = argument0;

switch(char)
{
    case ord("0"):
    case ord("1"):
    case ord("2"):
    case ord("3"):
    case ord("4"):
    case ord("5"):
    case ord("6"):
    case ord("7"):
    case ord("8"):
    case ord("9"):
        return 1;
        break;
    default:
        return 0;
        break;
}
