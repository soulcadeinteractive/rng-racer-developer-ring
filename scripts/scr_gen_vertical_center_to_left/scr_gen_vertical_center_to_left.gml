///Template Tile Code

var xx = argument0;
var yy = argument1;
var cell_size = argument2;
var select_section_varient = choose(1);
var tileset = layer_tilemap_get_id("Tiles_Road")
switch (select_section_varient)
{	
case 1:
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 0),yy + (cell_size * 0));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 1),yy + (cell_size * 0));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 0));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 3),yy + (cell_size * 0));

	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 0),yy + (cell_size * 1));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 1),yy + (cell_size * 1));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 1));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 3),yy + (cell_size * 1));

	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 0),yy + (cell_size * 2));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 1),yy + (cell_size * 2));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 2));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 3),yy + (cell_size * 2));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 0),yy + (cell_size * 3));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 1),yy + (cell_size * 3));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 3));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 3),yy + (cell_size * 3));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 0),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 1),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,74,xx + (cell_size * 3),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 4),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 5),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,53,xx + (cell_size * 6),yy + (cell_size * 4));
	tilemap_set_at_pixel(tileset,35,xx + (cell_size * 7),yy + (cell_size * 4));

	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 0),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 1),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 3),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 4),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 5));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 5));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 0),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 1),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 2),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 3),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 4),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 6));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 6));
	
	tilemap_set_at_pixel(tileset,22,xx + (cell_size * 0),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 1),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 2),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,15,xx + (cell_size * 3),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,87,xx + (cell_size * 4),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 7));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 7));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 8));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 8));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 9));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 9));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 10));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 10));
	
	tilemap_set_at_pixel(tileset,40,xx + (cell_size * 4),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 5),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,34,xx + (cell_size * 6),yy + (cell_size * 11));
	tilemap_set_at_pixel(tileset,28,xx + (cell_size * 7),yy + (cell_size * 11));
	break;
}
