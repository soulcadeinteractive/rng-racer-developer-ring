//Left
if gamepad_axis_value(0,gp_axislh) < 0
   {
    self.key_left = abs(gamepad_axis_value(0,gp_axislh));
    self.key_right = 0;   
    }
//Right
if gamepad_axis_value(0,gp_axislh) > 0
   {
   self.key_right = abs(gamepad_axis_value(0,gp_axislh));   
   self.key_left = 0;
   }
//No Keyboard Input
if gamepad_axis_value(0,gp_axislh) == 0
   {
   self.key_right = 0;   
   self.key_left = 0;
   }
