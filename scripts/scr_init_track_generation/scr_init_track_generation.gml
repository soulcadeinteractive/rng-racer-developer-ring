var xx = argument0;
var yy = argument1;

randomize();
enum point_of_contact
	{
	center,left,right,
	}
	
enum points_of_contact
	{
	center_to_center,
    center_to_left,
    center_to_right,
    left_to_center,
    left_to_right,
    left_to_left,
    right_to_center,
    right_to_right,
    right_to_left,
	}

sd = 3072
var track_shape = choose(1,2);

switch(track_shape)	
	{
	case 1:
	var a1,a2,b1,b2,c1,c2,d1,d2,e1,e2,f1,f2;
	a1 = choose(point_of_contact.left,point_of_contact.left,point_of_contact.center,point_of_contact.right,point_of_contact.right);
	a2 = choose(point_of_contact.left,point_of_contact.left,point_of_contact.center,point_of_contact.right,point_of_contact.right);
	b2 = choose(point_of_contact.left,point_of_contact.left,point_of_contact.center,point_of_contact.right,point_of_contact.right);
	b1 = a2
	c2 = b1
	c1 = choose(point_of_contact.left,point_of_contact.left,point_of_contact.center,point_of_contact.right,point_of_contact.right);;
	d2 = c2
	d1 = choose(point_of_contact.left,point_of_contact.left,point_of_contact.center,point_of_contact.right,point_of_contact.right);
	e2 = choose(point_of_contact.left,point_of_contact.left,point_of_contact.center,point_of_contact.right,point_of_contact.right);
	e1 = d1
	f2 = a1;
	f1 = e1;
	
	scr_generate_upward_right_section(xx + 0,yy + 0,a1,a2);
	scr_generate_upward_left_section(xx + sd,yy + 0,b1,b2);
	scr_generate_vertical_section(xx + sd,yy + sd,c1,c2);
	scr_generate_downward_left_section(xx + sd,yy + (sd*2),d1,d2);
	scr_generate_downward_right_section(xx + 0,yy + (sd*2),e1,e2);
	scr_generate_vertical_section(xx + 0,yy + sd,f1,f2);
	
	break;
	
	case 2:
	var a1,a2,b1,b2,c1,c2,d1,d2;
	a1 = choose(point_of_contact.left,point_of_contact.center,point_of_contact.right);
	a2 = choose(point_of_contact.left,point_of_contact.center,point_of_contact.right);
	b1 = a2;
	b2 = choose(point_of_contact.left,point_of_contact.center,point_of_contact.right);
	c1 = b2;
	c2 = choose(point_of_contact.left,point_of_contact.center,point_of_contact.right);
	d1 = c2;
	d2 = a1

	
	scr_generate_upward_right_section(xx + 0,yy + 0,a1,a2);
	scr_generate_upward_left_section(xx + sd,yy + 0,b2,a2);
	scr_generate_downward_left_section(xx + sd,yy + sd,b2,c2);
	scr_generate_downward_right_section(xx + 0,yy + sd,a1,c2);
	
	break;

	}