/*
****************************************************************************************************************************************************************************************************************
                                                                
                                                                        MENU 101, KINDA
                                                                        
****************************************************************************************************************************************************************************************************************

                                            So I could go over each and everything but that would be a little insulting so I'll
                                                            just point out the big things you need to know

****************************************************************************************************************************************************************************************************************
Variables?
****************************************************************************************************************************************************************************************************************

Emumerates:
menu: The different types of menus and tabs for our menu state machine
    (main,submenu1,submenu2,submenu3,submenu4,submenu5,submenu6,submenu7,submenu8,singleplayer,multiplayer,settings)
     
Actual Variables:
main_tab: The main tab currently selected
current_menu: The current menu selected
info_text: Current text being drawn in the text box

All these variables for saving and lerping positions of buttons

button_1x
button_2x
button_3x
button_4x

tab1y
tab2y
tab3y 

bump_x
bump_y

****************************************************************************************************************************************************************************************************************
NABIL WHERE THE FUCK ARE THE BUTTONS???
****************************************************************************************************************************************************************************************************************

Chill. All the menu code is nested inside a series of state machines.

****************************************************************************************************************************************************************************************************************
NABIL HOW DO ALL THE BUTTONS WORK???
****************************************************************************************************************************************************************************************************************

Because I am the beautiful designer I am, the code for buttons are pretty uniform, however they are
usually broken up into two pieces and look like below 

(For the example, I took the Campaign Button from the main menu)

//Button 1

    //Set position for button
    if ui_rollover(scale_ui_coordinate(128,1920),scale_ui_coordinate(320,1080),spr_button,window_get_width(),window_get_height(),1920,1080,false)
        {
        button_1x = lerp(button_1x,128+bump,0.25);
        info_text = "Campaign:##Traverse a randomly generated story mode unique to you";
        }

    if !ui_rollover(scale_ui_coordinate(128,1920),scale_ui_coordinate(320,1080),spr_button,window_get_width(),window_get_height(),1920,1080,false)
        {
        button_1x = lerp(button_1x,128,0.25);
        }
    //Clicking the button
    if ui_mouse_released(scale_ui_coordinate(128,1920),scale_ui_coordinate(320,1080),spr_button,window_get_width(),window_get_height(),1920,1080,false,mb_left)
        {
        //Code for what happens when you click on me
        } 
        
        
And Usually off to the very bottom is the draw event for the button

draw_ui_element_ext(scale_ui_coordinate(button_1x,1920),scale_ui_coordinate(320,1080),spr_button,window_get_width(),window_get_height(),c_white, 0.9,"Campaign",fa_left,fa_middle,64,0,1920,1080)

****************************************************************************************************************************************************************************************************************
HOW DO THEY ACTUALLY GET CLICKED ON???
****************************************************************************************************************************************************************************************************************

Alright there may be a smidge of inconsistancy in the order but if you see:

    ui_make_area_interactive(x,y,sprite,window_width,window_height,base_width,base_height)
    
    and
    
    Any mouse code that looks like its interaction code 
    
    ex:
    if ui_make_area_interactive(x,y,sprite,window_width,window_height,base_width,base_height)
    && mouse_check_button_pressed(mb_left)
        {
         // some_sick_shit("badassactiongoeshere");
        }
    
That is probably the code you are looking for but I believe in any case where buttons 
are being drawn, I left an empty space for the action associated with that button

****************************************************************************************************************************************************************************************************************
*/
