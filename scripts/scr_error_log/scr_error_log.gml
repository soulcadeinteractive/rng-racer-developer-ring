///scr_error_log(error);
var arg = argument0;
switch(arg){
    case Error.NOSERVER:
        show_message("ERROR " + string(arg) + " Error.NOSERVER: NO SERVERS AVAILABLE.");
    break;
    case Error.NOMASTERSERVER:
        show_message("ERROR " + string(arg) + " Error.NOMASTERSERVER: NO MASTER SERVER.");
    break;
    case Error.UNKNOWN:
        show_message("ERROR " + string(arg) + " Error.UNKNOWN: SOMETHING WENT WRONG.");
    break;
    case Error.COUNTINGPROBLEM:
        show_message("ERROR " + string(arg) + " Error.COUNTINGPROBLEM: COUNT HAS RECENTLY BEEN UPDATED INCORRECTLY.");
    break;
    case Error.ALREADYBOOKMARKED:
        show_message("ERROR " + string(arg) + " Error.ALREADYBOOKMARKED: THIS SERVER HAS ALREADY BEEN BOOKMARKED.");
    break;        
}
