{
    "id": "aac2c63e-4cab-47b9-80e8-b0046d025f72",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path2_rm_1",
    "closed": false,
    "hsnap": 16,
    "kind": 1,
    "points": [
        {
            "id": "b2f7a16e-1533-44e5-af6a-c906ba98d3fc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2096,
            "y": 704,
            "speed": 100
        },
        {
            "id": "dacd5b9e-f74c-4b37-b896-e2fb12be714d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3680,
            "y": 768,
            "speed": 100
        },
        {
            "id": "cf516b30-2731-44d7-b200-3464b93b88fc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4224,
            "y": 1280,
            "speed": 100
        },
        {
            "id": "b69a97aa-e276-43ac-93bc-4e0b0d0def35",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4336,
            "y": 5824,
            "speed": 100
        },
        {
            "id": "4d10f26f-5639-43e9-b58b-924edf795842",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2624,
            "y": 5600,
            "speed": 100
        },
        {
            "id": "9ca40a3a-a910-440d-b628-07e44963670e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 5616,
            "speed": 100
        },
        {
            "id": "d7fdc68f-dfbe-44df-a570-3a6f59c2342b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1248,
            "y": 4976,
            "speed": 100
        },
        {
            "id": "4e884cff-01c7-4ff6-921e-b517a9674247",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1136,
            "y": 3872,
            "speed": 100
        },
        {
            "id": "8e95b9b0-d075-4d6b-a805-781d63419738",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2576,
            "y": 4288,
            "speed": 100
        },
        {
            "id": "47f3691a-c7c2-42eb-8189-89e6aa0a5f49",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2400,
            "y": 3440,
            "speed": 100
        },
        {
            "id": "f0a496f3-a662-4686-866a-61d1a14a4516",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 3344,
            "speed": 100
        },
        {
            "id": "6fad9424-a735-40db-8f32-a3a00847bec1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 896,
            "y": 224,
            "speed": 100
        },
        {
            "id": "57d4cb07-eff6-4303-8349-a87e53517ada",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2096,
            "y": 704,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 16
}