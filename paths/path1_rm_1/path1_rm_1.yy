{
    "id": "fd9d39e9-fefc-410a-a1bc-41a8a74544be",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path1_rm_1",
    "closed": false,
    "hsnap": 16,
    "kind": 1,
    "points": [
        {
            "id": "5cc17c11-a809-44dd-a7d3-34c1aa750263",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1984,
            "y": 512,
            "speed": 100
        },
        {
            "id": "7086f01e-7a72-4074-9510-9608cff21d05",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3968,
            "y": 496,
            "speed": 100
        },
        {
            "id": "6ad22489-1b95-41b8-8376-c3672ec4e7fd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3936,
            "y": 1280,
            "speed": 100
        },
        {
            "id": "7d618592-4e75-4f31-9a7e-4e836e36b796",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4352,
            "y": 5648,
            "speed": 100
        },
        {
            "id": "20895867-d57d-40ca-80da-cd52ea00f475",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2624,
            "y": 5600,
            "speed": 100
        },
        {
            "id": "01f0c12c-95a5-48b7-9f77-4820bffc7cfc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1136,
            "y": 5616,
            "speed": 100
        },
        {
            "id": "9fd828ac-5adf-46c6-9d86-78ccd119396b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1248,
            "y": 4976,
            "speed": 100
        },
        {
            "id": "bf943edd-4d4c-4311-a745-bc85b9cb762d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1152,
            "y": 4032,
            "speed": 100
        },
        {
            "id": "4c3e50d1-2df7-407f-9869-eb3a64b50262",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2352,
            "y": 4304,
            "speed": 100
        },
        {
            "id": "ff10b05e-928d-4ac9-9ebe-8f015e05050f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2400,
            "y": 3440,
            "speed": 100
        },
        {
            "id": "19ee3594-2ae6-4511-ace1-199e7b322ecf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 656,
            "y": 3360,
            "speed": 100
        },
        {
            "id": "0ed63a63-b26c-4bd0-a5da-8048bd3bee3a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1008,
            "y": 528,
            "speed": 100
        },
        {
            "id": "eb73d937-61f3-41f9-afe6-d80aecd5fa11",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1984,
            "y": 512,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 16
}