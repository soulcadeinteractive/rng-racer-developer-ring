{
    "id": "f96b3ce9-0149-431f-b588-d2d5c8fe86a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 309,
    "bbox_left": 0,
    "bbox_right": 169,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1d7537bf-8426-46d2-860e-85cfe2fa9ff2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f96b3ce9-0149-431f-b588-d2d5c8fe86a9",
            "compositeImage": {
                "id": "d1bd5c82-011d-4d6a-b26a-54635e287edf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d7537bf-8426-46d2-860e-85cfe2fa9ff2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fc41f7f-1c93-4775-abdd-42d927ae5f10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d7537bf-8426-46d2-860e-85cfe2fa9ff2",
                    "LayerId": "8ee2b4c1-eec9-4608-8581-e6a9b9cede85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 310,
    "layers": [
        {
            "id": "8ee2b4c1-eec9-4608-8581-e6a9b9cede85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f96b3ce9-0149-431f-b588-d2d5c8fe86a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 170,
    "xorig": 85,
    "yorig": 155
}