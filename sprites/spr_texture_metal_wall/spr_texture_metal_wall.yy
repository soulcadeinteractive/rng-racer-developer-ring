{
    "id": "270f10bb-f42f-4962-85bf-a0b066c6b122",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_texture_metal_wall",
    "For3D": true,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f336f6a5-a08d-4581-8fc8-39c0971c0c97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "270f10bb-f42f-4962-85bf-a0b066c6b122",
            "compositeImage": {
                "id": "f400f11e-52fd-45a3-bdbc-02ab51093cd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f336f6a5-a08d-4581-8fc8-39c0971c0c97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d00cae5-2236-435d-b1ba-95d9f45d27d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f336f6a5-a08d-4581-8fc8-39c0971c0c97",
                    "LayerId": "547c5daa-4db1-49be-8d84-a5284fda537d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "547c5daa-4db1-49be-8d84-a5284fda537d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "270f10bb-f42f-4962-85bf-a0b066c6b122",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}