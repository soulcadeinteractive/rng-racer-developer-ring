{
    "id": "9a77cb1a-6b78-4e9a-b544-cc8bc26a7a1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 335,
    "bbox_left": 0,
    "bbox_right": 167,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "880dd04b-6cd5-4358-b8d1-19270869c099",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a77cb1a-6b78-4e9a-b544-cc8bc26a7a1b",
            "compositeImage": {
                "id": "eb6e2f99-5db7-4cb1-8ee3-de10cf9a39ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "880dd04b-6cd5-4358-b8d1-19270869c099",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "563d57e7-05f4-4832-8128-704667a28ce4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "880dd04b-6cd5-4358-b8d1-19270869c099",
                    "LayerId": "35b68e31-a652-453d-b4ab-d9104874fb02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 336,
    "layers": [
        {
            "id": "35b68e31-a652-453d-b4ab-d9104874fb02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a77cb1a-6b78-4e9a-b544-cc8bc26a7a1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 84,
    "yorig": 168
}