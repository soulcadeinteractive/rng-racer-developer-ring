{
    "id": "ef7053bc-2e13-4900-ad6d-fa993260a0b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_info_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 351,
    "bbox_left": 0,
    "bbox_right": 1007,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c87d3b1b-d89d-4a6b-ab95-ed7104285090",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7053bc-2e13-4900-ad6d-fa993260a0b2",
            "compositeImage": {
                "id": "f28b4c10-50a0-43e3-a5bc-5216e8fd9008",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c87d3b1b-d89d-4a6b-ab95-ed7104285090",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f6c156e-77be-437c-9b7a-1286ccce94f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c87d3b1b-d89d-4a6b-ab95-ed7104285090",
                    "LayerId": "db19d834-09f3-4584-beaa-7240cb1f1cde"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 352,
    "layers": [
        {
            "id": "db19d834-09f3-4584-beaa-7240cb1f1cde",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef7053bc-2e13-4900-ad6d-fa993260a0b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1008,
    "xorig": 0,
    "yorig": 0
}