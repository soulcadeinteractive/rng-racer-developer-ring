{
    "id": "592c993d-958c-499e-a8b2-6186c722fa99",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "79586c3a-fd8e-4e95-8618-c9c64d7a49fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "592c993d-958c-499e-a8b2-6186c722fa99",
            "compositeImage": {
                "id": "3c1ec593-333e-461c-8d7c-f5f85d43bd95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79586c3a-fd8e-4e95-8618-c9c64d7a49fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bc9b8a8-1be1-45e4-a026-0a72a8d85bab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79586c3a-fd8e-4e95-8618-c9c64d7a49fe",
                    "LayerId": "532b48b3-1f6f-4a11-9b89-929ccc8ce032"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "532b48b3-1f6f-4a11-9b89-929ccc8ce032",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "592c993d-958c-499e-a8b2-6186c722fa99",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 9
}