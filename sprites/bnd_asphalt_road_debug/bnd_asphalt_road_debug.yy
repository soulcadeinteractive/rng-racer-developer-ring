{
    "id": "41e935ce-774e-4a35-bbb8-a9fc0fc515a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bnd_asphalt_road_debug",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3898,
    "bbox_left": 0,
    "bbox_right": 1560,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ad2b7231-f920-49b4-9015-dc956b1ad12c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41e935ce-774e-4a35-bbb8-a9fc0fc515a4",
            "compositeImage": {
                "id": "c24712da-7706-4c46-8fc9-703f603f06f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad2b7231-f920-49b4-9015-dc956b1ad12c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9d57bfc-fd58-4c85-9a0f-8b72a1fd1ee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad2b7231-f920-49b4-9015-dc956b1ad12c",
                    "LayerId": "6d3f386c-0a8e-47fa-8f2e-6aafbcac3e2e"
                },
                {
                    "id": "061d54f0-9d8d-4187-9fea-9726d69ccd6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad2b7231-f920-49b4-9015-dc956b1ad12c",
                    "LayerId": "2771ef33-27e7-44d2-b621-94662eb9bd2d"
                },
                {
                    "id": "00f9c001-c23f-4920-9cd7-63fe11ef7637",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad2b7231-f920-49b4-9015-dc956b1ad12c",
                    "LayerId": "bc3cd248-9022-41c6-88e5-04071e7e1d84"
                }
            ]
        }
    ],
    "gridX": 260,
    "gridY": 260,
    "height": 3899,
    "layers": [
        {
            "id": "2771ef33-27e7-44d2-b621-94662eb9bd2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41e935ce-774e-4a35-bbb8-a9fc0fc515a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "bc3cd248-9022-41c6-88e5-04071e7e1d84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41e935ce-774e-4a35-bbb8-a9fc0fc515a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6d3f386c-0a8e-47fa-8f2e-6aafbcac3e2e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41e935ce-774e-4a35-bbb8-a9fc0fc515a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 22,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1561,
    "xorig": 0,
    "yorig": 0
}