{
    "id": "60177df3-4023-44b9-9b12-8f530ac848bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite34",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "489fac81-769d-4197-8769-023b70077aad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60177df3-4023-44b9-9b12-8f530ac848bb",
            "compositeImage": {
                "id": "989d76b2-b75e-4bed-b079-12496ebbbd7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "489fac81-769d-4197-8769-023b70077aad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "834f6fea-70fc-4364-9afb-d8f11e30f8c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "489fac81-769d-4197-8769-023b70077aad",
                    "LayerId": "565a623e-125b-4c27-aada-cc57c623a262"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "565a623e-125b-4c27-aada-cc57c623a262",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60177df3-4023-44b9-9b12-8f530ac848bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}