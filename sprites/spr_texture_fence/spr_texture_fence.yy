{
    "id": "6c4b4a21-da20-4fca-904e-c309d5748fc3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_texture_fence",
    "For3D": true,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "cf346750-a262-468a-aa34-0658301ad9b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c4b4a21-da20-4fca-904e-c309d5748fc3",
            "compositeImage": {
                "id": "84dbc7a9-c9cd-40c4-9a9c-0812b6c104d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf346750-a262-468a-aa34-0658301ad9b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f2f5951-3cb4-42bb-afc1-794d6b6a298a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf346750-a262-468a-aa34-0658301ad9b4",
                    "LayerId": "5ff1b1d5-445e-4faf-88f2-46ac9d4b09a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "5ff1b1d5-445e-4faf-88f2-46ac9d4b09a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c4b4a21-da20-4fca-904e-c309d5748fc3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 570,
    "yorig": 233
}