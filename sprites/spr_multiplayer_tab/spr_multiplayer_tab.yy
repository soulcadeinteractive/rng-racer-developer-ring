{
    "id": "86742016-a86b-40a8-b165-8415aa86d841",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_multiplayer_tab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "88779107-9fa6-408e-bffe-03834d4b1eb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86742016-a86b-40a8-b165-8415aa86d841",
            "compositeImage": {
                "id": "df86cdd6-c179-4436-8c59-0ae276aa6bbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88779107-9fa6-408e-bffe-03834d4b1eb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11d249e0-976d-4ade-a2fa-b2f5b13a6ea9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88779107-9fa6-408e-bffe-03834d4b1eb1",
                    "LayerId": "83310c64-7df3-4c44-a4f5-9342ef374a49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "83310c64-7df3-4c44-a4f5-9342ef374a49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86742016-a86b-40a8-b165-8415aa86d841",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}