{
    "id": "389d2332-bb78-422a-8c00-94240f297bd7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_test_car",
    "For3D": true,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1341,
    "bbox_left": 0,
    "bbox_right": 670,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a0dabbbe-c86b-4ea7-9dc7-4a7f9b526df5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "389d2332-bb78-422a-8c00-94240f297bd7",
            "compositeImage": {
                "id": "be16a829-f6e3-4a4a-975f-137621bc0d9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0dabbbe-c86b-4ea7-9dc7-4a7f9b526df5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e136dae4-e263-45c4-a7bb-ea4f1161ded5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0dabbbe-c86b-4ea7-9dc7-4a7f9b526df5",
                    "LayerId": "4768e6e3-7b0e-4d49-8013-3e83faceb64f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1342,
    "layers": [
        {
            "id": "4768e6e3-7b0e-4d49-8013-3e83faceb64f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "389d2332-bb78-422a-8c00-94240f297bd7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 671,
    "xorig": 335,
    "yorig": 671
}