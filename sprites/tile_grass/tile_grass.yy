{
    "id": "c98e7996-cd65-48a6-be06-5a5ea11b29bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tile_grass",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 511,
    "bbox_left": 512,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3ae628bc-5990-48b7-b1d7-2ebc26ed4f29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c98e7996-cd65-48a6-be06-5a5ea11b29bc",
            "compositeImage": {
                "id": "6c88e1b1-60cd-4ebb-9c0f-2c001d613458",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ae628bc-5990-48b7-b1d7-2ebc26ed4f29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2ce7f1a-25b0-4254-859b-5c974650c5d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ae628bc-5990-48b7-b1d7-2ebc26ed4f29",
                    "LayerId": "adc3b69f-14b5-4afc-9500-6dc0c89b3be4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "adc3b69f-14b5-4afc-9500-6dc0c89b3be4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c98e7996-cd65-48a6-be06-5a5ea11b29bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}