{
    "id": "b76bdd18-7f7a-4b14-bc55-ff0b44f35c21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 315,
    "bbox_left": 0,
    "bbox_right": 171,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "aa5b2f6a-a86b-4ffd-9fa0-8d348ed0ce24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76bdd18-7f7a-4b14-bc55-ff0b44f35c21",
            "compositeImage": {
                "id": "939e96d3-8414-4b3c-be9c-ddada6e3333d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa5b2f6a-a86b-4ffd-9fa0-8d348ed0ce24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "674b835d-ae17-4d04-a224-ee3260687092",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa5b2f6a-a86b-4ffd-9fa0-8d348ed0ce24",
                    "LayerId": "94ae8bc1-0afc-4990-9c0b-e2372c8c88a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 316,
    "layers": [
        {
            "id": "94ae8bc1-0afc-4990-9c0b-e2372c8c88a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b76bdd18-7f7a-4b14-bc55-ff0b44f35c21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 172,
    "xorig": 86,
    "yorig": 158
}