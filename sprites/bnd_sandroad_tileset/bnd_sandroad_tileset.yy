{
    "id": "893b3e82-6710-4d41-a83e-a3089f95f837",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bnd_sandroad_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3898,
    "bbox_left": 0,
    "bbox_right": 1817,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ad1ed70f-0f89-4101-807c-4963329f1225",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "893b3e82-6710-4d41-a83e-a3089f95f837",
            "compositeImage": {
                "id": "a9279067-7811-49c4-862f-31e6786572f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad1ed70f-0f89-4101-807c-4963329f1225",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec4e840c-0115-42c7-acf4-3325bbf84318",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad1ed70f-0f89-4101-807c-4963329f1225",
                    "LayerId": "d0e446a3-719a-4381-96c8-3a77f8b101ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3899,
    "layers": [
        {
            "id": "d0e446a3-719a-4381-96c8-3a77f8b101ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "893b3e82-6710-4d41-a83e-a3089f95f837",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1818,
    "xorig": 0,
    "yorig": 0
}