{
    "id": "dc7f3458-89c3-4b0f-b9d3-46f1b2afb567",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fence_horz",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ee54de2a-1675-4dd2-bd71-7b27c287247b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc7f3458-89c3-4b0f-b9d3-46f1b2afb567",
            "compositeImage": {
                "id": "49305520-3d39-4165-b78e-bd1ab9439c5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee54de2a-1675-4dd2-bd71-7b27c287247b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b85178f-ddc0-4ab3-84c5-57591c23c166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee54de2a-1675-4dd2-bd71-7b27c287247b",
                    "LayerId": "ea77e2b1-896f-40b0-bdb0-141631e2cdec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "ea77e2b1-896f-40b0-bdb0-141631e2cdec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc7f3458-89c3-4b0f-b9d3-46f1b2afb567",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}