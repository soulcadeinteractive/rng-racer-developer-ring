{
    "id": "7516751a-08b0-4ed4-a37e-48559d9350be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bnd_grass",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "fbe333b7-10d7-463c-80de-578ab12a188a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7516751a-08b0-4ed4-a37e-48559d9350be",
            "compositeImage": {
                "id": "9feab31b-91a8-4821-8dce-d21b2861b9a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbe333b7-10d7-463c-80de-578ab12a188a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34746156-1b77-4968-bad1-426bbe6e88d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbe333b7-10d7-463c-80de-578ab12a188a",
                    "LayerId": "0b210735-eeff-4cf2-925c-ff468d7b51b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "0b210735-eeff-4cf2-925c-ff468d7b51b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7516751a-08b0-4ed4-a37e-48559d9350be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}