{
    "id": "804015fc-9ca3-43df-a293-92aaa9f3d272",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bnd_sand_roads",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 1948,
    "bbox_left": 0,
    "bbox_right": 908,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ecca4257-0fe1-4e0c-9500-2c5a461b9f6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "804015fc-9ca3-43df-a293-92aaa9f3d272",
            "compositeImage": {
                "id": "3e16b835-d224-4e3c-8ca5-2203e4d80d55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecca4257-0fe1-4e0c-9500-2c5a461b9f6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db3c3c36-7495-4698-ad15-d76035939d30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecca4257-0fe1-4e0c-9500-2c5a461b9f6e",
                    "LayerId": "f5bef837-2945-45fb-8426-d2dd7126e1bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1949,
    "layers": [
        {
            "id": "f5bef837-2945-45fb-8426-d2dd7126e1bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "804015fc-9ca3-43df-a293-92aaa9f3d272",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 909,
    "xorig": 0,
    "yorig": 0
}