{
    "id": "9b5ebe46-7690-4b4d-a704-69acaee10501",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bnd_dirt_road",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3898,
    "bbox_left": 0,
    "bbox_right": 2079,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d93e1c3c-515d-4ea3-afc7-7f04dcb5837b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b5ebe46-7690-4b4d-a704-69acaee10501",
            "compositeImage": {
                "id": "c90d9e3f-81eb-4f37-a730-2f4f8b0bee84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d93e1c3c-515d-4ea3-afc7-7f04dcb5837b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78ce585e-33b0-4398-b819-5222e9f114d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d93e1c3c-515d-4ea3-afc7-7f04dcb5837b",
                    "LayerId": "fc91c41e-e8de-491d-b5ff-10e8bb4e0800"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3899,
    "layers": [
        {
            "id": "fc91c41e-e8de-491d-b5ff-10e8bb4e0800",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b5ebe46-7690-4b4d-a704-69acaee10501",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2080,
    "xorig": 0,
    "yorig": 0
}