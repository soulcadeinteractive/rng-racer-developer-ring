{
    "id": "ffb15b25-f7d3-48e2-94c4-90520ebe84bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bnd_dirt_roads",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 1948,
    "bbox_left": 0,
    "bbox_right": 1040,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5e20ebb2-6e98-415c-a1d4-e03153b2bda1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffb15b25-f7d3-48e2-94c4-90520ebe84bc",
            "compositeImage": {
                "id": "e4effc57-9fe4-4bae-bb8c-bbcadb3398a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e20ebb2-6e98-415c-a1d4-e03153b2bda1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "849b818e-5be6-44e7-921d-80ec24ebe09b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e20ebb2-6e98-415c-a1d4-e03153b2bda1",
                    "LayerId": "e44aef34-78de-4e9b-b044-7b3cd1cfa13a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1949,
    "layers": [
        {
            "id": "e44aef34-78de-4e9b-b044-7b3cd1cfa13a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffb15b25-f7d3-48e2-94c4-90520ebe84bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1041,
    "xorig": 0,
    "yorig": 0
}