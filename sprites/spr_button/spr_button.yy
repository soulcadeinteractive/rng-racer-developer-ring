{
    "id": "c86ec2a9-4856-4ca6-8db6-65731d4004e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 575,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e3a0eb91-753e-46c0-a65b-6045b7a4f6c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c86ec2a9-4856-4ca6-8db6-65731d4004e2",
            "compositeImage": {
                "id": "0b975d7e-f229-4f43-bfa1-d707d8a5c3ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3a0eb91-753e-46c0-a65b-6045b7a4f6c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "452a93eb-3e10-479a-850b-3e1d628822a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3a0eb91-753e-46c0-a65b-6045b7a4f6c0",
                    "LayerId": "3883142c-5fca-4dc2-82f0-3e3b842375d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3883142c-5fca-4dc2-82f0-3e3b842375d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c86ec2a9-4856-4ca6-8db6-65731d4004e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 576,
    "xorig": 0,
    "yorig": 0
}