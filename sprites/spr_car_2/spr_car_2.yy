{
    "id": "f4ce18f3-c16f-4162-996f-4bfc4931c055",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 333,
    "bbox_left": 0,
    "bbox_right": 178,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "78222312-40d2-427a-9f3d-b51eb1d7fd2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ce18f3-c16f-4162-996f-4bfc4931c055",
            "compositeImage": {
                "id": "684e2753-986b-4454-8d34-e98f0c24a0e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78222312-40d2-427a-9f3d-b51eb1d7fd2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22e092e7-68b3-4eda-b18f-64a792f98e37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78222312-40d2-427a-9f3d-b51eb1d7fd2a",
                    "LayerId": "d0c73982-41d9-42a6-a69f-4fa632ac6ce5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 334,
    "layers": [
        {
            "id": "d0c73982-41d9-42a6-a69f-4fa632ac6ce5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4ce18f3-c16f-4162-996f-4bfc4931c055",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 179,
    "xorig": 89,
    "yorig": 167
}