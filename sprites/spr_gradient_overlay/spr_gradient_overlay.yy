{
    "id": "71a310a6-a305-43ad-be99-ed1cbc666384",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gradient_overlay",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 514,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c4f476a6-91f4-4794-b398-01a3ec909a28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71a310a6-a305-43ad-be99-ed1cbc666384",
            "compositeImage": {
                "id": "dbcf960b-4382-40f9-a45f-db6b2d0dc710",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4f476a6-91f4-4794-b398-01a3ec909a28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9fac6e0-3f3c-40ec-b09f-ce791987f1ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4f476a6-91f4-4794-b398-01a3ec909a28",
                    "LayerId": "fe693a0a-c92d-4732-abd8-ab570086cfe8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 515,
    "layers": [
        {
            "id": "fe693a0a-c92d-4732-abd8-ab570086cfe8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71a310a6-a305-43ad-be99-ed1cbc666384",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}