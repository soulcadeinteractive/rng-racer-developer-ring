{
    "id": "ab81a62b-3b2f-47ee-9272-ee53d269a18d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bnd_asphalt_road",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3898,
    "bbox_left": 0,
    "bbox_right": 1560,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e6e29c3f-b623-418f-a2d7-c4f30e7b4e84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab81a62b-3b2f-47ee-9272-ee53d269a18d",
            "compositeImage": {
                "id": "16aa608c-4de3-4e87-91b4-0f1e0312adad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6e29c3f-b623-418f-a2d7-c4f30e7b4e84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4071ef4-dce0-41c4-ac46-9e6708c6a2cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6e29c3f-b623-418f-a2d7-c4f30e7b4e84",
                    "LayerId": "b0930df1-021f-4b59-a7f6-ded881ab5537"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3899,
    "layers": [
        {
            "id": "b0930df1-021f-4b59-a7f6-ded881ab5537",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab81a62b-3b2f-47ee-9272-ee53d269a18d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1561,
    "xorig": 0,
    "yorig": 0
}