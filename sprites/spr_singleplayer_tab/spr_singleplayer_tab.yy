{
    "id": "aa77d387-83f2-432e-832f-8406f68470ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_singleplayer_tab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2c3ef1f5-53a9-4ae9-9b77-374c64c1bd7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa77d387-83f2-432e-832f-8406f68470ec",
            "compositeImage": {
                "id": "7aee9edb-7583-4d3b-89d6-5b2789a23c33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c3ef1f5-53a9-4ae9-9b77-374c64c1bd7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "757f9341-bb51-4513-ba28-152ce2086d92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c3ef1f5-53a9-4ae9-9b77-374c64c1bd7e",
                    "LayerId": "2f82bcc6-ef72-49e3-a9de-28d74e972c2d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "2f82bcc6-ef72-49e3-a9de-28d74e972c2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa77d387-83f2-432e-832f-8406f68470ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}