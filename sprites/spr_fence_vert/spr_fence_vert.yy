{
    "id": "51bb04d7-0437-4da0-924e-3a4e3d65e2bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fence_vert",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9bb2578a-c7e0-45db-8206-0679d0f0f71c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51bb04d7-0437-4da0-924e-3a4e3d65e2bf",
            "compositeImage": {
                "id": "91c1332e-6f92-4705-8cb1-f80f810d871e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bb2578a-c7e0-45db-8206-0679d0f0f71c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a51172e7-642d-4ed9-9410-bf7077eafaec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bb2578a-c7e0-45db-8206-0679d0f0f71c",
                    "LayerId": "2066dfeb-c47c-4aaa-a468-d6ab6dc366ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "2066dfeb-c47c-4aaa-a468-d6ab6dc366ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51bb04d7-0437-4da0-924e-3a4e3d65e2bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}