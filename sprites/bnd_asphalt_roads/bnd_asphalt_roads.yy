{
    "id": "f884085e-e7bf-48f8-ae59-f227af33c816",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bnd_asphalt_roads",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 1948,
    "bbox_left": 0,
    "bbox_right": 779,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b8e363fc-b041-4f80-92ff-9fc8c604647b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f884085e-e7bf-48f8-ae59-f227af33c816",
            "compositeImage": {
                "id": "1000f292-beae-4b2d-b7c3-29d8fadcdf28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8e363fc-b041-4f80-92ff-9fc8c604647b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87d8cd4a-7b62-4a0c-a4f5-8c85b42fb3bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8e363fc-b041-4f80-92ff-9fc8c604647b",
                    "LayerId": "1943dabb-ac50-40d6-81c3-5f4965767e0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1949,
    "layers": [
        {
            "id": "1943dabb-ac50-40d6-81c3-5f4965767e0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f884085e-e7bf-48f8-ae59-f227af33c816",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 780,
    "xorig": 0,
    "yorig": 0
}