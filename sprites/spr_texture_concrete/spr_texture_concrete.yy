{
    "id": "4e66e7f4-841e-4afd-9462-869436586917",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_texture_concrete",
    "For3D": true,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8cc54c83-77b4-4ecf-b6ca-7116b87c1180",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e66e7f4-841e-4afd-9462-869436586917",
            "compositeImage": {
                "id": "23e624f7-2aa6-4a76-aa70-c274291435fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cc54c83-77b4-4ecf-b6ca-7116b87c1180",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15529217-b3f1-42e3-a941-2728d8699104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cc54c83-77b4-4ecf-b6ca-7116b87c1180",
                    "LayerId": "8bae527f-cf31-4f70-9f90-fd4932273e5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "8bae527f-cf31-4f70-9f90-fd4932273e5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e66e7f4-841e-4afd-9462-869436586917",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}