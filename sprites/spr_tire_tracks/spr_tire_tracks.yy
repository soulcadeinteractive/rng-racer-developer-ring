{
    "id": "fbcabe76-58b8-4a43-9070-47c1df6a988c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tire_tracks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 333,
    "bbox_left": 0,
    "bbox_right": 179,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b1184a8f-0ff3-4e44-a936-6407ae8310ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbcabe76-58b8-4a43-9070-47c1df6a988c",
            "compositeImage": {
                "id": "d71ab9b4-5255-4c1e-9c01-ec8c8248888c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1184a8f-0ff3-4e44-a936-6407ae8310ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdd69f7e-ca88-4dce-be33-ad0ec6c99b05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1184a8f-0ff3-4e44-a936-6407ae8310ae",
                    "LayerId": "479430f3-c9c6-429b-8a54-72fc052d431c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 334,
    "layers": [
        {
            "id": "479430f3-c9c6-429b-8a54-72fc052d431c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbcabe76-58b8-4a43-9070-47c1df6a988c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 180,
    "xorig": 90,
    "yorig": 167
}