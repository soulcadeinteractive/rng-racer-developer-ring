{
    "id": "0e02fb5a-0b95-4c04-9f39-b0befb6a6762",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_ground",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b06f1622-f497-49f4-82ce-10283f76ae68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e02fb5a-0b95-4c04-9f39-b0befb6a6762",
            "compositeImage": {
                "id": "1f5f7f16-4f03-47dc-8818-58151b791ef1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b06f1622-f497-49f4-82ce-10283f76ae68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a64c1a2c-2dbe-4347-995e-c7a032d8f1e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b06f1622-f497-49f4-82ce-10283f76ae68",
                    "LayerId": "acbc9142-231c-487c-a6e5-1470a61f524d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "acbc9142-231c-487c-a6e5-1470a61f524d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e02fb5a-0b95-4c04-9f39-b0befb6a6762",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}