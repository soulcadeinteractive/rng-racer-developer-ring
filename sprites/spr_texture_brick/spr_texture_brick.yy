{
    "id": "59ba91d3-5107-4220-bdbd-741aec0b8e11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_texture_brick",
    "For3D": true,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a42f7409-5df7-4cda-89db-9a5dbc960e0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59ba91d3-5107-4220-bdbd-741aec0b8e11",
            "compositeImage": {
                "id": "61115634-a45c-4389-a274-580d432c2f8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a42f7409-5df7-4cda-89db-9a5dbc960e0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bc360a4-0a98-48ab-b47e-5c92e1d030af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a42f7409-5df7-4cda-89db-9a5dbc960e0b",
                    "LayerId": "40b4c183-abd2-4854-8632-be90c5019aef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "40b4c183-abd2-4854-8632-be90c5019aef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59ba91d3-5107-4220-bdbd-741aec0b8e11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}