{
    "id": "6e820fda-bb65-4128-b1cf-288b53f221c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 575,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a0f5342-8591-4088-9e12-b45147614df7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e820fda-bb65-4128-b1cf-288b53f221c5",
            "compositeImage": {
                "id": "c26f9834-dc79-40ea-8300-5ca868b13e45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a0f5342-8591-4088-9e12-b45147614df7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1de3d52b-8a57-476e-a05d-0a284df6cc0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a0f5342-8591-4088-9e12-b45147614df7",
                    "LayerId": "2935550b-d4b0-45c8-b364-38165e33f831"
                },
                {
                    "id": "d528e0d3-0682-4bc9-bac1-284d356bd10e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a0f5342-8591-4088-9e12-b45147614df7",
                    "LayerId": "50bf9e72-0fee-4839-bbd3-f008aee10d68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "50bf9e72-0fee-4839-bbd3-f008aee10d68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e820fda-bb65-4128-b1cf-288b53f221c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "2935550b-d4b0-45c8-b364-38165e33f831",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e820fda-bb65-4128-b1cf-288b53f221c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 576,
    "xorig": 0,
    "yorig": 0
}