{
    "id": "862ffbd9-36ca-4d32-9036-bc20e386c60a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 334,
    "bbox_left": 0,
    "bbox_right": 162,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3f9d8057-edae-4525-9e18-bf5f1fd625bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "862ffbd9-36ca-4d32-9036-bc20e386c60a",
            "compositeImage": {
                "id": "37e45b1c-acce-4dc1-a0d7-c62b507171cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f9d8057-edae-4525-9e18-bf5f1fd625bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3640bd48-2d9f-4493-9c55-b117abda0def",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f9d8057-edae-4525-9e18-bf5f1fd625bb",
                    "LayerId": "a7d9c4a1-8214-4402-b376-359b2f58f8b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 335,
    "layers": [
        {
            "id": "a7d9c4a1-8214-4402-b376-359b2f58f8b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "862ffbd9-36ca-4d32-9036-bc20e386c60a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 163,
    "xorig": 81,
    "yorig": 167
}