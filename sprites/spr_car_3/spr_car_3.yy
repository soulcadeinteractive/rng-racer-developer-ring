{
    "id": "92c13196-7cd8-4a8b-8997-29e08244d283",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 348,
    "bbox_left": 0,
    "bbox_right": 160,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "67eed30c-dcc0-4701-be0c-0b7ee5c8eb01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92c13196-7cd8-4a8b-8997-29e08244d283",
            "compositeImage": {
                "id": "09af77b1-a9d2-4c3c-9e84-a4a180cebaf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67eed30c-dcc0-4701-be0c-0b7ee5c8eb01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a56d050-d4a6-44e2-82ab-7a1516402ce2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67eed30c-dcc0-4701-be0c-0b7ee5c8eb01",
                    "LayerId": "bd442040-46ef-477e-8e4d-813760a1bddd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 349,
    "layers": [
        {
            "id": "bd442040-46ef-477e-8e4d-813760a1bddd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92c13196-7cd8-4a8b-8997-29e08244d283",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 161,
    "xorig": 80,
    "yorig": 174
}