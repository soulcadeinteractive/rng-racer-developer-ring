{
    "id": "ba2f8fd7-bef8-4f2d-930d-614b3fb6d443",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_npc_target",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "74609d70-4b8a-4852-90da-f34be654dfc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba2f8fd7-bef8-4f2d-930d-614b3fb6d443",
            "compositeImage": {
                "id": "0c356235-d87f-4aa9-9477-231015cc5998",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74609d70-4b8a-4852-90da-f34be654dfc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8898a6c-adaa-45ac-841e-cc0f95ddf496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74609d70-4b8a-4852-90da-f34be654dfc6",
                    "LayerId": "834c157c-1b74-4d8e-a237-0901422a0f08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "834c157c-1b74-4d8e-a237-0901422a0f08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba2f8fd7-bef8-4f2d-930d-614b3fb6d443",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}