{
    "id": "5b7fbf9c-bb3a-4dd3-a4b6-260d23f1cc35",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "Steamworks_gml",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 9223372036854775807,
    "date": "2017-44-21 12:11:52",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "eff14e81-59d3-4597-83a7-ced791d27e90",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                {
                    "id": "26917591-edd3-4ec4-ae80-c200ea77c68a",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_version",
                    "hidden": false,
                    "value": "100"
                },
                {
                    "id": "7b0ab37a-10ac-4468-956d-2cba2cdedd48",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_unreliable",
                    "hidden": false,
                    "value": "0"
                },
                {
                    "id": "0c986667-ea44-46c7-a1d6-5d11622d25d6",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_unreliable \/* Basic UDP send (<1200 bytes; may get lost) *\/",
                    "hidden": false,
                    "value": "0"
                },
                {
                    "id": "1fd48c45-c6bb-4528-8c6c-12272da9b116",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_unreliable_nodelay",
                    "hidden": false,
                    "value": "1"
                },
                {
                    "id": "e2134c25-5c60-422f-b005-056141959dfa",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_unreliable_nodelay \/* Instant non-buffering UDP send (e.g. for voice data) *\/",
                    "hidden": false,
                    "value": "1"
                },
                {
                    "id": "7fe6a114-5a04-438e-9570-abde0a89ae3f",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_reliable",
                    "hidden": false,
                    "value": "2"
                },
                {
                    "id": "7d91155f-97dc-452d-b0eb-a0862a3a1914",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_reliable \/* Reliable send (up to 1MB) *\/",
                    "hidden": false,
                    "value": "2"
                },
                {
                    "id": "3af8c6df-ad85-45e4-bf84-7ceb29729825",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_reliable_buffer",
                    "hidden": false,
                    "value": "3"
                },
                {
                    "id": "303e9186-4631-4490-acef-71283ce95c38",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_reliable_buffer \/* Buffering send (Nagle algorithm) *\/",
                    "hidden": false,
                    "value": "3"
                },
                {
                    "id": "5b104a29-30c0-4d57-9cc4-dba34a4f2e4a",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_eq",
                    "hidden": false,
                    "value": "0"
                },
                {
                    "id": "149f54af-953f-412b-8046-a0c5b44f1b0c",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_eq \/* \"==\" \/ Equal *\/",
                    "hidden": false,
                    "value": "0"
                },
                {
                    "id": "7e1bca57-4648-4adc-8314-07b707880879",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_ne",
                    "hidden": false,
                    "value": "3"
                },
                {
                    "id": "f5a8a728-34d2-4d02-bdb8-942a0753aaa0",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_ne \/* \"!=\" \/ Not Equal *\/",
                    "hidden": false,
                    "value": "3"
                },
                {
                    "id": "afeb67f0-b0dc-4f88-8640-f530e2c799b4",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_lt",
                    "hidden": false,
                    "value": "-1"
                },
                {
                    "id": "4d2b4087-25d2-4a28-b449-a148ccdfbde9",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_lt \/* \"<\" \/ Less Than *\/",
                    "hidden": false,
                    "value": "-1"
                },
                {
                    "id": "7971c634-664e-448c-94b5-8e5fabfcf5aa",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_gt",
                    "hidden": false,
                    "value": "1"
                },
                {
                    "id": "c86bd846-1f05-466a-a160-2057f09edfd5",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_gt \/* \">\" \/ Greater Than *\/",
                    "hidden": false,
                    "value": "1"
                },
                {
                    "id": "6489e868-e545-411e-a27a-5e0b71589b60",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_le",
                    "hidden": false,
                    "value": "-2"
                },
                {
                    "id": "fe97728b-93c5-495f-aa6a-0023456aa95e",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_le \/* \"<=\" \/ Less than or Equal *\/",
                    "hidden": false,
                    "value": "-2"
                },
                {
                    "id": "f2f70375-a33f-4378-b125-0dbea91106ce",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_ge",
                    "hidden": false,
                    "value": "2"
                },
                {
                    "id": "4b7c800c-c9fe-4af0-9cac-cdc22ff58615",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_ge \/* \">=\" \/ Greater than or Equal *\/",
                    "hidden": false,
                    "value": "2"
                },
                {
                    "id": "fc38dc26-32ab-4411-a9c6-7da952a14399",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_distance_filter_close",
                    "hidden": false,
                    "value": "0"
                },
                {
                    "id": "eadfdd3d-9011-4134-8b7a-f68cd2e31ef9",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_distance_filter_default",
                    "hidden": false,
                    "value": "1"
                },
                {
                    "id": "16b8e0d8-7e56-4496-a813-16d8d34b4faa",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_distance_filter_far",
                    "hidden": false,
                    "value": "2"
                },
                {
                    "id": "cdae5671-291e-4bfd-acc5-39ceab4e7a29",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_distance_filter_worldwide",
                    "hidden": false,
                    "value": "3"
                },
                {
                    "id": "77f12340-2d3a-4ebf-8141-0e04f2e70e78",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_type_private",
                    "hidden": false,
                    "value": "0"
                },
                {
                    "id": "5758203b-df80-4ab9-b5c3-0a03099a062a",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_type_private \/* Private lobby. People can only join if invited. *\/",
                    "hidden": false,
                    "value": "0"
                },
                {
                    "id": "6840a5be-da6f-406a-afbc-faf7a3136e58",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_type_friends_only",
                    "hidden": false,
                    "value": "1"
                },
                {
                    "id": "ed223efe-152b-44c5-be5b-ddd4eb24c1aa",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_type_friends_only \/* Friends-only lobby. Visible to friends. *\/",
                    "hidden": false,
                    "value": "1"
                },
                {
                    "id": "f56979ca-2b52-45c0-b0eb-c8b20a6765e7",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_type_public",
                    "hidden": false,
                    "value": "2"
                },
                {
                    "id": "c4674ef8-b1b8-41ad-a18e-5b0496c0e26e",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_type_public \/* Public lobby. Visible to everyone. *\/",
                    "hidden": false,
                    "value": "2"
                }
            ],
            "copyToTargets": 123145844424768,
            "filename": "Steamworks.gml.dll",
            "final": "",
            "functions": [
                {
                    "id": "8274ac1e-3ac5-440e-b421-337173c66261",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 4,
                    "args": [
                        1,
                        1,
                        1,
                        1
                    ],
                    "externalName": "RegisterCallbacks",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "RegisterCallbacks",
                    "returnType": 2
                },
                {
                    "id": "b1c55fd9-759a-4f27-9c5a-c09127962c93",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_net_set_auto_accept_p2p_sessions",
                    "help": "steam_net_set_auto_accept_p2p_sessions(auto_accept) : Sets whether to auto-accept all incoming P2P session requests.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_set_auto_accept_p2p_sessions",
                    "returnType": 2
                },
                {
                    "id": "cc62a8f7-4de8-4bfb-9011-058216d12ad4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_net_accept_p2p_session_raw",
                    "help": "steam_net_accept_p2p_session_raw(user_id_high, user_id_low) : Accepts a P2P session with the given user. Should only be called in the \"p2p_session_request\" event.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_accept_p2p_session_raw",
                    "returnType": 2
                },
                {
                    "id": "527fd7b8-086f-4f28-a2a7-03e0ecbe6e3a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_net_close_p2p_session_raw",
                    "help": "steam_net_close_p2p_session_raw(user_id_high, user_id_low)",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_close_p2p_session_raw",
                    "returnType": 2
                },
                {
                    "id": "1b4676ef-161b-45bb-bb11-eaccd50a3289",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_net_packet_set_type",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_set_type",
                    "returnType": 2
                },
                {
                    "id": "388554c9-e6f6-4515-be54-1aa2178b0a4e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 4,
                    "args": [
                        2,
                        2,
                        1,
                        2
                    ],
                    "externalName": "steam_net_packet_send_raw",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_send_raw",
                    "returnType": 2
                },
                {
                    "id": "a34ff847-e111-4f79-9609-c4fffd05f82c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_packet_receive",
                    "help": "steam_net_packet_receive() : Receives a packet, returns whether successful (retrieve data via steam_net_packet_).",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_receive",
                    "returnType": 2
                },
                {
                    "id": "5cfaf22b-ad9d-42cd-9439-4206d01f783e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_packet_get_size",
                    "help": "steam_net_packet_get_size() : Returns the size of the last received packet (in bytes).",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_get_size",
                    "returnType": 2
                },
                {
                    "id": "a767297e-aff6-4898-b0f8-46bee50827f3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "steam_net_packet_get_data_raw",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_get_data_raw",
                    "returnType": 2
                },
                {
                    "id": "0fd330b2-46bc-47d8-aac9-ea449b4b2908",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_packet_get_sender_id_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_get_sender_id_high",
                    "returnType": 2
                },
                {
                    "id": "20ef18f9-0fe6-4d5b-a220-a2f702e08249",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_packet_get_sender_id_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_get_sender_id_low",
                    "returnType": 2
                },
                {
                    "id": "a5c746d5-6aee-4b65-a5c4-e5e4d8be28c6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_leave",
                    "help": "steam_lobby_leave() : Leaves the current lobby (if any)",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_leave",
                    "returnType": 2
                },
                {
                    "id": "199bdb30-f303-450e-b8f6-299eba437d69",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_is_owner",
                    "help": "steam_lobby_is_owner() : Returns whether the local user is the owner of the current lobby.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_is_owner",
                    "returnType": 2
                },
                {
                    "id": "3027d725-0167-425a-b5c3-d093b37b4217",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_get_owner_id_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_owner_id_high",
                    "returnType": 2
                },
                {
                    "id": "85923d53-763c-4d80-804e-26232ba6d6f4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_get_owner_id_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_owner_id_low",
                    "returnType": 2
                },
                {
                    "id": "db815290-eb80-4ede-abea-a32b5eed6f65",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_get_member_count",
                    "help": "steam_lobby_get_member_count() : Returns the number of users in the lobby.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_member_count",
                    "returnType": 2
                },
                {
                    "id": "5f5e134a-d6d2-44af-bb07-9a3d5b12c22e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_get_member_id_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_member_id_high",
                    "returnType": 2
                },
                {
                    "id": "e5582f19-7fa4-433f-a384-9abd40e902c8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_get_member_id_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_member_id_low",
                    "returnType": 2
                },
                {
                    "id": "f1efeb6c-fe5a-453b-927d-a556be46603a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_activate_invite_overlay",
                    "help": "steam_lobby_activate_invite_overlay() : Opens an overlay to invite users to the current lobby.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_activate_invite_overlay",
                    "returnType": 2
                },
                {
                    "id": "08fc742e-ba55-4d6b-b676-a908c42c1623",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_list_request",
                    "help": "steam_lobby_list_request() : Requests the list of lobbies to be (re-)loaded.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_request",
                    "returnType": 2
                },
                {
                    "id": "dc2986ea-9d21-4dd2-981f-3c448a86bb52",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_list_is_loading",
                    "help": "steam_lobby_list_is_loading() : Returns whether the list of lobbies is currently loading.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_is_loading",
                    "returnType": 2
                },
                {
                    "id": "cd6f00fb-a78a-4356-b4fd-3fa36833a971",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 3,
                    "args": [
                        1,
                        1,
                        2
                    ],
                    "externalName": "steam_lobby_list_add_string_filter",
                    "help": "steam_lobby_list_add_string_filter(key, value, comparison_type) : Sets a string filter for the next lobby list request.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_add_string_filter",
                    "returnType": 2
                },
                {
                    "id": "74e8657c-e961-4f3a-a282-74f08b97866c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 3,
                    "args": [
                        1,
                        2,
                        2
                    ],
                    "externalName": "steam_lobby_list_add_numerical_filter",
                    "help": "steam_lobby_list_add_numerical_filter(key, value, comparison_type) : Sets a numerical filter for the next lobby list request.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_add_numerical_filter",
                    "returnType": 2
                },
                {
                    "id": "5555a099-5a52-474b-aa76-86627562d586",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        2
                    ],
                    "externalName": "steam_lobby_list_add_near_filter",
                    "help": "steam_lobby_list_add_near_filter(key, value) : Sorts the results of the next lobby list request based to proximity to the given value.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_add_near_filter",
                    "returnType": 2
                },
                {
                    "id": "0afa4db2-7af9-4f76-93fd-5099384a6ceb",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_list_add_distance_filter",
                    "help": "steam_lobby_list_add_distance_filter(mode) : Restricts results by region",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_add_distance_filter",
                    "returnType": 2
                },
                {
                    "id": "07f7c53e-1dfc-41bc-84fb-b2f337d8f74b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_list_get_count",
                    "help": "steam_lobby_list_get_count() : Returns the number of the matching lobbies.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_get_count",
                    "returnType": 2
                },
                {
                    "id": "953e0c62-df6f-4285-bb4e-9d28bce5b625",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        1
                    ],
                    "externalName": "steam_lobby_list_get_data",
                    "help": "steam_lobby_list_get_data(index, key) : Retrieves given information about the given lobby",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_get_data",
                    "returnType": 1
                },
                {
                    "id": "2771ee4a-4979-40d4-8fba-4a747aa884c2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_list_get_lobby_id_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_get_lobby_id_high",
                    "returnType": 2
                },
                {
                    "id": "a9d45649-67e2-483d-aee4-a9a5b06f2348",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_list_get_lobby_id_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_get_lobby_id_low",
                    "returnType": 2
                },
                {
                    "id": "e413418c-49e3-428b-8a50-fe2c6dad3a9b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_list_join",
                    "help": "steam_lobby_list_join(index) : [async] Starts joining the given lobby.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_join",
                    "returnType": 2
                },
                {
                    "id": "54577209-2168-441f-9fbf-fd8bc6064082",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_lobby_join_id_raw",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_join_id_raw",
                    "returnType": 2
                },
                {
                    "id": "bbe1fca8-8044-4e20-9403-7bcf2c1dc334",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_lobby_create",
                    "help": "steam_lobby_create(type, max_members) : [async] Creates a lobby.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_create",
                    "returnType": 2
                },
                {
                    "id": "237c28b5-af53-4437-b708-944b4a3ac908",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "steam_lobby_set_data",
                    "help": "steam_lobby_set_data(key, value) : [lobby owner only] Sets the data for the current lobby.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_set_data",
                    "returnType": 2
                },
                {
                    "id": "1fcabef6-de8c-48a3-be83-ab684bfef7a6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_set_type",
                    "help": "steam_lobby_set_type(type) : [lobby owner only] Changes the type of the current lobby.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_set_type",
                    "returnType": 2
                },
                {
                    "id": "0ecf8274-f1ca-4bda-9f17-1936eb346e0b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_get_user_steam_id_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_get_user_steam_id_high",
                    "returnType": 2
                },
                {
                    "id": "15b08609-b1bd-4c55-bcce-0eaa2a4be291",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_get_user_steam_id_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_get_user_steam_id_low",
                    "returnType": 2
                },
                {
                    "id": "738d98b6-225f-4c4a-a5eb-6038f250b1ef",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_user_set_played_with",
                    "help": "steam_user_set_played_with(id_high, id_low) : Can be called on lobby session start, adds the user to \"recently played with\" list.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_user_set_played_with",
                    "returnType": 2
                },
                {
                    "id": "c0c74e79-af26-4b60-befb-57e3d1a41b66",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "steam_activate_overlay_raw",
                    "help": "steam_activate_overlay_raw(overlay_code) : Activates an overlay by it's raw Steam API name.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_activate_overlay_raw",
                    "returnType": 2
                },
                {
                    "id": "76ed3684-8c1b-4e27-b58c-5cb27ee56c38",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "int64_from_string_high",
                    "help": "int64_from_string_high(cstring)",
                    "hidden": false,
                    "kind": 11,
                    "name": "int64_from_string_high",
                    "returnType": 2
                },
                {
                    "id": "faf52200-b048-4782-91c4-fa9bb725c84f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "int64_from_string_low",
                    "help": "int64_from_string_low(cstring)",
                    "hidden": false,
                    "kind": 11,
                    "name": "int64_from_string_low",
                    "returnType": 2
                },
                {
                    "id": "e737f3d0-78d8-4228-a9ec-8985b4bdd6fa",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "int64_combine_string",
                    "help": "int64_combine_string(high, low)",
                    "hidden": false,
                    "kind": 11,
                    "name": "int64_combine_string",
                    "returnType": 1
                },
                {
                    "id": "44495330-3354-4a0a-8374-d6ec61e26d88",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_update",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_update",
                    "returnType": 2
                },
                {
                    "id": "dd65c9d6-e348-4861-ba36-b8975e19e774",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_restart_if_necessary",
                    "help": "steam_restart_if_necessary() : Detects if the app was run from Steam client and restarts if needed. Returns whether app should quit.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_restart_if_necessary",
                    "returnType": 2
                },
                {
                    "id": "75150da6-46a6-4ff0-a2d8-19d8e5ba9645",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_api_flags",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_api_flags",
                    "returnType": 2
                },
                {
                    "id": "a7cf1cbe-0ef6-4faa-a6c0-d6221c195eaa",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_net_init_cpp",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_init_cpp",
                    "returnType": 2
                },
                {
                    "id": "0e330f3f-2215-4a66-923f-fdaa0f89b411",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_is_ready",
                    "help": "steam_net_is_ready() : Returns whether the extension has initialized successfully.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_is_ready",
                    "returnType": 2
                },
                {
                    "id": "c3291f9a-5c90-4f1e-86c7-b7584498ef33",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_get_version",
                    "help": "steam_net_get_version() : Returns ",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_get_version",
                    "returnType": 2
                },
                {
                    "id": "956ae01b-1f91-41ca-b5af-1ea46addc672",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_is_available",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_is_available",
                    "returnType": 2
                },
                {
                    "id": "3b285e95-900a-4d4d-afe9-3dd4fb8479d5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_init_cpp_pre",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_init_cpp_pre",
                    "returnType": 2
                }
            ],
            "init": "steam_net_init_cpp_pre",
            "kind": 1,
            "order": [
                
            ],
            "origname": "Steamworks.gml.dll",
            "uncompress": false
        },
        {
            "id": "4322fc4d-f111-47a5-9cab-25e2a55dea0a",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                {
                    "id": "9b2c0cb4-e75c-4683-8d2d-eaf520535ccb",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_version",
                    "hidden": true,
                    "value": "100"
                },
                {
                    "id": "7e52993c-b3c5-412b-92b0-dea48998718f",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_unreliable",
                    "hidden": true,
                    "value": "0"
                },
                {
                    "id": "736c6625-4be7-4a14-8ac3-b141dd8ff45a",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_unreliable_nodelay",
                    "hidden": true,
                    "value": "1"
                },
                {
                    "id": "8c6fc9df-d2b0-47a3-a95e-d3a0952bb057",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_reliable",
                    "hidden": true,
                    "value": "2"
                },
                {
                    "id": "59249974-6bf4-4fbd-a887-32a5c182c3e0",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_reliable_buffer",
                    "hidden": true,
                    "value": "3"
                },
                {
                    "id": "c9abbdef-626b-45d4-a9bc-15617158e1e6",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_eq",
                    "hidden": true,
                    "value": "0"
                },
                {
                    "id": "e3784f95-aff7-4a39-b8b9-ceb9386090d9",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_ne",
                    "hidden": true,
                    "value": "3"
                },
                {
                    "id": "6c14990f-5655-4270-bfcd-09a38edc3c7d",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_lt",
                    "hidden": true,
                    "value": "-1"
                },
                {
                    "id": "d7468bb9-8a43-4a7a-a0c0-0b664babfdf8",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_gt",
                    "hidden": true,
                    "value": "1"
                },
                {
                    "id": "0be53c5d-9992-462f-b45e-7f96f62e0113",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_le",
                    "hidden": true,
                    "value": "-2"
                },
                {
                    "id": "98e111e5-6a41-4838-9687-1123b62e4155",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_ge",
                    "hidden": true,
                    "value": "2"
                },
                {
                    "id": "93a059db-7a48-4c4f-a171-327676f3daf1",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_distance_filter_close",
                    "hidden": true,
                    "value": "0"
                },
                {
                    "id": "4940c239-ee31-4509-afdc-ba2ba2102397",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_distance_filter_default",
                    "hidden": true,
                    "value": "1"
                },
                {
                    "id": "bc791401-7ab1-4f3b-ab01-774238ac0f47",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_distance_filter_far",
                    "hidden": true,
                    "value": "2"
                },
                {
                    "id": "1b30e429-a725-4a50-a717-da639d4af6e5",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_distance_filter_worldwide",
                    "hidden": true,
                    "value": "3"
                },
                {
                    "id": "4e3b3bc8-605f-4e5e-9ab8-daf760692b20",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_type_private",
                    "hidden": true,
                    "value": "0"
                },
                {
                    "id": "41cc3921-4f06-4d72-a4ac-62dd6d036827",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_type_friends_only",
                    "hidden": true,
                    "value": "1"
                },
                {
                    "id": "4dfaaa5b-f7b1-4546-b93a-29cff8cbe147",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_type_public",
                    "hidden": true,
                    "value": "2"
                }
            ],
            "copyToTargets": 17592320262272,
            "filename": "Steamworks.gml.so",
            "final": "",
            "functions": [
                {
                    "id": "8c19ea6c-d5aa-428e-9666-2f3a485f6c3e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 4,
                    "args": [
                        1,
                        1,
                        1,
                        1
                    ],
                    "externalName": "RegisterCallbacks",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "RegisterCallbacks",
                    "returnType": 2
                },
                {
                    "id": "27f8c144-acf6-4ae9-9ab6-f8c281b090d4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_net_set_auto_accept_p2p_sessions",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_set_auto_accept_p2p_sessions",
                    "returnType": 2
                },
                {
                    "id": "96cda830-0530-44b5-b79e-06473713e906",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_net_accept_p2p_session_raw",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_accept_p2p_session_raw",
                    "returnType": 2
                },
                {
                    "id": "9b44d9d5-e428-44e0-bbc2-5ab428d7baab",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_net_close_p2p_session_raw",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_close_p2p_session_raw",
                    "returnType": 2
                },
                {
                    "id": "aa6860c4-c114-4533-9c19-1116c40383ef",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_net_packet_set_type",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_set_type",
                    "returnType": 2
                },
                {
                    "id": "ad700ddb-3054-472f-8b8d-55b5540803a5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 4,
                    "args": [
                        2,
                        2,
                        1,
                        2
                    ],
                    "externalName": "steam_net_packet_send_raw",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_send_raw",
                    "returnType": 2
                },
                {
                    "id": "761f2a52-55aa-4dcd-8061-3e464f073e61",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_packet_receive",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_receive",
                    "returnType": 2
                },
                {
                    "id": "cf5451a9-774d-428b-a5f6-b0906e54d56a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_packet_get_size",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_get_size",
                    "returnType": 2
                },
                {
                    "id": "dd7cbd0f-62ba-4d12-8b2f-fb12ac1a62a7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "steam_net_packet_get_data_raw",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_get_data_raw",
                    "returnType": 2
                },
                {
                    "id": "01822fa3-7c97-4e99-ae4e-55af84159a59",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_packet_get_sender_id_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_get_sender_id_high",
                    "returnType": 2
                },
                {
                    "id": "3a9297e1-a91f-44c1-a384-9951fc6c152a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_packet_get_sender_id_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_get_sender_id_low",
                    "returnType": 2
                },
                {
                    "id": "bd8af594-28f0-45b3-af05-bf5dfc5b8b40",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_leave",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_leave",
                    "returnType": 2
                },
                {
                    "id": "4f5033a8-05b8-4c9e-a26a-c1892d77a26d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_is_owner",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_is_owner",
                    "returnType": 2
                },
                {
                    "id": "88399eb2-6fb7-4f96-b1c0-653f62fb717f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_get_owner_id_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_owner_id_high",
                    "returnType": 2
                },
                {
                    "id": "b3e1ffe1-8ffc-4655-ba1e-c501500a1d7e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_get_owner_id_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_owner_id_low",
                    "returnType": 2
                },
                {
                    "id": "9ba5def8-8015-49b3-ae6e-7e5c462e083a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_get_member_count",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_member_count",
                    "returnType": 2
                },
                {
                    "id": "82137ce4-6934-4d4c-a583-a1ff044167b5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_get_member_id_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_member_id_high",
                    "returnType": 2
                },
                {
                    "id": "ee5b8ad8-25a0-40f3-8445-99a484f66b94",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_get_member_id_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_member_id_low",
                    "returnType": 2
                },
                {
                    "id": "7fa05f6c-6315-4299-a56c-4d874075fe09",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_activate_invite_overlay",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_activate_invite_overlay",
                    "returnType": 2
                },
                {
                    "id": "239ff0bd-0930-4089-ae63-d3499fc8c433",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_list_request",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_request",
                    "returnType": 2
                },
                {
                    "id": "e56c2941-6abf-4106-afda-bcbcb35fdec8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_list_is_loading",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_is_loading",
                    "returnType": 2
                },
                {
                    "id": "0505fd03-5993-458e-a8f3-cf9371fb519e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 3,
                    "args": [
                        1,
                        1,
                        2
                    ],
                    "externalName": "steam_lobby_list_add_string_filter",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_add_string_filter",
                    "returnType": 2
                },
                {
                    "id": "e71dfe4c-2ade-45b5-8360-9b6a5fef1a2a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 3,
                    "args": [
                        1,
                        2,
                        2
                    ],
                    "externalName": "steam_lobby_list_add_numerical_filter",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_add_numerical_filter",
                    "returnType": 2
                },
                {
                    "id": "ce969d4a-1067-48f3-83ec-fa158dd8bc00",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        2
                    ],
                    "externalName": "steam_lobby_list_add_near_filter",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_add_near_filter",
                    "returnType": 2
                },
                {
                    "id": "2d12a2d7-5c33-49a6-8e5b-9bbc25ff7a14",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_list_add_distance_filter",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_add_distance_filter",
                    "returnType": 2
                },
                {
                    "id": "8bcc8423-7d98-4103-8178-664257847765",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_list_get_count",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_get_count",
                    "returnType": 2
                },
                {
                    "id": "9caea3df-ed5f-4c39-83be-fb0facdc8370",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        1
                    ],
                    "externalName": "steam_lobby_list_get_data",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_get_data",
                    "returnType": 1
                },
                {
                    "id": "5e1cd372-6018-49a1-b592-c973d201b3d1",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_list_get_lobby_id_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_get_lobby_id_high",
                    "returnType": 2
                },
                {
                    "id": "5beeb203-bf19-4269-bfb3-d71ffbc6f414",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_list_get_lobby_id_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_get_lobby_id_low",
                    "returnType": 2
                },
                {
                    "id": "8314152a-391c-461c-901c-af003cbc086c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_list_join",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_join",
                    "returnType": 2
                },
                {
                    "id": "9e24c62c-da97-4e42-ad31-f796c9a80f96",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_lobby_join_id_raw",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_join_id_raw",
                    "returnType": 2
                },
                {
                    "id": "1030b463-f53d-454b-a049-63be8b701ebf",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_lobby_create",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_create",
                    "returnType": 2
                },
                {
                    "id": "4b15900b-75d8-452c-9fa5-dd489f81bd08",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "steam_lobby_set_data",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_set_data",
                    "returnType": 2
                },
                {
                    "id": "a047fda5-c86a-4a30-bdac-be45cd4a9f30",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_set_type",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_set_type",
                    "returnType": 2
                },
                {
                    "id": "17994bc9-b6da-4bd0-b674-94924144c399",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_get_user_steam_id_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_get_user_steam_id_high",
                    "returnType": 2
                },
                {
                    "id": "003c48ef-1c17-4b8c-a249-ee84f62f5256",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_get_user_steam_id_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_get_user_steam_id_low",
                    "returnType": 2
                },
                {
                    "id": "09df843b-adec-423a-a6f6-1930f6439a17",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_user_set_played_with",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_user_set_played_with",
                    "returnType": 2
                },
                {
                    "id": "558d966b-e6a0-4475-9c82-9c82448f4fa2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "steam_activate_overlay_raw",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_activate_overlay_raw",
                    "returnType": 2
                },
                {
                    "id": "ece06620-dd8a-418a-8f6f-152d2848c2b9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "int64_from_string_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "int64_from_string_high",
                    "returnType": 2
                },
                {
                    "id": "bea00bc7-bf2a-47d5-a162-fdf36e5498fb",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "int64_from_string_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "int64_from_string_low",
                    "returnType": 2
                },
                {
                    "id": "0b1523c3-10bd-44f7-a54d-322bcbd2987f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "int64_combine_string",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "int64_combine_string",
                    "returnType": 1
                },
                {
                    "id": "a9a9d74d-b119-4560-8dde-df45c8e27635",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_update",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_update",
                    "returnType": 2
                },
                {
                    "id": "2ad260dc-1993-42e5-8ae9-b2a635bb1b6e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_restart_if_necessary",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_restart_if_necessary",
                    "returnType": 2
                },
                {
                    "id": "b0a0b84e-3b42-4ca4-bc84-e87b8680a5cd",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_api_flags",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_api_flags",
                    "returnType": 2
                },
                {
                    "id": "2d0d4d50-bb30-4cd0-8a53-a22511a2885d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_net_init_cpp",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_init_cpp",
                    "returnType": 2
                },
                {
                    "id": "84a7988b-a150-4498-a7a1-a97e043a4afc",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_is_ready",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_is_ready",
                    "returnType": 2
                },
                {
                    "id": "ade794a8-2c52-4bfe-8ade-ec8cadacf61d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_get_version",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_get_version",
                    "returnType": 2
                },
                {
                    "id": "ec1d86ed-1bfd-4e8d-a00d-4cbef7c15a53",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_is_available",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_is_available",
                    "returnType": 2
                },
                {
                    "id": "e4794353-7fa2-48ac-8af1-38572bcb83ba",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_init_cpp_pre",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_init_cpp_pre",
                    "returnType": 2
                }
            ],
            "init": "steam_net_init_cpp_pre",
            "kind": 1,
            "order": [
                
            ],
            "origname": "extensions\\Steamworks.gml.so",
            "uncompress": false
        },
        {
            "id": "9aeea1fd-ca37-40e0-87f6-7a7f839d3fed",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                {
                    "id": "10eed1bb-0883-4d16-9390-68a0486749e3",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_version",
                    "hidden": true,
                    "value": "100"
                },
                {
                    "id": "7c4b7419-f93f-45e6-9488-785b18094a24",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_unreliable",
                    "hidden": true,
                    "value": "0"
                },
                {
                    "id": "484eaf62-27b2-475a-9168-27bbafba8876",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_unreliable_nodelay",
                    "hidden": true,
                    "value": "1"
                },
                {
                    "id": "a19025f1-f6ef-4f09-96cf-0dbfda0133f3",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_reliable",
                    "hidden": true,
                    "value": "2"
                },
                {
                    "id": "607c0f89-2c0f-4395-8106-907fdac31fc1",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_packet_type_reliable_buffer",
                    "hidden": true,
                    "value": "3"
                },
                {
                    "id": "265019bb-3753-4f9c-a739-2b0283352bfa",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_eq",
                    "hidden": true,
                    "value": "0"
                },
                {
                    "id": "a8ef10bb-1121-4590-adda-51b5153082bb",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_ne",
                    "hidden": true,
                    "value": "3"
                },
                {
                    "id": "850c7ac6-9367-4692-8485-b03959bff6d6",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_lt",
                    "hidden": true,
                    "value": "-1"
                },
                {
                    "id": "26051c06-cbca-47ba-b07a-ae887ce5ce03",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_gt",
                    "hidden": true,
                    "value": "1"
                },
                {
                    "id": "dfa4d8f9-8592-4a60-bea0-c0b7d45af97d",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_le",
                    "hidden": true,
                    "value": "-2"
                },
                {
                    "id": "27a69b76-2fc9-443a-952b-442671277639",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_filter_ge",
                    "hidden": true,
                    "value": "2"
                },
                {
                    "id": "ca2d2635-a95b-4891-b15d-415214c4b663",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_distance_filter_close",
                    "hidden": true,
                    "value": "0"
                },
                {
                    "id": "1f08d3de-2c77-40f4-a20b-12dea797e4d2",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_distance_filter_default",
                    "hidden": true,
                    "value": "1"
                },
                {
                    "id": "aad6cea4-1415-4921-bced-f6443164db9c",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_distance_filter_far",
                    "hidden": true,
                    "value": "2"
                },
                {
                    "id": "34a3b6da-5ac8-4f76-bf64-24b055e8e19d",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_list_distance_filter_worldwide",
                    "hidden": true,
                    "value": "3"
                },
                {
                    "id": "779d863f-7c8e-4a63-be70-afede4d889ac",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_type_private",
                    "hidden": true,
                    "value": "0"
                },
                {
                    "id": "eabe0d92-7f99-4edb-922f-6a6133902a56",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_type_friends_only",
                    "hidden": true,
                    "value": "1"
                },
                {
                    "id": "f7d4dc78-a6dd-4460-a3dc-c52a40afbe40",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_lobby_type_public",
                    "hidden": true,
                    "value": "2"
                }
            ],
            "copyToTargets": 67108866,
            "filename": "Steamworks.gml.dylib",
            "final": "",
            "functions": [
                {
                    "id": "bf8689f2-6df8-4874-8ffa-5017be54269b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 4,
                    "args": [
                        1,
                        1,
                        1,
                        1
                    ],
                    "externalName": "RegisterCallbacks",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "RegisterCallbacks",
                    "returnType": 2
                },
                {
                    "id": "7b4e929a-00c4-4927-8f14-c977e2032570",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_net_set_auto_accept_p2p_sessions",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_set_auto_accept_p2p_sessions",
                    "returnType": 2
                },
                {
                    "id": "50918554-c28a-489d-bd99-a830dbe5ebe4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_net_accept_p2p_session_raw",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_accept_p2p_session_raw",
                    "returnType": 2
                },
                {
                    "id": "0ba2ad46-2c65-49ea-847f-4ca082cf019b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_net_close_p2p_session_raw",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_close_p2p_session_raw",
                    "returnType": 2
                },
                {
                    "id": "1516f91f-0f8a-4916-af70-d3ed3e790a28",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_net_packet_set_type",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_set_type",
                    "returnType": 2
                },
                {
                    "id": "4cb706a6-1b82-49ed-af15-11c54ea83f28",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 4,
                    "args": [
                        2,
                        2,
                        1,
                        2
                    ],
                    "externalName": "steam_net_packet_send_raw",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_send_raw",
                    "returnType": 2
                },
                {
                    "id": "18581648-dd33-4e4c-90cd-e32d7cd0dad0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_packet_receive",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_receive",
                    "returnType": 2
                },
                {
                    "id": "6ed95b72-699a-4c88-b7da-7eb54d078008",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_packet_get_size",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_get_size",
                    "returnType": 2
                },
                {
                    "id": "e67d5ff8-da3f-4882-a4e8-72a7ff9490cf",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "steam_net_packet_get_data_raw",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_get_data_raw",
                    "returnType": 2
                },
                {
                    "id": "9d50ed8a-3e22-4a41-9091-97fe02642c44",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_packet_get_sender_id_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_get_sender_id_high",
                    "returnType": 2
                },
                {
                    "id": "8b5ceac1-3ce8-4efc-985c-2387e534ae0e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_packet_get_sender_id_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_get_sender_id_low",
                    "returnType": 2
                },
                {
                    "id": "37c6606f-e51b-44e2-858c-3f924ced7c4b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_leave",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_leave",
                    "returnType": 2
                },
                {
                    "id": "b6dfc8b3-e67b-4eca-a2e7-2b7069d30fae",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_is_owner",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_is_owner",
                    "returnType": 2
                },
                {
                    "id": "08615f88-3198-41dc-ab89-831739ffdd67",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_get_owner_id_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_owner_id_high",
                    "returnType": 2
                },
                {
                    "id": "f50772ae-5473-4d3a-8869-ec02ad782b29",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_get_owner_id_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_owner_id_low",
                    "returnType": 2
                },
                {
                    "id": "d36aa567-2c46-49a3-8350-61f1bc3ba7c3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_get_member_count",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_member_count",
                    "returnType": 2
                },
                {
                    "id": "44e5eb78-3c5c-4d5e-9434-7a8e5ac6db6c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_get_member_id_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_member_id_high",
                    "returnType": 2
                },
                {
                    "id": "ee671473-99a1-4ccd-91f4-b655623d69f9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_get_member_id_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_member_id_low",
                    "returnType": 2
                },
                {
                    "id": "60528f98-7595-4d4b-98e1-5ddac0824cab",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_activate_invite_overlay",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_activate_invite_overlay",
                    "returnType": 2
                },
                {
                    "id": "9fbdfbfa-16ae-4a26-b05a-ed6c0ded2fc4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_list_request",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_request",
                    "returnType": 2
                },
                {
                    "id": "ca5bceaa-b2e3-4bd3-9024-eb2aeaab0037",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_list_is_loading",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_is_loading",
                    "returnType": 2
                },
                {
                    "id": "a98c4bee-2c97-4311-90df-bb535f96b0ba",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 3,
                    "args": [
                        1,
                        1,
                        2
                    ],
                    "externalName": "steam_lobby_list_add_string_filter",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_add_string_filter",
                    "returnType": 2
                },
                {
                    "id": "4f1a4443-6ff3-440c-86e2-5d1467b9feee",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 3,
                    "args": [
                        1,
                        2,
                        2
                    ],
                    "externalName": "steam_lobby_list_add_numerical_filter",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_add_numerical_filter",
                    "returnType": 2
                },
                {
                    "id": "e3a6357f-c56e-4b09-aec7-f883fee95f5e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        2
                    ],
                    "externalName": "steam_lobby_list_add_near_filter",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_add_near_filter",
                    "returnType": 2
                },
                {
                    "id": "ffca2255-2d21-499d-824f-c5e006b891d2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_list_add_distance_filter",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_add_distance_filter",
                    "returnType": 2
                },
                {
                    "id": "c51d7161-867c-4bc1-a8fc-2673ca0b807b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_list_get_count",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_get_count",
                    "returnType": 2
                },
                {
                    "id": "98d72289-bedc-423e-ab90-25fada405d8f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        1
                    ],
                    "externalName": "steam_lobby_list_get_data",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_get_data",
                    "returnType": 1
                },
                {
                    "id": "110689bc-fea5-4d59-8da7-af6408a05735",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_list_get_lobby_id_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_get_lobby_id_high",
                    "returnType": 2
                },
                {
                    "id": "5ee7fad2-872e-44b6-8fed-44a741a9b530",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_list_get_lobby_id_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_get_lobby_id_low",
                    "returnType": 2
                },
                {
                    "id": "19bb4123-e393-4bf5-ab29-a3cdbaba3294",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_list_join",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_join",
                    "returnType": 2
                },
                {
                    "id": "1fdc8aee-c3eb-4b19-a6bc-3d81f47f8fe8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_lobby_join_id_raw",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_join_id_raw",
                    "returnType": 2
                },
                {
                    "id": "586527e0-69cc-4b53-ad46-6a68fdee713b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_lobby_create",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_create",
                    "returnType": 2
                },
                {
                    "id": "819eda20-08a3-443d-aec1-0b8760b2e0e8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "steam_lobby_set_data",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_set_data",
                    "returnType": 2
                },
                {
                    "id": "9e61a680-9412-4a6d-b999-dd17c01171de",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_set_type",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_set_type",
                    "returnType": 2
                },
                {
                    "id": "7a64bf99-7a3f-4488-b504-a2eb0a1ee34c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_get_user_steam_id_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_get_user_steam_id_high",
                    "returnType": 2
                },
                {
                    "id": "8910752a-97ac-4a91-a76b-c92293552ad7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_get_user_steam_id_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_get_user_steam_id_low",
                    "returnType": 2
                },
                {
                    "id": "9949e8fd-a0e2-45c3-886f-b20585c121d6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_user_set_played_with",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_user_set_played_with",
                    "returnType": 2
                },
                {
                    "id": "d045065e-694c-48ed-9b47-7d9e6089d435",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "steam_activate_overlay_raw",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_activate_overlay_raw",
                    "returnType": 2
                },
                {
                    "id": "71e9dc0e-d230-49e5-8ab3-9964aff679be",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "int64_from_string_high",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "int64_from_string_high",
                    "returnType": 2
                },
                {
                    "id": "9b36b01a-b8dc-49d0-81f3-3bd3a1b420e2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "int64_from_string_low",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "int64_from_string_low",
                    "returnType": 2
                },
                {
                    "id": "d5b85239-e589-4abb-90c7-5cd64a3c90b9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "int64_combine_string",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "int64_combine_string",
                    "returnType": 1
                },
                {
                    "id": "54847f0f-e7c9-4d61-a69b-de2d1a6a0f17",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_update",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_update",
                    "returnType": 2
                },
                {
                    "id": "eac2fe05-0712-4836-85a2-2d60e6feb3f4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_restart_if_necessary",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_restart_if_necessary",
                    "returnType": 2
                },
                {
                    "id": "1703186b-3953-4b1f-9a00-e7d4b71f2828",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_api_flags",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_api_flags",
                    "returnType": 2
                },
                {
                    "id": "3d6cb49f-fe23-449e-9294-4b01e163f773",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_net_init_cpp",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_init_cpp",
                    "returnType": 2
                },
                {
                    "id": "6c0796c1-1f7b-4137-bdf6-6ffa432fc878",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_is_ready",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_is_ready",
                    "returnType": 2
                },
                {
                    "id": "a7b92c31-2191-4a91-8ce6-94fe3572cfc3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_get_version",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_get_version",
                    "returnType": 2
                },
                {
                    "id": "c2cc93aa-00c8-438f-9e35-0d43e25d4a99",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_is_available",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_is_available",
                    "returnType": 2
                },
                {
                    "id": "af07c19f-6d07-4046-b8af-0e99ee135716",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_init_cpp_pre",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_init_cpp_pre",
                    "returnType": 2
                }
            ],
            "init": "steam_net_init_cpp_pre",
            "kind": 1,
            "order": [
                
            ],
            "origname": "extensions\\Steamworks.gml.dylib",
            "uncompress": false
        },
        {
            "id": "817b7c2f-e0b0-49ca-a482-56a1eaec6021",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                {
                    "id": "3cca2eae-7714-42cf-adff-882009363d6c",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_initialized",
                    "hidden": false,
                    "value": "global.g_steam_net_initialized"
                },
                {
                    "id": "ccb3a752-34ee-4732-9dc3-4706754347ae",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "steam_net_initialized \/* Whether the extension is initialized. *\/",
                    "hidden": false,
                    "value": "global.g_steam_net_initialized"
                }
            ],
            "copyToTargets": 113497714299118,
            "filename": "Steamworks.gml.gml",
            "final": "",
            "functions": [
                {
                    "id": "b623c204-61c5-482d-8235-6f8bd82c5d78",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_id_create",
                    "help": "steam_id_create(high, low) : Creates an immutable Steam ID.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_id_create",
                    "returnType": 2
                },
                {
                    "id": "52cd2301-9d85-403d-ac02-649121591bfe",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_id_get_high",
                    "help": "steam_id_get_high(steam_id) : Returns higher 32 bits of a Steam ID",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_id_get_high",
                    "returnType": 2
                },
                {
                    "id": "09398365-2538-4847-b8af-62af198823a5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_id_get_low",
                    "help": "steam_id_get_low(steam_id) : Returns lower 32 bits of a Steam ID",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_id_get_low",
                    "returnType": 2
                },
                {
                    "id": "06ddf7a5-891b-469b-a7e4-b5708e729ffa",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "steam_id_equals",
                    "help": "steam_id_equals(id1, id2) : Returns whether two IDs match up.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_id_equals",
                    "returnType": 2
                },
                {
                    "id": "e9d4c034-f491-4b2d-bb50-cd6b433a5966",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_id_from_int64",
                    "help": "steam_id_from_int64(value) : Creates a Steam ID from an int64",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_id_from_int64",
                    "returnType": 2
                },
                {
                    "id": "d95febd2-3786-4bf6-a93f-27c8f9057293",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_id_to_int64",
                    "help": "steam_id_to_int64(steam_id) : Converts a Steam ID to int64",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_id_to_int64",
                    "returnType": 2
                },
                {
                    "id": "48b3f81e-2312-4875-ac87-24d5bcb0fa03",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_check_version",
                    "help": "steam_net_check_version() : Returns whether the DLL matches the extension version.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_check_version",
                    "returnType": 2
                },
                {
                    "id": "a2e1b11c-901c-4114-b601-7835301fbd6f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_net_accept_p2p_session",
                    "help": "steam_net_accept_p2p_session(user_id) : Accepts a P2P session with the given user. Should only be called in the \"p2p_session_request\" event.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_accept_p2p_session",
                    "returnType": 2
                },
                {
                    "id": "00b5ea78-d8a4-4d30-b104-4092553f8fb2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_net_close_p2p_session",
                    "help": "steam_net_close_p2p_session(user_id) : Closes the P2P session with the given user (if any)",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_close_p2p_session",
                    "returnType": 2
                },
                {
                    "id": "70f8cfc3-8f00-4384-b7b6-b6f5290bdd8a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_packet_get_sender_id",
                    "help": "steam_net_packet_get_sender_id() : Returns the sender ID (int64) of the last received packet.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_get_sender_id",
                    "returnType": 2
                },
                {
                    "id": "747fe8b5-03a3-4085-8095-0ee7e211b9d9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_net_packet_get_data",
                    "help": "steam_net_packet_get_data(buffer) : Copies the current packet data to the given buffer.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_get_data",
                    "returnType": 2
                },
                {
                    "id": "84a2fbb2-87d9-479e-b98b-8dc8ee6da106",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 4,
                    "args": [
                        2,
                        2,
                        2,
                        2
                    ],
                    "externalName": "steam_net_packet_send",
                    "help": "steam_net_packet_send(steam_id, buffer, size, type) : Sends a packet to the given destination.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_packet_send",
                    "returnType": 2
                },
                {
                    "id": "fcfdd0dd-76a5-4cd5-a665-c2d02fcdd259",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_join_id",
                    "help": "steam_lobby_join_id(steam_id) : Joins the given lobby",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_join_id",
                    "returnType": 2
                },
                {
                    "id": "fd01c80a-86d7-4a0d-81b7-04e0a3ca3905",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_lobby_get_owner_id",
                    "help": "steam_lobby_get_owner_id() : Returns the user ID of the authoritative user in the lobby.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_owner_id",
                    "returnType": 2
                },
                {
                    "id": "4119d714-af54-4661-b841-b208d3b9e732",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_get_member_id",
                    "help": "steam_lobby_get_member_id(index) : Returns the user ID of the given user in the lobby.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_get_member_id",
                    "returnType": 2
                },
                {
                    "id": "3fd73807-4a30-41cc-92b0-9183e9f1a57b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "steam_lobby_list_get_lobby_id",
                    "help": "steam_lobby_list_get_lobby_id(index) : Returns the ID of the given lobby.",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_lobby_list_get_lobby_id",
                    "returnType": 2
                },
                {
                    "id": "4b96db30-462e-43fd-976f-6627bbd598f2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "steam_net_init_gml",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "steam_net_init_gml",
                    "returnType": 2
                }
            ],
            "init": "steam_net_init_gml",
            "kind": 2,
            "order": [
                
            ],
            "origname": "Steamworks.gml.gml",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "LGPLv3",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "version": "1.0.0"
}